package ir.asgari.yekmive;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

public class ActivityViewItem extends AppCompatActivity {
    public static String ID_LAYOUT = "name_layout";
    public static int home = 1;
    public static int favorites = 1;
    public static int notification = 2;
    public static int profile = 3;
    public static int search = 10;
    public static int NotLogin = 11;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item2);
        switch (getIntent().getIntExtra(ID_LAYOUT, 0)) {
            case 3: {
                setFragment(new FavoritesFragment());
                break;
            }
            case 4: {
                setFragment(new NotificationsFragment());
                break;
            }
            case 100: {
                setFragment(new FragmentEditProfile());
                break;
            }
            case 10: {
                setFragment(new SearchFragment());
                break;
            }
            case 11:
                setFragment(new NotLoginFragment());
                break;
        }
    }

    void setFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, fragment);
        ft.commit();
    }
}