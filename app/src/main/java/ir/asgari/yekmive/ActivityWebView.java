package ir.asgari.yekmive;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;

public class ActivityWebView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view2);
        WebView webView=findViewById(R.id.web_view);
        webView.loadUrl(getIntent().getStringExtra("url"));

    }
}