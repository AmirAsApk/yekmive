package ir.asgari.yekmive;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

import ir.asgari.yekmive.databinding.BottomSheetDialogBuyBinding;

public class BottomSheetDialogBuy extends BottomSheetDialogFragment {

    BottomSheetDialogBuyBinding binding;
    FactorBuy factorBuy;

    public BottomSheetDialogBuy(FactorBuy factorBuy) {
        this.factorBuy = factorBuy;
    }

    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_dialog_buy, container, false);
        binding.setItem(factorBuy);

        binding.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BuyActivity) getActivity()).startBuy(factorBuy.getKey());
                dismiss();
            }
        });
        return binding.getRoot();
    }


    public static class FactorBuy {
        private String title;
        private String price;
        private String titleItem;
        private String details;
        private String key;
        private String itemId;

        public void setItemId(String itemId) {
            this.itemId = itemId;
        }

        public String getItemId() {
            return itemId;
        }

        public String getTitle() {
            return "خرید شما : " + title;
        }
        public String getOrgTitle() {
            return  title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPrice() {
            return  "قیمت پرداختی : " + price+" تومان ";
        }
        public String getOrgPrice() {
            return    price;
        }

        public void setPrice(String price) {
            this.price =price;
        }

        public String getTitleItem() {
            return " اگهی انتخاب شده : " + titleItem;
        }

        public void setTitleItem(String titleItem) {
            this.titleItem = titleItem;
        }

        public String getDetails() {
            return " توضیحات خرید : " + details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }

}
