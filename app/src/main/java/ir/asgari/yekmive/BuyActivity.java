package ir.asgari.yekmive;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import ir.asgari.yekmive.adapter.AdapterBuy;
import ir.asgari.yekmive.model.ModelResultSendBuy;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import ir.asgari.yekmive.util.IabHelper;
import ir.asgari.yekmive.util.IabResult;
import ir.asgari.yekmive.databinding.ActivityBuyBinding;
import ir.asgari.yekmive.model.ModelBuy;
import ir.asgari.yekmive.view.LoginSms.SharePrefLogin;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuyActivity extends AppCompatActivity {
    ActivityBuyBinding buyBinding;
    public final int REQUEST_CODE_MYiTEM = 122;
    BottomSheetDialogBuy.FactorBuy factorBuy;
    // Debug tag, for logging
    static final String TAG = "bazzar";
    // SKUs for our products: the premium upgrade (non-consumable)
    static String SKU_PREMIUM = "testmahsol";
    // Does the user have the premium upgrade?
    boolean mIsPremium = false;
    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 111;
    // The helper object
    IabHelper mHelper;
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = (result, inventory) -> {
        Log.d(TAG, "Query inventory finished.");
        if (result.isFailure()) {
            Log.d(TAG, "Failed to query inventory: " + result);
            return;
        } else {
            Log.d(TAG, "Query inventory was successful.");
            // does the user have the premium upgrade?
            mIsPremium = inventory.hasPurchase(SKU_PREMIUM);
            // update UI accordingly
            Log.d(TAG, "User is " + (mIsPremium ? "PREMIUM" : "NOT PREMIUM"));
        }
        Log.d(TAG, "Initial inventory query finished; enabling main UI.");
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = (result, purchase) -> {
        if (result.isFailure()) {
            Log.d(TAG, "Error purchasing: " + result);
            return;
        } else if (purchase.getSku().equals(SKU_PREMIUM)) {
            // give user access to premium content and update the UI
            sendBuy(result.toString());
        }
    };

    public void sendBuy(String response) {
        int nardebaan = 0;
        int vizhe = 0;
        int fori = 0;

        if (factorBuy.getOrgTitle().equals("nardebaan")) {
            nardebaan = 1;
        }
        if (factorBuy.getOrgTitle().equals("fori")) {
            fori = 1;
        }
        if (factorBuy.getOrgTitle().equals("vizhe")) {
            vizhe = 1;
        }
        SharePrefHelper pre = new SharePrefHelper(this);

        Log.i("abbbbb",
                "title " + factorBuy.getOrgTitle() +
                        "1" + " | " +
                        pre.getAccountID() + " | " +
                        pre.getAccessToken() + " | " +
                        factorBuy.getItemId() + " | " +
                        factorBuy.getPrice() + " | " +
                        response.toString() + " | " +
                        "ir.asgari.yekmive" + " | " +
                        nardebaan + " | " +
                        fori + " | " +
                        vizhe
        );

        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        serverRequest.sendBuy("1",
                pre.getAccountID(),
                pre.getAccessToken(),
                factorBuy.getItemId(),
                factorBuy.getPrice(),
                response,
                "ir.asgari.yekmive",
                nardebaan + "",
                fori + "",
                vizhe + ""
        ).enqueue(new Callback<ModelResultSendBuy>() {
            @Override
            public void onResponse(Call<ModelResultSendBuy> call, Response<ModelResultSendBuy> response) {
                if (!response.body().getError())
                    Toast.makeText(BuyActivity.this, "خرید با موفقیت انحام شد", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(BuyActivity.this, "خطا ! لطفا دوباره تلاش کنید", Toast.LENGTH_SHORT).show();

                Log.i("abbbb", response.body().getError().toString());
            }

            @Override
            public void onFailure(Call<ModelResultSendBuy> call, Throwable t) {
                Toast.makeText(BuyActivity.this, "خطا ! لطفا دوباره تلاش کنید", Toast.LENGTH_SHORT).show();
                Log.i("abbbbe", t.getMessage());
            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        buyBinding = DataBindingUtil.setContentView(this, R.layout.activity_buy);
        factorBuy = new BottomSheetDialogBuy.FactorBuy();
        String base64EncodedPublicKey = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwCmo6HiZoeknXQ5BPf8M17SRNhRJJqepF5u8UapehEbz7Wzn0Hq74xqo7X25FYLbRVcO+NEfkKAIzzTt/4fGG6f7DV2t+Yunmm7CLBEffaLKNPQp9oENK/4gQzV3twKIRfX4cfP26kRJ6LlvhLP6Rt5peFaNkLHfkug5HxUyEGS7BMwcr6nCFWkKzKQVgRMZGe0Vv2lc77ZPHWtEdbPGLQltYo37p9NSHk0P/oSr98CAwEAAQ==";
        // You can find it in your Bazaar console, in the Dealers section.
        // It is recommended to add more security than just pasting it in your source code;
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        Log.d(TAG, "Starting setup.");
        getItemAdapter();
        mHelper.startSetup(result -> {
            Log.d(TAG, "Setup finished.");
            if (!result.isSuccess()) {
                // Oh noes, there was a problem.
                Log.d(TAG, "Problem setting up In-app Billing: " + result);
            }
            // Hooray, IAB is fully set up!
            mHelper.queryInventoryAsync(mGotInventoryListener);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    AdapterBuy adapterBuy;

    public void getItemAdapter() {
        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        serverRequest.getBuyPlan("1").enqueue(new Callback<List<ModelBuy>>() {
            @Override
            public void onResponse(Call<List<ModelBuy>> call, Response<List<ModelBuy>> response) {
                if (response.body() != null) {
                    Log.i("myPlan", response.body().toString());
                    adapterBuy = new AdapterBuy(response.body(), BuyActivity.this, buy -> {
                        factorBuy.setTitle(buy.getName());
                        factorBuy.setDetails(buy.getDetilas());
                        factorBuy.setPrice(buy.getPrice());
                        factorBuy.setKey(buy.getKey());
                        startActivityForResult(new Intent(BuyActivity.this, MyItemActivityBuy.class), REQUEST_CODE_MYiTEM);
                    });

                    buyBinding.tv.setOnClickListener(v -> {

                    });

                    buyBinding.setAdapter(adapterBuy);
                }
            }

            @Override
            public void onFailure(Call<List<ModelBuy>> call, Throwable t) {
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_CODE_MYiTEM && resultCode == RESULT_OK) {
                factorBuy.setTitleItem(data.getStringExtra("title"));
                factorBuy.setItemId(data.getStringExtra("id"));
                new BottomSheetDialogBuy(factorBuy).show(getSupportFragmentManager(), "");
            }
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    public void startBuy(String key) {
        if (key.equals("nardebaan")) {
            sendBuy("free");
        } else {
            SKU_PREMIUM = key;
            mHelper.launchPurchaseFlow(BuyActivity.this, SKU_PREMIUM, RC_REQUEST, mPurchaseFinishedListener, "");
        }

    }
}