package ir.asgari.yekmive;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import ir.asgari.yekmive.app.App;
import ir.asgari.yekmive.common.ActivityBase;
import ir.asgari.yekmive.dialogs.MsgImageChooseDialog;
import ir.asgari.yekmive.dialogs.ProfileBlockDialog;

public class ChatActivity extends ActivityBase implements MsgImageChooseDialog.AlertPositiveListener, ProfileBlockDialog.AlertPositiveListener {

    Toolbar mToolbar;

    ChatFragment fragment;
    Boolean restore = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dialogs);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setOnClickListener(v -> {
            long itemId = Long.parseLong(getIntent().getStringExtra("itemId"));
            if (itemId == 0) {
                Intent intent = new Intent(ChatActivity.this, ProfileActivity.class);
                intent.putExtra("profileId", getIntent().getLongExtra("profileId", 0));
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, ViewItemActivity.class);
                intent.putExtra("itemId", itemId);
                startActivity(intent);
            }
        });

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        if (savedInstanceState != null) {
            fragment = (ChatFragment) getSupportFragmentManager().getFragment(savedInstanceState, "currentFragment");
            restore = savedInstanceState.getBoolean("restore");
        } else {
            fragment = new ChatFragment();

            restore = false;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container_body, fragment)
                .commit();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        outState.putBoolean("restore", true);
        getSupportFragmentManager().putFragment(outState, "currentFragment", fragment);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    protected void onPause() {

        ChatFragment p = (ChatFragment) fragment;
        p.hideEmojiKeyboard();

        super.onPause();
    }

    @Override
    protected void onStop() {

        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        fragment.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {

            case android.R.id.home: {

                App.getInstance().setCurrentChatId(0);

                finish();

                return true;
            }

            default: {


                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onBackPressed() {

        ChatFragment p = (ChatFragment) fragment;
        p.hideEmojiKeyboard();

        App.getInstance().setCurrentChatId(0);

        finish();
    }

    @Override
    public void onImageFromGallery() {
        ChatFragment p = (ChatFragment) fragment;
        p.imageFromGallery();
    }

    @Override
    public void onImageFromCamera() {

        ChatFragment p = (ChatFragment) fragment;
        p.imageFromCamera();
    }

    @Override
    public void onProfileBlock() {
        fragment.onProfileBlock();
    }
}