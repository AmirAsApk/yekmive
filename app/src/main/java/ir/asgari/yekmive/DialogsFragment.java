package ir.asgari.yekmive;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.asgari.yekmive.adapter.DialogListAdapter;
import ir.asgari.yekmive.app.App;
import ir.asgari.yekmive.constants.Constants;
import ir.asgari.yekmive.model.Chat;
import ir.asgari.yekmive.model.ModelChatItem;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import ir.asgari.yekmive.utilorg.CustomRequest;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DialogsFragment extends Fragment implements Constants, SwipeRefreshLayout.OnRefreshListener {

    private static final String STATE_LIST = "State Adapter Data";

    RecyclerView mListView;
    TextView mMessage;
    ImageView mSplash;

    SwipeRefreshLayout mItemsContainer;

    private List<ModelChatItem.Chat> chatsList;
    private DialogListAdapter chatsAdapter;

    private int itemId = 0;
    private int arrayLength = 0;
    private Boolean loadingMore = false;
    private Boolean viewMore = false;
    private Boolean restore = false;
    LinearLayoutManager layoutManager;

    public DialogsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        chatsList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        chatsAdapter = new DialogListAdapter((AppCompatActivity) getActivity(), chatsList);


        if (savedInstanceState != null) {
            restore = savedInstanceState.getBoolean("restore");
            itemId = savedInstanceState.getInt("itemId");
        } else {
            chatsAdapter = new DialogListAdapter((AppCompatActivity) getActivity(), chatsList);
            restore = false;
            itemId = 0;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_dialogs, container, false);

        mItemsContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.container_items);
        mItemsContainer.setOnRefreshListener(this);

        mMessage = rootView.findViewById(R.id.message);
        mSplash = (ImageView) rootView.findViewById(R.id.splash);

        mListView = rootView.findViewById(R.id.listView);
        mListView.setLayoutManager(layoutManager);
        mListView.setAdapter(chatsAdapter);
        chatsAdapter.setClick(position -> {
            ModelChatItem.Chat chat = chatsList.get(position);
            Intent intent = new Intent(getActivity(), ChatActivity.class);
            intent.putExtra("position", position);
            intent.putExtra("chatId", Integer.parseInt(chat.getId()));
            intent.putExtra("profileId", Long.valueOf(chat.getToUserId()));
            intent.putExtra("withProfile", chat.getWithUserFullname());
            intent.putExtra("blocked", false);
            intent.putExtra("fromUserId", chat.getFromUserId());
            intent.putExtra("toUserId", chat.getToUserId());
            intent.putExtra("withUserName", chat.getWithUserUsername());
            intent.putExtra("itemId", chat.getToItemId());
            startActivityForResult(intent, VIEW_CHAT);

            chat.setNewMessagesCount(chat.getNewMessagesCount());

            if (App.getInstance().getMessagesCount() > 0) {

                App.getInstance().setMessagesCount(App.getInstance().getMessagesCount() - 1);
            }

            chatsAdapter.notifyDataSetChanged();
        });


        if (chatsAdapter.getItemCount() == 0) {
            showMessage(getText(R.string.label_empty_list).toString());
        } else {
            hideMessage();
        }

        if (!restore) {
            showMessage(getText(R.string.msg_loading_2).toString());
            getConversations();
        }


        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onRefresh() {
        if (App.getInstance().isConnected()) {
            itemId = 0;
            getConversations();
        } else {
            mItemsContainer.setRefreshing(false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VIEW_CHAT && resultCode == getActivity().RESULT_OK && null != data) {
            int pos = data.getIntExtra("position", 0);
            Toast.makeText(getActivity(), getString(R.string.msg_chat_has_been_removed), Toast.LENGTH_SHORT).show();
            chatsList.remove(pos);
            chatsAdapter.notifyDataSetChanged();
            if (chatsAdapter.getItemCount() == 0) {
                showMessage(getText(R.string.label_empty_list).toString());
            } else {
                hideMessage();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("restore", true);
        outState.putInt("itemId", itemId);
//        outState.putParcelableArrayList(STATE_LIST, chatsList);
    }

    public void getConversations() {
        mItemsContainer.setRefreshing(true);
        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        serverRequest.getItemChat(App.getInstance().getId() + "",
                App.getInstance().getAccessToken(),
                String.valueOf(itemId),
                "en").enqueue(new Callback<ModelChatItem>() {
            @Override
            public void onResponse(Call<ModelChatItem> call, Response<ModelChatItem> response) {
                Log.e("chattttttt", "res");
                itemId = Integer.parseInt(response.body().getItemId());
                chatsList = response.body().getChats();
                chatsAdapter.setChatList(chatsList);
                chatsAdapter.notifyDataSetChanged();
                loadingComplete();
            }

            @Override
            public void onFailure(Call<ModelChatItem> call, Throwable t) {
                Log.e("chattttttt", "error");

            }
        });

//        CustomRequest jsonReq = new CustomRequest(Request.Method.POST, METHOD_DIALOGS_GET, null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.i("chatTest", response.toString());
//
//                        if (!isAdded() || getActivity() == null) {
//                            Log.e("ERROR", "DialogsFragment Not Added to Activity");
//                            return;
//                        }
//                        try {
//                            arrayLength = 0;
//                            if (!loadingMore) {
//                                chatsList.clear();
//                            }
//                            if (!response.getBoolean("error")) {
//                                itemId = response.getInt("itemId");
//                                if (response.has("chats")) {
//                                    JSONArray chatsArray = response.getJSONArray("chats");
//                                    arrayLength = chatsArray.length();
//                                    if (arrayLength > 0) {
//                                        for (int i = 0; i < chatsArray.length(); i++) {
//                                            JSONObject chatObj = (JSONObject) chatsArray.get(i);
//                                            Chat chat = new Chat(chatObj);
//                                            chatsList.add(chat);
//                                        }
//                                    }
//                                }
//                            }
//
//                        } catch (JSONException e) {
//
//                            loadingComplete();
//
//                            e.printStackTrace();
//
//                        } finally {
//                            loadingComplete();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                if (!isAdded() || getActivity() == null) {
//                    Log.e("ERROR", "DialogsFragment Not Added to Activity");
//                    return;
//                }
//                loadingComplete();
//            }
//        }) {
//
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("accountId", Long.toString(App.getInstance().getId()));
//                params.put("accessToken", App.getInstance().getAccessToken());
//                params.put("itemId", Integer.toString(itemId));
//                params.put("language", "en");
//                return params;
//            }
//        };
//
//        App.getInstance().addToRequestQueue(jsonReq);
    }

    public void loadingComplete() {

        if (arrayLength == LIST_ITEMS) {

            viewMore = true;

        } else {

            viewMore = false;
        }

        chatsAdapter.notifyDataSetChanged();

        if (chatsAdapter.getItemCount() == 0) {

            showMessage(getText(R.string.label_empty_list).toString());

        } else {

            hideMessage();
        }

        loadingMore = false;
        mItemsContainer.setRefreshing(false);
    }

    public void showMessage(String message) {

        mMessage.setText(message);
        mMessage.setVisibility(View.VISIBLE);

        mSplash.setVisibility(View.VISIBLE);
    }

    public void hideMessage() {

        mMessage.setVisibility(View.GONE);

        mSplash.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}