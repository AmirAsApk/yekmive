package ir.asgari.yekmive;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ir.asgari.yekmive.databinding.FragmentEditProfileBinding;
import ir.asgari.yekmive.model.ModelEditProfile;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import ir.asgari.yekmive.view.BaseFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentEditProfile extends BaseFragment<FragmentEditProfileBinding> {

    SharePrefHelper prefHelper;
    ServerRequest serverRequest;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        prefHelper = new SharePrefHelper(getContext());

        if (!prefHelper.isLogin()) {
            ((ActivityViewItem) getActivity()).setFragment(new NotLoginFragment());
        } else {
            serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
            Call<ModelGetProfile> call = serverRequest.getProfile(1 + "", prefHelper.getAccountID(), prefHelper.getAccessToken());
            call.enqueue(new Callback<ModelGetProfile>() {
                @Override
                public void onResponse(Call<ModelGetProfile> call, Response<ModelGetProfile> response) {
                    if (response.body() != null) {
                        ModelGetProfile.Account account = response.body().getAccount().get(0);
                        binding.setItem(account);
                    }
                }

                @Override
                public void onFailure(Call<ModelGetProfile> call, Throwable t) {

                }
            });
        }

        binding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.save.setAlpha(0.5f);
                binding.save.setEnabled(false);
                ModelGetProfile.Account account = binding.getItem();
                Log.i("account",""+account.getStatus());
                Call<ModelEditProfile> call = serverRequest.setProfile(1 + "", prefHelper.getAccountID(), prefHelper.getAccessToken()
                        , account.getStatus(), account.getFullname(), account.getFbPage(), account.getInstagramPage(), account.getPhone());
                call.enqueue(new Callback<ModelEditProfile>() {
                    @Override
                    public void onResponse(Call<ModelEditProfile> call, Response<ModelEditProfile> response) {
                        if (!response.body().getError()) {
                            Toast.makeText(getContext(), "با موفقیت دخیره شد", Toast.LENGTH_SHORT).show();
                            getActivity().finish();
                        }else {
                            binding.save.setAlpha(0.5f);
                            binding.save.setEnabled(false);

                        }
                    }

                    @Override
                    public void onFailure(Call<ModelEditProfile> call, Throwable t) {
                        binding.save.setAlpha(0.5f);
                        binding.save.setEnabled(false);

                    }
                });
            }
        });


        return binding.getRoot();
    }

    @Override
    public int getIdLayout() {
        return R.layout.fragment_edit_profile;
    }

}
