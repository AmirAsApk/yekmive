package ir.asgari.yekmive;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

import ir.asgari.yekmive.databinding.FragmentGetPerLocationBinding;
import ir.asgari.yekmive.view.BaseFragment;

public class FragmentGetPerLocation extends BottomSheetDialogFragment {

    FragmentGetPerLocationBinding binding;
    int status;

    public FragmentGetPerLocation(int status) {
        this.status = status;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_get_per_location, container, false);
        binding.close.setOnClickListener(v -> dismiss());

        if (status == 1) {
            binding.tv.setText("لطفا برای استفاده از لوکیشن خودکار دسترسی به لوکیشن را به یک میوه بدهید");
            binding.next.setOnClickListener(v -> requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1));
        } else if (status == 2) {
            binding.tv.setText("لطفا برای استفاده از لوکیشن خودکار لوکیشن خود را روشن کنید");
            binding.next.setOnClickListener(v -> {
                Intent viewIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(viewIntent);
                dismiss();
            });

        }
        return binding.getRoot();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull @NotNull String[] permissions, @NonNull @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (requestCode==1&&grantResults[0]== PackageManager.PERMISSION_GRANTED){
        dismiss();
    }
    }
}
