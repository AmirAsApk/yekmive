package ir.asgari.yekmive;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import ir.asgari.yekmive.databinding.FragmentViewImageBinding;
import ir.asgari.yekmive.view.BaseFragment;

public class FragmentViewImage extends BaseFragment<FragmentViewImageBinding> {
    String imUrl;

    public FragmentViewImage(String imUrl) {
        this.imUrl = imUrl;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Picasso.with(getContext()).load(imUrl).into(binding.im);
        return binding.getRoot();
    }

    @Override
    public int getIdLayout() {
        return R.layout.fragment_view_image;
    }
}
