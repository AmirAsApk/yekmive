package ir.asgari.yekmive;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import ir.asgari.yekmive.model.CountNewMassage;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import ir.asgari.yekmive.view.CategoryFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ir.asgari.yekmive.FragmentDrawer.RESULT_LOGIN;

public class HomeActivity extends AppCompatActivity {

    AHBottomNavigation bottomNavigation;
    SharePrefHelper prefHelper;
    FragmentDrawer fragmentDrawer;
    int id;
    DialogsFragment dialogsFragment;
    NotLoginFragment notLoginFragment;
    CategoryFragment categoryFragment;
    StreamFragment streamFragment;

    int countMassage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        prefHelper = new SharePrefHelper(this);


        bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.addItem(new AHBottomNavigationItem("اپ من", R.drawable.ic_baseline_person_24));
        bottomNavigation.addItem(new AHBottomNavigationItem("چت", R.drawable.ic_baseline_chat_24));
        bottomNavigation.addItem(new AHBottomNavigationItem("ثبت اگهی", R.drawable.ic_baseline_add_24));
        bottomNavigation.addItem(new AHBottomNavigationItem("دسته بندی", R.drawable.ic_action_categories));
        bottomNavigation.addItem(new AHBottomNavigationItem("خانه", R.drawable.ic_baseline_home_24));
        bottomNavigation.setAccentColor(getResources().getColor(R.color.colorAccent));
        bottomNavigation.setCurrentItem(4);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        dialogsFragment = new DialogsFragment();
        fragmentDrawer = new FragmentDrawer();
        notLoginFragment = new NotLoginFragment();
        categoryFragment = new CategoryFragment();
        streamFragment = new StreamFragment();

        fragmentDrawer.setDrawerListener((view, position) -> {
            Log.i("selectDrawer", position + "");
            displayView(position);
        });
        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        Call<CountNewMassage> call = serverRequest.getCountNewMassage("1", prefHelper.getAccountID(), prefHelper.getAccessToken());
        call.enqueue(new Callback<CountNewMassage>() {
            @Override
            public void onResponse(Call<CountNewMassage> call, Response<CountNewMassage> response) {
                if (response.body() != null && response.body().getMessagesCount() != null && response.body().getMessagesCount() > 0) {
                    if (!response.body().getError() && response.body().getMessagesCount() > 0) {
                        bottomNavigation.getItem(1).setDrawable(R.drawable.ic_new_chat);
                    }
                }
            }

            @Override
            public void onFailure(Call<CountNewMassage> call, Throwable t) {

            }
        });
        setFragment(new StreamFragment());
        bottomNavigation.setOnTabSelectedListener((position, wasSelected) -> {
            id = position;

            switch (position) {
                case 0:
                    setFragment(fragmentDrawer);
                    return true;
                case 1:
                    if (prefHelper.isLogin())
                        setFragment(dialogsFragment);
                    else
                        setFragment(notLoginFragment);
                    return true;
                case 2:

                    startActivity(new Intent(HomeActivity.this, NewItemActivity.class));

                    break;
                case 3:
                    setFragment(categoryFragment);
                    return true;
                case 4:
                    setFragment(streamFragment);
                    return true;
            }
            return false;
        });
    }

    void setFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, fragment);
        ft.commit();
    }

    private void displayView(int position) {
        if (position == 4) {
            startActivity(new Intent(this, SettingsActivity.class));
        } else {
            Intent intent = new Intent(HomeActivity.this, ActivityViewItem.class);
            intent.putExtra(ActivityViewItem.ID_LAYOUT, position);
            startActivity(intent);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOGIN && resultCode == Activity.RESULT_OK) {
            fragmentDrawer.reLoad();
        }
    }

}