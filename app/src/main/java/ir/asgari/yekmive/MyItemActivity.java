package ir.asgari.yekmive;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;

import ir.asgari.yekmive.adapter.MyItemAdapter;
import ir.asgari.yekmive.adapter.SearchListAdapter;
import ir.asgari.yekmive.databinding.ActivityMyItemBinding;
import ir.asgari.yekmive.model.ModelSearch;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyItemActivity extends AppCompatActivity {
    ActivityMyItemBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_item);
        getMyItems();
        binding.refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMyItems();
            }
        });
    }

    public void getMyItems() {
        binding.refresh.setRefreshing(true);
        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        SharePrefHelper pref = new SharePrefHelper(this);
        Log.i("mymymy", pref.getAccountID() + "|||" + pref.getAccessToken() + "|||" + pref.getProfileID());

        Call<ModelSearch> call = serverRequest.getMyItem(
                "1",
                "0",
                pref.getAccountID(),
                pref.getAccessToken(),
                pref.getProfileID(),
                pref.getUserName());

        call.enqueue(new Callback<ModelSearch>() {
            @Override
            public void onResponse(Call<ModelSearch> call, Response<ModelSearch> response) {
                if (response.body() != null && response.body().getItems() != null) {
                    if (response.body().getItems().size() == 0) {
                        binding.setMessage("شما اگهی ثبت نکردید ");
                    } else {
                        binding.setAdapter(new MyItemAdapter(MyItemActivity.this, response.body().getItems()));
                    }
                }
                binding.refresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ModelSearch> call, Throwable t) {
                binding.setMessage("خطا لطفا دوباره تلاش کنید ");
                binding.refresh.setRefreshing(false);
            }
        });
    }
}