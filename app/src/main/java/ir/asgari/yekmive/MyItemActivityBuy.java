package ir.asgari.yekmive;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.io.IOException;

import ir.asgari.yekmive.adapter.MyItemAdapterBuy;
import ir.asgari.yekmive.databinding.ActivityMyItemBuyBinding;
import ir.asgari.yekmive.model.ModelSearch;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyItemActivityBuy extends AppCompatActivity {
    ActivityMyItemBuyBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_item_buy);
        getMyItems();
        binding.refresh.setOnRefreshListener(() -> getMyItems());
    }

    public void getMyItems() {
        binding.refresh.setRefreshing(true);
        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        SharePrefHelper pref = new SharePrefHelper(this);
        Log.i("mymymy",
                "1" + "-" +
                        "0" + "-" +
                        pref.getAccountID() + "-" +
                        pref.getAccessToken() + "-" +
                        pref.getProfileID() + "-" +
                        pref.getUserName());

        Call<ModelSearch> call = serverRequest.getMyItem(
                "1",
                "0",
                pref.getAccountID(),
                pref.getAccessToken(),
                pref.getProfileID(),
                pref.getUserName());

        call.enqueue(new Callback<ModelSearch>() {
            @Override
            public void onResponse(Call<ModelSearch> call, Response<ModelSearch> response) {

                if (response.body() != null && response.body().getItems()!= null) {
                    if (response.body().getItems().size() == 0) {
                        binding.setMessage("شما اگهی ثبت نکردید ");
                    } else {
                        binding.setAdapter(new MyItemAdapterBuy(MyItemActivityBuy.this, response.body().getItems(), item -> {
                            Intent intent = new Intent();
                            intent.putExtra("id", item.getId());
                            intent.putExtra("title", item.getItemTitle());
                            setResult(RESULT_OK, intent);
                            finish();
                        }));
                    }
                }
                binding.refresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ModelSearch> call, Throwable t) {
                binding.setMessage("خطا لطفا دوباره تلاش کنید ");
                binding.refresh.setRefreshing(false);
            }
        });
    }
}