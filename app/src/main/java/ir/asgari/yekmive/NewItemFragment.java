package ir.asgari.yekmive;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import ir.asgari.yekmive.app.App;
import ir.asgari.yekmive.constants.Constants;
import ir.asgari.yekmive.databinding.FragmentNewItemBinding;
import ir.asgari.yekmive.model.ModelCategoryNewItem;
import ir.asgari.yekmive.model.ModelChildCategory;
import ir.asgari.yekmive.model.ModelNewItem;
import ir.asgari.yekmive.model.ModelUploadImage;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import ir.asgari.yekmive.view.BaseFragment;
import ir.asgari.yekmive.view.LoginSms.LoginSmsCodeActivity;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import saman.zamani.persiandate.PersianDate;

import static ir.asgari.yekmive.ActivityViewItem.ID_LAYOUT;

public class NewItemFragment extends BaseFragment<FragmentNewItemBinding> implements Constants, View.OnClickListener {

    public static int REQUEST_LOGIN = 1;
    public static int RQ_CODE_IMAGE = 10;
    public static int RQ_CODE_MAP = 111;
    List<String> list;
    SpinnerHelper spinnerHelper;
    int ostan, shahr, comment;
    double lat, lng;
    String title = "", description = "", minPrice = "", maxPrice;
    PreNewItemHelper newItemHelper;
    int idImage;
    int statusLocation = 0;
    SharePrefHelper pre;
    Map<Integer, String> images;
    String token;
    List<String> imageList;
    ServerRequest serverRequest;
    Intent nologin;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        images = new HashMap<>();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding.setClick(new ClickNewItem());
        pre = new SharePrefHelper(getActivity());
        newItemHelper = new PreNewItemHelper(getActivity());
        spinnerHelper = new SpinnerHelper(getActivity(), getContext());
        serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        imageList = new ArrayList<>();
        nologin = new Intent(getActivity(), ActivityViewItem.class);
        nologin.putExtra(ID_LAYOUT, ActivityViewItem.NotLogin);
        binding.itemPublish.setEnabled(false);
        gson = new Gson();
        binding.itemPublish.setAlpha(0.5f);
        categories = new ArrayList<>();
        childCategories = new ArrayList<>();
        token = getNameImage();
        categoryAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                android.R.id.text1);
        manageSpinner();

        binding.img1.setTag("0");
        binding.img2.setTag("0");
        binding.img3.setTag("0");
        binding.img4.setTag("0");

        binding.itemPublish.setOnClickListener(this);

        return binding.getRoot();
    }

    @Override
    public int getIdLayout() {
        return R.layout.fragment_new_item;
    }

    private void manageSpinner() {
        // Initialize Spinner | add categories list
        categoryAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                android.R.id.text1);
        binding.spinnerCategory.setAdapter(categoryAdapter);
        getCategory();
        binding.spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    category = Integer.parseInt(categories.get(position - 1).getId());
                    getChildCategory();
                } else {
                    category = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binding.spinnerChildCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    childCategory = Integer.parseInt(childCategories.get(position - 1).getId());
                } else {
                    childCategory = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinnerHelper.setupSpinnerOstan(binding.spinnerOtan);
        binding.spinnerOtan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (position != 0) {
                    ostan = Integer.parseInt(spinnerHelper.getModelOstans().get(position - 1).getId());
                    newItemHelper.setostan(position);
                    spinnerHelper.setupSpinnerShahr(binding.spinnerShahr, String.valueOf(ostan));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        binding.spinnerShahr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    shahr = Integer.parseInt(spinnerHelper.getModelShahr().get(position - 1).getId());
                    newItemHelper.setshahr(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void publish() {
        if (App.getInstance().isConnected()) {
            title = binding.itemTitle.getText().toString().trim();
            description = binding.itemDescription.getText().toString().trim();
            minPrice = binding.minPrice.getText().toString().trim();
            maxPrice = binding.maxItemPrice.getText().toString().trim();
            Log.e("imagessss", gson.toJson(images.values()));
            if (!getError()) {
                if (App.getInstance().getId() != 0) {
                    sendItem();
                } else {
                    startActivityForResult(new Intent(getActivity(), LoginSmsCodeActivity.class), REQUEST_LOGIN);
                }
            }
        } else {
            Toast toast = Toast.makeText(getActivity(), getText(R.string.msg_network_error), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    private boolean getError() {
        boolean error = false;
        //check category is not empty
        if (category == 0) {
            binding.categoryError.setVisibility(View.VISIBLE);
            error = true;
        } else {
            binding.categoryError.setVisibility(View.GONE);
        }
        //check child category is not empty
        if (childCategory == 0) {
            binding.childCategoryError.setVisibility(View.VISIBLE);
            error = true;
        } else {
            binding.childCategoryError.setVisibility(View.GONE);
        }
        //check ostan is not empty
        if (ostan == 0) {
            binding.ostanContainer.setVisibility(View.VISIBLE);
            error = true;
        } else {
            binding.ostanError.setVisibility(View.GONE);
        }
        //check shahr is not empty
        if (shahr == 0) {
            binding.shahrError.setVisibility(View.VISIBLE);
            error = true;
        } else {
            binding.shahrError.setVisibility(View.GONE);
        }
        //check title is not empty
        if (title.isEmpty()) {
            binding.titleError.setVisibility(View.VISIBLE);
            error = true;
        } else {
            binding.titleError.setVisibility(View.GONE);
        }

        //check description is not empty
        if (description.isEmpty()) {
            binding.descriptionError.setVisibility(View.VISIBLE);
            error = true;
        } else {
            binding.descriptionError.setVisibility(View.GONE);
        }
        //check price is not empty
        if (minPrice.isEmpty()) {
            binding.minPriceError.setVisibility(View.VISIBLE);
            error = true;
        } else {
            binding.minPriceError.setVisibility(View.GONE);
        }
        //check price is not empty
        if (maxPrice.isEmpty()) {
            binding.maxPriceError.setVisibility(View.VISIBLE);
            error = true;
        } else {
            binding.maxPriceError.setVisibility(View.GONE);
        }

        if (images.values().size() == 0) {
            binding.imageError.setVisibility(View.VISIBLE);
            error = true;
        } else {
            binding.imageError.setVisibility(View.GONE);
        }

        return error;
    }

    public void sendItem() {
        binding.itemPublish.setAlpha(0.5f);
        binding.itemPublish.setEnabled(false);
        Log.i("newItem", "start");

        list = new ArrayList<String>(images.values());
        switch (list.size()) {
            case 0:
                list.add("");
            case 1:
                list.add("");
            case 2:
                list.add("");
            case 3:
                list.add("");
        }

        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        SharePrefHelper sharePrefHelper = new SharePrefHelper(getActivity());
        Call<ModelNewItem> call = serverRequest.sendNewItem(
                1,
                sharePrefHelper.getAccountID(),
                sharePrefHelper.getAccessToken(),
                token,
                String.valueOf(category),
                String.valueOf(childCategory),
                title,
                description,
                ostan,
                shahr,
                1,
                minPrice,
                maxPrice,
                comment,
                0,
                lat,
                lng,
                list.get(0),
                list.get(1),
                list.get(2),
                list.get(3)
        );

        call.enqueue(new retrofit2.Callback<ModelNewItem>() {
            @Override
            public void onResponse(Call<ModelNewItem> call, retrofit2.Response<ModelNewItem> response) {
                Log.i("newItem", response.body().getError() + "");
                if (!response.body().getError()) {
                    showDialogSuccess();
                } else {
                    Toast.makeText(getContext(), "خطا لطفا دوباره تلاش کنید", Toast.LENGTH_SHORT).show();
                    binding.itemPublish.setAlpha(1);
                    binding.itemPublish.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<ModelNewItem> call, Throwable t) {
                Log.i("newItem", t.getMessage());
                binding.itemPublish.setAlpha(1);
                binding.itemPublish.setEnabled(true);
            }
        });
    }

    private void showDialogSuccess() {
        TextView mTitle = new TextView(getContext());
        mTitle.setText("آگهی با موفقیت ثبت شد.");
        mTitle.setTextColor(Color.BLACK);
        mTitle.setGravity(Gravity.RIGHT);
        mTitle.setPadding(10, 40, 20, 0);
        mTitle.setTextSize(22);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCustomTitle(mTitle).
                setMessage("پس از تایید منتشر میشود.").
                setPositiveButton("تایید", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                }).show();
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.itemPublish) {
            if (pre.isLogin())
                publish();
            else {
                Intent nologin = new Intent(getActivity(), ActivityViewItem.class);
                nologin.putExtra(ID_LAYOUT, ActivityViewItem.NotLogin);
                startActivity(nologin);
            }
        }
    }

    public class ClickNewItem {
        public void clickImage(View v) {
            if (!pre.isLogin()) {
                startActivity(nologin);
            } else {
                if (v.getTag().equals("0")) {
                    idImage = v.getId();
                    v.setTag("1");
                    galleryClick();
                } else {
                    ImageView im = (ImageView) v;
                    im.setImageResource(R.drawable.ic_add_image);
                    v.setTag("0");
                    if (images.containsKey(v.getId()))
                        images.remove(v.getId());
                }
            }
        }

        public void clickLocation(View v) {
            Intent i = new Intent(getActivity(), OsmActivity.class);
            i.putExtra("status", 1);
            if (lat == 0 && lng == 0) {
                if (ostan != 0)
                    i.putExtra("city", spinnerHelper.getModelOstans().get(ostan - 1).getTitle());
                else {
                    i.putExtra("lat", 35.715298);
                    i.putExtra("lng", 51.404343);
                }
            } else {
                i.putExtra("lat", lat);
                i.putExtra("lng", lng);
            }
            startActivityForResult(i, RQ_CODE_MAP);
        }
    }

    private void galleryClick() {
        if (getPermissionGallery()) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, RQ_CODE_IMAGE);
        }
    }

    private boolean getPermissionGallery() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, RQ_CODE_IMAGE);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RQ_CODE_IMAGE && data != null) {
            String nameImage = getNameImage() + ".";
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                String type = getFileExtension(data.getData());
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                if (idImage != 0) {
                    ImageView imageView = binding.getRoot().findViewById(idImage);
                    imageView.setImageBitmap(selectedImage);
                    binding.lySendImage.setVisibility(View.VISIBLE);
                    binding.lySendImage2.setVisibility(View.VISIBLE);
                    uploadImage(getBytes(inputStream), type, nameImage + type, imageView);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == RQ_CODE_MAP && data != null) {
            lat = data.getDoubleExtra("lat", 0);
            lng = data.getDoubleExtra("lon", 0);
            binding.lyLocation.setBackgroundResource(R.drawable.bg_ok_location);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }
        }
    }

    public void uploadImage(byte[] bytes, String type, String name, ImageView imageView) {
        ServerRequest apiInterface = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), bytes);
        MultipartBody.Part file = MultipartBody.Part.createFormData("image", name, requestFile);
        Log.i("response_image", pre.getAccountID() + "|" + pre.getAccessToken());
        Call<ModelUploadImage> call = apiInterface.sendImage(file, token, "1", pre.getAccountID(), pre.getAccessToken());
        call.enqueue(new Callback<ModelUploadImage>() {
            @Override
            public void onResponse(Call<ModelUploadImage> call, Response<ModelUploadImage> response) {
                Log.i("response_image", response.body().getError() + "");
                Log.i("response_image", response.body().getImgUrl() + "");
                if (response.body().getError().equals("false")) {
                    images.put(imageView.getId(), response.body().getImgUrl());
                    binding.itemPublish.setEnabled(true);
                    binding.itemPublish.setAlpha(1);
                }
                binding.lySendImage.setVisibility(View.GONE);
                binding.lySendImage2.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ModelUploadImage> call, Throwable t) {
                Log.i("response_image", t.getMessage());
                binding.lySendImage.setVisibility(View.GONE);
                binding.lySendImage2.setVisibility(View.GONE);
            }
        });
    }

    private byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();
        int buffSize = 1024;
        byte[] buff = new byte[buffSize];
        int len = 0;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }
        return byteBuff.toByteArray();
    }

    private String getFileExtension(Uri uri) {
        ContentResolver cr = getActivity().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));
    }

    public String getNameImage() {
        PersianDate persianDate = new PersianDate();
        Date date = new Date();
        Random random = new Random();
        int ran = random.nextInt((1000000 - 10000) + 1) + 100000;
        return persianDate.getShYear() +
                String.valueOf(persianDate.getShMonth()) +
                persianDate.getShDay() +
                date.getHours() +
                date.getMinutes() +
                date.getSeconds() +
                ran;
    }

    public static void requestGPS(Context context) {
        Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        context.startActivity(intent);
    }

    int category, childCategory;
    List<ModelCategoryNewItem.Item> categories;
    List<ModelChildCategory.Item> childCategories;
    ArrayAdapter<String> categoryAdapter;
    Gson gson;

    private void getCategory() {
        serverRequest.getCategoryNewItem(Integer.parseInt(pre.getAccountID()), 0).enqueue(new Callback<ModelCategoryNewItem>() {
            @Override
            public void onResponse(Call<ModelCategoryNewItem> call, Response<ModelCategoryNewItem> response) {
                if (response.body() != null) {
                    categories = response.body().getItems();
                    Log.i("spinnerTest", response.body().toString());
                    categoryAdapter.add(getString(R.string.label_choice_category));
                    for (ModelCategoryNewItem.Item item : response.body().getItems()) {
                        categoryAdapter.add(item.getTitle());
                    }
                    categoryAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ModelCategoryNewItem> call, Throwable t) {
            }
        });
    }

    private void getChildCategory() {
        ArrayAdapter<String> childCategoryAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                android.R.id.text1);
        binding.spinnerChildCategory.setAdapter(childCategoryAdapter);
        serverRequest.getChildCategory(pre.getAccountID(), "0", category + "").enqueue(new Callback<ModelChildCategory>() {
            @Override
            public void onResponse(Call<ModelChildCategory> call, Response<ModelChildCategory> response) {
                if (response.body() != null) {
                    childCategories = response.body().getItems();
                    Log.i("spinnerTest", response.body().toString());
                    childCategoryAdapter.add(getString(R.string.label_choice_child_category));
                    for (ModelChildCategory.Item item : response.body().getItems()) {
                        childCategoryAdapter.add(item.getChildcategoriesTag());
                    }
                    childCategoryAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ModelChildCategory> call, Throwable t) {
            }
        });

    }
}