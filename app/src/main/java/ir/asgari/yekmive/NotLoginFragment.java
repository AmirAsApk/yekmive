package ir.asgari.yekmive;

import android.app.Activity;
import android.content.Intent;
import android.database.CursorJoiner;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.share.widget.GameRequestDialog;

import javax.xml.transform.Result;

import ir.asgari.yekmive.view.LoginSms.LoginSmsCodeActivity;

public class NotLoginFragment extends Fragment {

    TextView tv1, tv2;
    Button btn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_not_login, container, false);
        btn = view.findViewById(R.id.login_btn);
        tv1 = view.findViewById(R.id.textView7);
        tv2 = view.findViewById(R.id.textView8);
        btn.setOnClickListener(v -> startActivityForResult(new Intent(getActivity(), LoginSmsCodeActivity.class), 1));

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            tv1.setText("موفق");
            tv2.setText("شما با موفقیت وارد شدید ");
            btn.setText("ادامه");
            btn.setOnClickListener(v -> getActivity().finish());
            btn.setTextColor(getActivity().getResources().getColor(R.color.white));
            btn.setBackgroundResource(R.drawable.bg_ok_location);
        }
    }
}