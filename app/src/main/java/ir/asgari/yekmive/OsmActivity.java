package ir.asgari.yekmive;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.config.IConfigurationProvider;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.Map;

import static org.osmdroid.tileprovider.util.StorageUtils.getStorage;

public class OsmActivity extends AppCompatActivity {
    private MapView map;
    private IMapController mapController;
    LocationManager locationManager;
    ProgressBar pg;
    boolean location_b;
    double lat, lon;
    String nameCity;
    Marker startMarker;
    int status = 0;
    TextView tv;
    FloatingActionButton btn;
    FloatingActionButton btn_my_location;

    private static final String TAG = "OsmActivity";

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = getApplicationContext();
        setContentView(R.layout.activity_osm);
        IConfigurationProvider provider = Configuration.getInstance();
        provider.setUserAgentValue(BuildConfig.APPLICATION_ID);
        provider.setOsmdroidBasePath(getStorage());
        provider.setOsmdroidTileCache(getStorage());

        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        tv=findViewById(R.id.tv);
        btn=findViewById(R.id.btn);
        btn_my_location=findViewById(R.id.btn_my_location);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        pg = findViewById(R.id.progress_bar);
        map = findViewById(R.id.mapView);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        mapController = map.getController();
        mapController.setZoom(15);

        Intent i = getIntent();
        status=i.getIntExtra("status",0);

        if (status==1){
            if (i.hasExtra("city")) {
                status = 1;
                nameCity = i.getStringExtra("city");
                setLatLngWithCity();
            } else if (i.hasExtra("lat") && i.hasExtra("lng")) {
                lat = i.getDoubleExtra("lat", 0d);
                lon = i.getDoubleExtra("lng", 0d);
                setLatLngWithNumber();
            }
        }else if (status==2){
            btn.setVisibility(View.GONE);
            btn_my_location.setVisibility(View.GONE);
            tv.setVisibility(View.GONE);
            lat = i.getDoubleExtra("lat", 0d);
            lon = i.getDoubleExtra("lng", 0d);
            setLatLngWithNumber();
        }


        GpsMyLocationProvider prov = new GpsMyLocationProvider(getApplicationContext());
        prov.addLocationSource(LocationManager.NETWORK_PROVIDER);
        MyLocationNewOverlay locationOverlay = new MyLocationNewOverlay(prov, map);
        locationOverlay.enableMyLocation();
        map.getOverlayManager().add(locationOverlay);

        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent data = new Intent();
                lat = map.getMapCenter().getLatitude();
                lon = map.getMapCenter().getLongitude();

                data.putExtra("lat", lat);
                data.putExtra("lon", lon);
                setResult(RESULT_OK, data);
                finish();
            }
        });

        findViewById(R.id.btn_my_location).setOnClickListener(view -> {
            location_b = true;
            if (ActivityCompat.checkSelfPermission(OsmActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(OsmActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                new FragmentGetPerLocation(1).show(getSupportFragmentManager(), "");

            } else if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                new FragmentGetPerLocation(2).show(getSupportFragmentManager(), "");
            } else {
                pg.setVisibility(View.VISIBLE);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
                    @Override
                    public void onLocationChanged(@NonNull Location location) {
                        if (location_b) {
                            pg.setVisibility(View.GONE);
                            lat = location.getLatitude();
                            lon = location.getLongitude();
                            location_b = false;
                            final GeoPoint startPoint1 = new GeoPoint(location.getLatitude(), location.getLongitude());
                            mapController.setCenter(startPoint1);
                        }
                    }
                });
            }
        });
    }

    private void setLatLngWithNumber() {
        final GeoPoint pointer = new GeoPoint(lat, lon);
        mapController.setCenter(pointer);
    }

    private void setLatLngWithCity() {
        if (nameCity != null && nameCity.isEmpty()) return;

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                "https://nominatim.openstreetmap.org/search?format=json&q=" + nameCity
                , response -> {
            try {
                Log.i("resssponse", response);
                JSONArray jsona = new JSONArray(response);
                if (jsona.length() != 0) {
                    JSONObject json = (JSONObject) jsona.get(0);
                    if (json.has("lat") && json.has("lon")) {
                        double lat = Double.parseDouble(json.getString("lat"));
                        double lon = Double.parseDouble(json.getString("lon"));
                        Log.i("resssponse", lat + "," + lon);
                        final GeoPoint startPoint1 = new GeoPoint(lat, lon);
                        mapController.setCenter(startPoint1);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }, error -> {

        }) {
            @Nullable
            @org.jetbrains.annotations.Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }
        };

        queue.add(stringRequest);

    }

    public void onResume() {
        super.onResume();
        if (map != null)
            map.onResume();
    }

    public void onPause() {
        super.onPause();
        if (map != null)
            map.onPause();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
        }
    }

}
