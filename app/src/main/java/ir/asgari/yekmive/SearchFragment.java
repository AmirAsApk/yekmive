package ir.asgari.yekmive;

import android.app.Activity;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.asgari.yekmive.adapter.SearchListAdapter;
import ir.asgari.yekmive.databinding.FragmentSearchBinding;
import ir.asgari.yekmive.model.ModelCategoryNewItem;
import ir.asgari.yekmive.model.ModelChildCategory;
import ir.asgari.yekmive.model.ModelOstan;
import ir.asgari.yekmive.model.ModelResultSendBuy;
import ir.asgari.yekmive.model.ModelSearch;
import ir.asgari.yekmive.model.ModelShahr;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import ir.asgari.yekmive.view.BaseFragment;
import ir.asgari.yekmive.view.LoginSms.SharePrefLogin;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ir.asgari.yekmive.PaginationListener.PAGE_START;

public class SearchFragment extends BaseFragment<FragmentSearchBinding> {
    SearchListAdapter adapter;
    List<ModelSearch.Item> items;
    SpinnerHelper spinnerHelper;
    int ostan, shahr;
    int category, childCategory;
    List<ModelCategoryNewItem.Item> categories;
    List<ModelChildCategory.Item> childCategories;
    List<ModelOstan.Item> ostans;
    List<ModelShahr.Item> shahrs;

    ArrayAdapter<String> categoryAdapter;
    ArrayAdapter<String> childCategoryAdapter;
    ArrayAdapter<String> ostanAdapter;
    ArrayAdapter<String> shahrAdapter;

    RecyclerView mRecyclerView;
    boolean status = false;
    private int itemId = 0;
    private int arrayLength = 0;
    private Boolean loadingMore = false;
    private Boolean viewMore = false;
    private Boolean restore = false;
    private int currentPage = PAGE_START;
    private boolean isLoading = false;
    ServerRequest serverRequest;
    SharePrefHelper pre;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding.setClick(new SearchClick());
        binding.setText("");
        categories = new ArrayList<>();
        childCategories = new ArrayList<>();
        ostans = new ArrayList<>();
        shahrs = new ArrayList<>();

        items = new ArrayList<>();
        adapter = new SearchListAdapter(getActivity(), items);

        categoryAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, android.R.id.text1);

        childCategoryAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, android.R.id.text1);

        shahrAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                android.R.id.text1);

        ostanAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                android.R.id.text1);

        pre = new SharePrefHelper(getContext());

        serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        binding.spinnerOstan.setAdapter(ostanAdapter);
        binding.spinnerShahr.setAdapter(shahrAdapter);
        binding.spinnerCategory.setAdapter(categoryAdapter);
        binding.spinnerChildCategory.setAdapter(childCategoryAdapter);

        ModelFilter modelFilter = new ModelFilter();
        spinnerHelper = new SpinnerHelper(getActivity(), getContext());
        binding.setModel(modelFilter);
        binding.containerItems.setOnRefreshListener(() -> getItemSearch());


        mRecyclerView = binding.recyclerView;
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnBottomReachedListener(new SearchListAdapter.onBottomReachedListener() {
            @Override
            public void onBottomReached(int position) {
                //End of list
                isLoading = true;
                currentPage++;
                doApiCall();

            }
        });

        getOstan();
        getCategory();


        binding.spinnerShahr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    shahr = Integer.parseInt(shahrs.get(position - 1).getId());
                } else {
                    shahr = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        binding.spinnerOstan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    ostan = Integer.parseInt(ostans.get(position - 1).getId());
                    getCity();
                } else {
                    ostan = 0;
                    shahrs.clear();
                    shahrAdapter.clear();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    category = Integer.parseInt(categories.get(position - 1).getId());
                    getChildCategory();
                } else {
                    category = 0;
                    childCategoryAdapter.clear();
                    childCategories.clear();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerChildCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    childCategory = Integer.parseInt(childCategories.get(position - 1).getId());
                } else {
                    childCategory = 0;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        return binding.getRoot();
    }

    private void doApiCall() {
        Log.e("aaaaaaaa", "getttttttt");
        itemId++;
        getItemSearch();
        isLoading = false;
    }

    private void getCategory() {
        serverRequest.getCategoryNewItem(Integer.parseInt(pre.getAccountID()), 0).enqueue(new Callback<ModelCategoryNewItem>() {
            @Override
            public void onResponse(Call<ModelCategoryNewItem> call, Response<ModelCategoryNewItem> response) {
                if (response.body() != null) {
                    categories = response.body().getItems();
                    categoryAdapter.add(getString(R.string.label_choice_category));
                    for (ModelCategoryNewItem.Item item : response.body().getItems()) {
                        categoryAdapter.add(item.getTitle());
                    }
                    categoryAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<ModelCategoryNewItem> call, Throwable t) {

            }
        });
    }

    private void getChildCategory() {
        childCategories.clear();
        childCategoryAdapter.clear();
        binding.spinnerChildCategory.setAdapter(childCategoryAdapter);
        serverRequest.getChildCategory(pre.getAccountID(), "0", category + "").enqueue(new Callback<ModelChildCategory>() {
            @Override
            public void onResponse(Call<ModelChildCategory> call, Response<ModelChildCategory> response) {
                if (response.body() != null) {
                    childCategories = response.body().getItems();
                    Log.i("spinnerTest", response.body().toString());
                    childCategoryAdapter.add(getString(R.string.label_choice_child_category));
                    for (ModelChildCategory.Item item : response.body().getItems()) {
                        childCategoryAdapter.add(item.getChildcategoriesTag());
                    }
                    childCategoryAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ModelChildCategory> call, Throwable t) {
            }
        });
    }

    private void getOstan() {
        serverRequest.getOstan("1").enqueue(new Callback<ModelOstan>() {
            @Override
            public void onResponse(Call<ModelOstan> call, Response<ModelOstan> response) {
                if (response.body() != null) {
                    ostans = response.body().getItems();
                    ostanAdapter.add(getString(R.string.label_choice_ostan));
                    for (ModelOstan.Item item : response.body().getItems()) {
                        ostanAdapter.add(item.getTitle());
                    }
                    ostanAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ModelOstan> call, Throwable t) {
            }
        });
    }

    private void getCity() {
        shahrs.clear();
        shahrAdapter.clear();
        serverRequest.getShahrWithOstan(ostan + "").enqueue(new Callback<ModelShahr>() {
            @Override
            public void onResponse(Call<ModelShahr> call, Response<ModelShahr> response) {
                if (response.body() != null) {
                    shahrs = response.body().getItems();
                    Log.i("spinnerTest", response.body().toString());
                    shahrAdapter.add(getString(R.string.label_choice_shahr));
                    for (ModelShahr.Item item : response.body().getItems()) {
                        shahrAdapter.add(item.getTitle());
                    }
                    shahrAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ModelShahr> call, Throwable t) {
            }
        });
    }

    public void getItemSearch() {
        if (status) {
            binding.progressNewItem.setVisibility(View.VISIBLE);
        }


        Log.e("search", "category : " + category + " childCategory : " + childCategory + " ostan : " + ostan + " shahr : " + shahr);
        binding.setMassage("");
        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        Call<ModelSearch> call = serverRequest.getSearch(
                binding.getText(),
                String.valueOf(itemId),
                "",
                String.valueOf(childCategory),
                "",
                ostan + "",
                binding.etMinPrice.getText().toString().isEmpty() ? "0" : binding.etMinPrice.getText().toString(),
                binding.etMaxPrice.getText().toString().isEmpty() ? "0" : binding.etMaxPrice.getText().toString(),
                "");

        call.enqueue(new Callback<ModelSearch>() {
            @Override
            public void onResponse(Call<ModelSearch> call, Response<ModelSearch> response) {
                if (!response.body().getError()) {
                    if (!status) status = true;
                    binding.progressNewItem.setVisibility(View.GONE);
                    itemId = Integer.parseInt(response.body().getItemId());
                    if (response.body().getItems() != null) {
                        items.addAll(response.body().getItems());
                        adapter.setItemList(items);
                        adapter.notifyDataSetChanged();
                        binding.containerItems.setRefreshing(false);
                    }
                }
                binding.containerItems.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ModelSearch> call, Throwable t) {
                Log.i("searchQ", t.getMessage());
                binding.setMassage("خطا!! لطفا دوباره تلاش کنید ");
                binding.containerItems.setRefreshing(false);
            }
        });

    }

    @Override
    public int getIdLayout() {
        return R.layout.fragment_search;
    }

    public class SearchClick {
        public void filter_click(View v) {
            if (binding.layoutSearch.getVisibility() == View.VISIBLE) {
                binding.imFilter.setRotation(180);
                binding.layoutSearch.setVisibility(View.GONE);
            } else {
                binding.imFilter.setRotation(0);
                binding.layoutSearch.setVisibility(View.VISIBLE);
            }
        }

        public void search_click(View v) {
            hideKeyboard(getActivity());
            Log.i("searchLog", shahr + "" + ostan);
            items.clear();
            adapter.notifyDataSetChanged();
            binding.containerItems.setRefreshing(true);
            getItemSearch();
            binding.imFilter.setRotation(180);
            binding.layoutSearch.setVisibility(View.GONE);
        }

        public void search_delete(View v) {
            binding.spinnerShahr.setSelection(0);
            binding.spinnerOstan.setSelection(0);
            binding.spinnerCategory.setSelection(0);
            binding.spinnerChildCategory.setSelection(0);
            binding.etMinPrice.setText("");
            binding.etMaxPrice.setText("");
        }
    }


    public class ModelFilter {
        int category;
        int city;
        String minPrice;
        String maxPrice;

        public int getCategory() {
            return category;
        }

        public void setCategory(int category) {
            this.category = category;
        }

        public int getCity() {
            return city;
        }

        public void setCity(int city) {
            this.city = city;
        }

        public String getMinPrice() {
            return minPrice;
        }

        public void setMinPrice(String minPrice) {
            this.minPrice = minPrice;
        }

        public String getMaxPrice() {
            return maxPrice;
        }

        public void setMaxPrice(String maxPrice) {
            this.maxPrice = maxPrice;
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}