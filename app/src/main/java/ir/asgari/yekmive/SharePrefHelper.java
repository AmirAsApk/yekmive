package ir.asgari.yekmive;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.common.util.SharedPreferencesUtils;

import ir.asgari.yekmive.app.App;

public class SharePrefHelper {
    App app;
    SharedPreferences sharedPreferences;

    public SharePrefHelper(Context context) {
        app = App.getInstance();
        sharedPreferences = context.getSharedPreferences("login", Context.MODE_PRIVATE);
    }

    public void setParams(String number, String userName, String fullName, String accessToken, String accountId, String profileId, String img) {
        app.setPhone(number);
        app.setUsername(userName);
        app.setFullname(fullName);
        app.setAccessToken(accessToken);
        app.setId(Long.parseLong(accountId));
        sharedPreferences.edit().putString("profileId", profileId).apply();
        sharedPreferences.edit().putString("image", img).apply();
        Log.i("preHelper", accountId + "   |    " + accessToken);
        app.saveData();
        Log.i("preHelper", app.getId() + "   |    " + app.getAccessToken());
    }

    public boolean isLogin() {
        if (app.getId() != 0)
            return true;
        else
            return false;
    }

    public String getFullName() {
        return app.getFullname();
    }

    public String getUserName() {
        return app.getUsername();
    }

    public String getAccountID() {
        return app.getId() + "";
    }

    public String getProfileID() {
        return sharedPreferences.getString("profileId", "");
    }

    public String getImg() {
        return sharedPreferences.getString("image", "");
    }

    public String getAccessToken() {
        return app.getAccessToken() + "";
    }

    public String getAccessId() {
        return app.getAccessId() + "";
    }

    public App getApp() {
        return app;
    }

    public String getNumber() {
        return app.getPhone();
    }

    public void deleteParam() {
        app.setPhone("");
        app.setUsername("");
        app.setId(0);
        app.setFullname("");
        app.saveData();
    }

}
