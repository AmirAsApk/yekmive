package ir.asgari.yekmive;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.gson.internal.$Gson$Types;

import java.util.ArrayList;
import java.util.List;

import ir.asgari.yekmive.model.ModelCategoryNewItem;
import ir.asgari.yekmive.model.ModelChildCategory;
import ir.asgari.yekmive.model.ModelOstan;
import ir.asgari.yekmive.model.ModelShahr;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import retrofit2.Call;

public class SpinnerHelper {

    Activity activity;
    PreNewItemHelper newItemHelper;

    public SpinnerHelper(Activity activity, Context context) {
        this.activity = activity;
        newItemHelper = new PreNewItemHelper(context);
    }

    List<ModelOstan.Item> modelOstans;
    List<ModelShahr.Item> modelShahr;
    List<ModelCategoryNewItem.Item> modelCategory;
    List<ModelChildCategory.Item> modelChildCategory;


    public List<ModelOstan.Item> getModelOstans() {
        return modelOstans;
    }

    public void setModelOstans(List<ModelOstan.Item> modelOstans) {
        this.modelOstans = modelOstans;
    }

    public List<ModelShahr.Item> getModelShahr() {
        return modelShahr;
    }

    public void setModelShahr(List<ModelShahr.Item> modelShahr) {
        this.modelShahr = modelShahr;
    }

    public List<ModelCategoryNewItem.Item> getModelCategory() {
        return modelCategory;
    }

    public void setModelCategory(List<ModelCategoryNewItem.Item> modelCategory) {
        this.modelCategory = modelCategory;
    }

    public List<ModelChildCategory.Item> getModelChildCategory() {
        return modelChildCategory;
    }

    public void setModelChildCategory(List<ModelChildCategory.Item> modelChildCategory) {
        this.modelChildCategory = modelChildCategory;
    }

    public void setupSpinnerCategory(Spinner mChoiceCategory) {
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_dropdown_item, android.R.id.text1);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mChoiceCategory.setAdapter(spinnerAdapter);
        spinnerAdapter.add(activity.getString(R.string.label_choice_category));
        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        Call<ModelCategoryNewItem> categoryNewItemCall = serverRequest.getCategoryNewItem(0, 0);
        categoryNewItemCall.enqueue(new retrofit2.Callback<ModelCategoryNewItem>() {
            @Override
            public void onResponse(Call<ModelCategoryNewItem> call, retrofit2.Response<ModelCategoryNewItem> response) {
                if (response.body() != null) {
                    Log.i("spinnerTest", response.body().toString());
                    for (ModelCategoryNewItem.Item item : response.body().getItems()) {
                        spinnerAdapter.add(item.getTitle());
                    }
                    Log.i("spinner", "" + mChoiceCategory.getCount() + "|" + newItemHelper.getcategory());
                    mChoiceCategory.setSelection(newItemHelper.getcategory());
                    spinnerAdapter.notifyDataSetChanged();
                    modelCategory = response.body().getItems();

                }
            }

            @Override
            public void onFailure(Call<ModelCategoryNewItem> call, Throwable t) {
            }
        });
    }

    public void setupSpinnerChildCategory(Spinner mChoiceCategory, String category) {
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, android.R.id.text1);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mChoiceCategory.setAdapter(spinnerAdapter);
        spinnerAdapter.add(activity.getString(R.string.label_choice_child_category));
        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        Call<ModelChildCategory> categoryNewItemCall = serverRequest.getChildCategory("0", "0", "");
        categoryNewItemCall.enqueue(new retrofit2.Callback<ModelChildCategory>() {
            @Override
            public void onResponse(Call<ModelChildCategory> call, retrofit2.Response<ModelChildCategory> response) {
                if (response.body() != null) {
                    Log.i("spinnerTest", response.body().toString());

                    for (ModelChildCategory.Item item : response.body().getItems()) {
                        if (item.getCategoryId().equals(category))
                            spinnerAdapter.add(item.getChildcategoriesTag());
                    }
                    spinnerAdapter.notifyDataSetChanged();
                    Log.i("spinner", "" + response.body().toString());
                    modelChildCategory = response.body().getItems();
                }
            }

            @Override
            public void onFailure(Call<ModelChildCategory> call, Throwable t) {
            }
        });
    }

    public void setupSpinnerOstan(Spinner mChoiceOstan) {
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_dropdown_item, android.R.id.text1);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mChoiceOstan.setAdapter(spinnerAdapter);
        spinnerAdapter.add(activity.getString(R.string.label_choice_ostan));
        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        Call<ModelOstan> categoryNewItemCall = serverRequest.getOstan("1");
        categoryNewItemCall.enqueue(new retrofit2.Callback<ModelOstan>() {
            @Override
            public void onResponse(Call<ModelOstan> call, retrofit2.Response<ModelOstan> response) {
                if (response.body() != null) {
                    Log.i("spinnerTest", response.body().toString());
                    for (ModelOstan.Item item : response.body().getItems()) {
                        spinnerAdapter.add(item.getTitle());
                    }
                    mChoiceOstan.setSelection(newItemHelper.getostan());
                    spinnerAdapter.notifyDataSetChanged();
                    modelOstans = response.body().getItems();

                }
            }

            @Override
            public void onFailure(Call<ModelOstan> call, Throwable t) {

            }
        });
    }

    public void setupSpinnerShahr(Spinner mChoiceCategory, String ostanId) {
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, android.R.id.text1);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mChoiceCategory.setAdapter(spinnerAdapter);

        spinnerAdapter.add(activity.getString(R.string.label_choice_shahr));
        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        Call<ModelShahr> categoryNewItemCall = serverRequest.getShahrWithOstan(ostanId);
        categoryNewItemCall.enqueue(new retrofit2.Callback<ModelShahr>() {
            @Override
            public void onResponse(Call<ModelShahr> call, retrofit2.Response<ModelShahr> response) {
                Log.i("spinnerTest", response.body().toString());
                if (response.body() != null) {
                    for (ModelShahr.Item item : response.body().getItems()) {
                        spinnerAdapter.add(item.getTitle());
                    }
                    spinnerAdapter.notifyDataSetChanged();
                    modelShahr = response.body().getItems();
                }
            }

            @Override
            public void onFailure(Call<ModelShahr> call, Throwable t) {

            }
        });
    }

}
