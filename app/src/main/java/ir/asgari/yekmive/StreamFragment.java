package ir.asgari.yekmive;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.asgari.yekmive.adapter.SearchListAdapter;
import ir.asgari.yekmive.adapter.StreamListAdapter;
import ir.asgari.yekmive.app.App;
import ir.asgari.yekmive.constants.Constants;
import ir.asgari.yekmive.model.Item;
import ir.asgari.yekmive.model.ModelSearch;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import ir.asgari.yekmive.utilorg.CustomRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ir.asgari.yekmive.ActivityViewItem.ID_LAYOUT;
import static ir.asgari.yekmive.PaginationListener.PAGE_START;

public class StreamFragment extends Fragment implements Constants, SwipeRefreshLayout.OnRefreshListener {
    private static final String STATE_LIST = "State Adapter Data";
    private static final String STATE_LIST_2 = "State Adapter Data";
    private static final int PROFILE_NEW_POST = 4;
    
    RecyclerView mRecyclerView;
    TextView mMessage;
    CardView card;
    SwipeRefreshLayout mItemsContainer;
    ProgressBar progressBar;
    boolean status = false;
    private SearchListAdapter adapter;
    private List<ModelSearch.Item> items;
    private Parcelable mListState;

    private int itemId = 0;
    private int arrayLength = 0;
    private Boolean loadingMore = false;
    private Boolean viewMore = false;
    private Boolean restore = false;
    private int currentPage = PAGE_START;
    private boolean isLoading = false;

    public StreamFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        items = new ArrayList<>();
        adapter = new SearchListAdapter(getActivity(), items);

        if (savedInstanceState != null) {
            restore = savedInstanceState.getBoolean("restore");
            itemId = savedInstanceState.getInt("itemId");
            viewMore = savedInstanceState.getBoolean("viewMore");
        } else {
            restore = false;
            itemId = 0;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_stream, container, false);
        mItemsContainer = rootView.findViewById(R.id.container_items);
        mItemsContainer.setOnRefreshListener(this);
        card = rootView.findViewById(R.id.card);
        progressBar = rootView.findViewById(R.id.progress_new_item);
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ActivityViewItem.class);
                intent.putExtra(ID_LAYOUT, ActivityViewItem.search);
                startActivity(intent);
            }
        });
        mMessage = rootView.findViewById(R.id.message);
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(adapter);
        mItemsContainer.setRefreshing(true);
        getItems();
        adapter.setOnBottomReachedListener(new SearchListAdapter.onBottomReachedListener() {
            @Override
            public void onBottomReached(int position) {
                //End of list
                isLoading = true;
                currentPage++;
                doApiCall();

            }
        });
        return rootView;
    }

    private void doApiCall() {
        Log.e("aaaaaaaa", "getttttttt");
        getItems();
        isLoading = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("viewMore", viewMore);
        outState.putBoolean("restore", true);
        outState.putInt("itemId", itemId);
        mListState = mRecyclerView.getLayoutManager().onSaveInstanceState();
        outState.putParcelable(STATE_LIST, mListState);
//        outState.putParcelableArrayList(STATE_LIST_2, itemsList);
    }

    @Override
    public void onRefresh() {
        if (App.getInstance().isConnected()) {
            itemId = 0;
            items.clear();
            adapter.setItemList(items);
            adapter.notifyDataSetChanged();

            getItems();
        } else {
            mItemsContainer.setRefreshing(false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == STREAM_NEW_POST && resultCode == getActivity().RESULT_OK && null != data) {
            itemId = 0;
            getItems();
        } else if (requestCode == ITEM_EDIT && resultCode == getActivity().RESULT_OK) {
            int position = data.getIntExtra("position", 0);
//            Item item = itemsList.get(position);
//            item.setContent(data.getStringExtra("post"));
//            item.setImgUrl(data.getStringExtra("imgUrl"));
//            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void getItems() {
        if (status) {
            progressBar.setVisibility(View.VISIBLE);
        }
        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        serverRequest.getOrgItem("1",
                String.valueOf(App.getInstance().getId()),
                App.getInstance().getAccessToken(),
                itemId, "en").enqueue(new Callback<ModelSearch>() {
            @Override
            public void onResponse(Call<ModelSearch> call, Response<ModelSearch> response) {
                if (!response.body().getError()) {
                    if (!status) status = true;
                    progressBar.setVisibility(View.GONE);
                    itemId = Integer.parseInt(response.body().getItemId());
                    if (response.body().getItems() != null) {
                        items.addAll(response.body().getItems());
                        adapter.setItemList(items);
                        adapter.notifyDataSetChanged();
                        mItemsContainer.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelSearch> call, Throwable t) {
                mItemsContainer.setRefreshing(false);
            }
        });
    }

    public void loadingComplete() {

        if (arrayLength == LIST_ITEMS) {

            viewMore = true;

        } else {

            viewMore = false;
        }

        adapter.notifyDataSetChanged();

        if (adapter.getItemCount() == 0) {

            if (StreamFragment.this.isVisible()) {

//                showMessage(getText(R.string.label_empty_list).toString());
            }

        } else {

            hideMessage();
        }

        loadingMore = false;
        mItemsContainer.setRefreshing(false);
    }

    public void showMessage(String message) {

        mMessage.setText(message);
        mMessage.setVisibility(View.VISIBLE);
    }

    public void hideMessage() {

        mMessage.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
