package ir.asgari.yekmive;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import android.view.MenuItem;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import ir.asgari.yekmive.common.ActivityBase;
import ir.asgari.yekmive.dialogs.CommentActionDialog;
import ir.asgari.yekmive.dialogs.CommentDeleteDialog;
import ir.asgari.yekmive.dialogs.ItemActionDialog;
import ir.asgari.yekmive.dialogs.ItemDeleteDialog;
import ir.asgari.yekmive.dialogs.ItemReportDialog;
import ir.asgari.yekmive.dialogs.MyCommentActionDialog;
import ir.asgari.yekmive.dialogs.MyItemActionDialog;


public class ViewItemActivity extends ActivityBase implements CommentDeleteDialog.AlertPositiveListener, ItemDeleteDialog.AlertPositiveListener, ItemReportDialog.AlertPositiveListener, MyItemActionDialog.AlertPositiveListener, ItemActionDialog.AlertPositiveListener, CommentActionDialog.AlertPositiveListener, MyCommentActionDialog.AlertPositiveListener {
    Toolbar mToolbar;
    Fragment fragment;

    int status;

    public void setFragment(Fragment fragment1, int status) {
        this.status = status;
        fragment = fragment1;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.container_body, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item);
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        setFragment(new ViewItemFragment(), 1);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        getSupportFragmentManager().putFragment(outState, "currentFragment", fragment);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onItemDelete(int position) {

        ViewItemFragment p = (ViewItemFragment) fragment;
        p.onItemDelete(position);
    }

    @Override
    public void onItemReport(int position, int reasonId) {

        ViewItemFragment p = (ViewItemFragment) fragment;
        p.onItemReport(position, reasonId);
    }

    @Override
    public void onItemRemove(int position) {

        ViewItemFragment p = (ViewItemFragment) fragment;
        p.onItemRemove(position);
    }

    @Override
    public void onItemEdit(int position) {

        ViewItemFragment p = (ViewItemFragment) fragment;
        p.onItemEdit(position);
    }

    @Override
    public void onItemShare(int position) {

        ViewItemFragment p = (ViewItemFragment) fragment;
        p.onItemShare(position);
    }

    @Override
    public void onItemReportDialog(int position) {
        ViewItemFragment p = (ViewItemFragment) fragment;
        p.report(position);
    }

    @Override
    public void onCommentRemove(int position) {

        ViewItemFragment p = (ViewItemFragment) fragment;
        p.onCommentRemove(position);
    }

    @Override
    public void onCommentDelete(int position) {

        ViewItemFragment p = (ViewItemFragment) fragment;
        p.onCommentDelete(position);
    }

    @Override
    public void onCommentReply(int position) {
        ViewItemFragment p = (ViewItemFragment) fragment;
        p.onCommentReply(position);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {

            case android.R.id.home: {

                finish();
                return true;
            }

            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (status == 2)
            super.onBackPressed();
        else if (status == 1)
            finish();
    }
}
