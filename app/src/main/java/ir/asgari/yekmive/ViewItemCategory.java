package ir.asgari.yekmive;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ir.asgari.yekmive.adapter.SearchListAdapter;
import ir.asgari.yekmive.databinding.ActivityViewItemCategoryBinding;
import ir.asgari.yekmive.model.ModelSearch;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ir.asgari.yekmive.PaginationListener.PAGE_START;

public class ViewItemCategory extends AppCompatActivity {
    int id;
    ActivityViewItemCategoryBinding binding;

    boolean status = false;
    private int itemId = 0;
    private int arrayLength = 0;
    private Boolean loadingMore = false;
    private Boolean viewMore = false;
    private Boolean restore = false;
    private int currentPage = PAGE_START;
    private boolean isLoading = false;
    SearchListAdapter adapter;
    List<ModelSearch.Item> items;
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_item_category);
        id = getIntent().getIntExtra("id", 0);
        binding.setMassage("");
        binding.containerItems.setOnRefreshListener(() -> getCategoryItem());
        items = new ArrayList<>();
        adapter = new SearchListAdapter(ViewItemCategory.this, items,getIntent().getStringExtra("image"));
        binding.recyclerView.setAdapter(adapter);
        mRecyclerView = binding.recyclerView;
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnBottomReachedListener(new SearchListAdapter.onBottomReachedListener() {
            @Override
            public void onBottomReached(int position) {
                //End of list
                isLoading = true;
                currentPage++;
                doApiCall();

            }
        });
        binding.containerItems.setRefreshing(true);
        getCategoryItem();


    }

    private void doApiCall() {
        Log.e("aaaaaaaa", "getttttttt");
        itemId++;
        getCategoryItem();
        isLoading = false;
    }

    private void getCategoryItem() {
        if (status) {
            binding.progressNewItem.setVisibility(View.VISIBLE);
        }

        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        Call<ModelSearch> call = serverRequest.getSearch("", String.valueOf(itemId), "", id + "", "", "", "0", "0", "");
        call.enqueue(new Callback<ModelSearch>() {
            @Override
            public void onResponse(Call<ModelSearch> call, Response<ModelSearch> response) {
                if (!response.body().getError()) {
                    if (!status) status = true;
                    binding.progressNewItem.setVisibility(View.GONE);
                    itemId = Integer.parseInt(response.body().getItemId());
                    if (response.body().getItems() != null) {
                        items.addAll(response.body().getItems());
                        adapter.setItemList(items);
                        adapter.notifyDataSetChanged();
                        binding.containerItems.setRefreshing(false);
                    }
                }
                binding.containerItems.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ModelSearch> call, Throwable t) {
                binding.setMassage("خطا لطفا دوباره امتحان کنید");
                binding.containerItems.setRefreshing(false);
            }
        });
    }
}