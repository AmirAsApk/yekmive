package ir.asgari.yekmive;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ir.asgari.yekmive.adapter.ChildCategoryListAdapter;
import ir.asgari.yekmive.databinding.ActivityViewItemChildCategoryBinding;
import ir.asgari.yekmive.model.ModelChildCategory;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ir.asgari.yekmive.PaginationListener.PAGE_START;

public class ViewItemChildCategory extends AppCompatActivity {

    ActivityViewItemChildCategoryBinding binding;
    ServerRequest serverRequest;
    ChildCategoryListAdapter adapter;
    List<ModelChildCategory.Item> childCategories;

    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private int totalPage = 10;
    private boolean isLoading = false;
    int itemCount = 0;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_item_child_category);
        id = getIntent().getStringExtra("category");
        serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        getChildCategory(id);
        GridLayoutManager layoutManager=new GridLayoutManager(getApplicationContext(),2,LinearLayoutManager.VERTICAL,false);
        binding.recycler.setLayoutManager(layoutManager);
        childCategories = new ArrayList<>();
        adapter = new ChildCategoryListAdapter(ViewItemChildCategory.this, childCategories);
        binding.setAdapter(adapter);
//        binding.recycler.addOnScrollListener(new PaginationListener(layoutManager) {
//            @Override
//            protected void loadMoreItems() {
//                isLoading = true;
//                currentPage++;
//                doApiCall();
//            }
//
//            @Override
//            public boolean isLastPage() {
//                return isLastPage;
//            }
//
//            @Override
//            public boolean isLoading() {
//                return isLoading;
//            }
//        });
    }

    private void getChildCategory(String id) {
        Call<ModelChildCategory> categoryCall = serverRequest.getChildCategory("", "0", id);
        categoryCall.enqueue(new Callback<ModelChildCategory>() {
            @Override
            public void onResponse(Call<ModelChildCategory> call, Response<ModelChildCategory> response) {
                childCategories.addAll(response.body().getItems());
                adapter.setItemList(childCategories);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ModelChildCategory> call, Throwable t) {

            }
        });
    }
}