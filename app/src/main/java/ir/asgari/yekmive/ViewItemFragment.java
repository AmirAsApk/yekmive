package ir.asgari.yekmive;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import github.ankushsachdeva.emojicon.EditTextImeBackListener;
import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconGridView;
import github.ankushsachdeva.emojicon.EmojiconsPopup;
import github.ankushsachdeva.emojicon.emoji.Emojicon;
import ir.asgari.yekmive.adapter.AdapterVIewSlider;
import ir.asgari.yekmive.adapter.CommentListAdapter;
import ir.asgari.yekmive.adapter.ImagesAdapter;
import ir.asgari.yekmive.app.App;
import ir.asgari.yekmive.constants.Constants;
import ir.asgari.yekmive.dialogs.CommentActionDialog;
import ir.asgari.yekmive.dialogs.CommentDeleteDialog;
import ir.asgari.yekmive.dialogs.ItemActionDialog;
import ir.asgari.yekmive.dialogs.ItemDeleteDialog;
import ir.asgari.yekmive.dialogs.ItemReportDialog;
import ir.asgari.yekmive.dialogs.MyCommentActionDialog;
import ir.asgari.yekmive.dialogs.MyItemActionDialog;
import ir.asgari.yekmive.model.Comment;
import ir.asgari.yekmive.model.ImageItem;
import ir.asgari.yekmive.model.Item;
import ir.asgari.yekmive.utilorg.Api;
import ir.asgari.yekmive.utilorg.CommentInterface;
import ir.asgari.yekmive.utilorg.CustomRequest;
import ir.asgari.yekmive.utilorg.Helper;

import static ir.asgari.yekmive.ActivityViewItem.ID_LAYOUT;

public class ViewItemFragment extends Fragment implements Constants, SwipeRefreshLayout.OnRefreshListener, CommentInterface {
    private ProgressDialog pDialog;
    ArrayList<ImageItem> images;
    SwipeRefreshLayout mContentContainer;
    RelativeLayout mErrorScreen, mLoadingScreen, mEmptyScreen;
    LinearLayout mContentScreen, mCommentFormContainer;
    EmojiconEditText mCommentText;
    ListView listView;
    Button mRetryBtn, mItemViewAuthorProfile, mItemCallToAuthor;
    View mListViewHeader;
    ImageView mItemLike, mItemShare, mItemComment, mEmojiBtn, mSendComment, imReport;
    TextView mItemCategory, mItemDate, mItemTitle, mItemLikesCount, mItemCommentsCount, mItemPrice, mItemDescription, mItemLocation;
    LinearLayout mDots;
    Button btn_chat;
    SliderView sliderView;
    ImageLoader imageLoader = App.getInstance().getImageLoader();
    private ArrayList<Comment> commentsList;
    private CommentListAdapter itemAdapter;
    Item item;
    long itemId = 0, replyToUserId = 0;
    int arrayLength = 0;
    String commentText;
    String itemImg2 = "", itemImg3 = "", itemImg4 = "";
    private Boolean loading = false;
    private Boolean restore = false;
    private Boolean preload = false;
    private Boolean loadingComplete = false;
    EmojiconsPopup popup;
    String city, createAt;
    SharePrefHelper pre;

    public ViewItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        setHasOptionsMenu(true);

        initpDialog();

        Intent i = getActivity().getIntent();

        itemId = i.getLongExtra("itemId", 0);
        city = i.getStringExtra("city");
        createAt = i.getStringExtra("createAt");
        pre = new SharePrefHelper(getActivity());

        commentsList = new ArrayList<Comment>();
        itemAdapter = new CommentListAdapter((AppCompatActivity) getActivity(), commentsList, this);

        getActivity().setTitle(getString(R.string.title_activity_view_item));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_view_item, container, false);
        popup = new EmojiconsPopup(rootView, getActivity());
        popup.setSizeForSoftKeyboard();
        popup.setOnEmojiconClickedListener(emojicon -> mCommentText.append(emojicon.getEmoji()));
        popup.setOnEmojiconBackspaceClickedListener(v -> {
            KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
            mCommentText.dispatchKeyEvent(event);
        });

        popup.setOnDismissListener(() -> setIconEmojiKeyboard());
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {
            @Override
            public void onKeyboardOpen(int keyBoardHeight) {
            }
            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });

        popup.setOnEmojiconClickedListener(emojicon -> mCommentText.append(emojicon.getEmoji()));

        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {

                KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                mCommentText.dispatchKeyEvent(event);
            }
        });

        if (savedInstanceState != null) {

            restore = savedInstanceState.getBoolean("restore");
            loading = savedInstanceState.getBoolean("loading");
            preload = savedInstanceState.getBoolean("preload");

            replyToUserId = savedInstanceState.getLong("replyToUserId");

        } else {

            restore = false;
            loading = false;
            preload = false;

            replyToUserId = 0;
        }

        if (loading) {

            showpDialog();
        }

        if (images == null) {

            images = new ArrayList<ImageItem>();
        }

        mEmptyScreen = (RelativeLayout) rootView.findViewById(R.id.emptyScreen);
        mErrorScreen = (RelativeLayout) rootView.findViewById(R.id.errorScreen);
        mLoadingScreen = (RelativeLayout) rootView.findViewById(R.id.loadingScreen);
        mContentContainer = rootView.findViewById(R.id.contentContainer);
        mContentContainer.setOnRefreshListener(this);
        mContentScreen = (LinearLayout) rootView.findViewById(R.id.contentScreen);
        mCommentFormContainer = (LinearLayout) rootView.findViewById(R.id.commentFormContainer);
        mCommentText = (EmojiconEditText) rootView.findViewById(R.id.commentText);
        mSendComment = (ImageView) rootView.findViewById(R.id.sendCommentImg);
        mEmojiBtn = (ImageView) rootView.findViewById(R.id.emojiBtn);
        mRetryBtn = (Button) rootView.findViewById(R.id.retryBtn);
        mRetryBtn.setOnClickListener(v -> {
            if (App.getInstance().isConnected()) {
                showLoadingScreen();
                getItem();
            }
        });
        listView = (ListView) rootView.findViewById(R.id.listView);
        mListViewHeader = getActivity().getLayoutInflater().inflate(R.layout.view_item_list_row, null);
        listView.addHeaderView(mListViewHeader);
        listView.setAdapter(itemAdapter);
        mDots = (LinearLayout) mListViewHeader.findViewById(R.id.dots);
        sliderView = mListViewHeader.findViewById(R.id.imageSlider);
        rootView.findViewById(R.id.secure_buy).setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), ActivityWebView.class);
            intent.putExtra("url", "https://yekmive.ir/safty.php");
            startActivity(intent);
        });

        rootView.findViewById(R.id.ic_location).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), OsmActivity.class);
                i.putExtra("status", 2);
                double lat = item.getLat();
                double lng = item.getLng();
                if (lat == 0 && lng == 0) {
                    lat = 35.715298;
                    lng = 51.404343;
                }
                i.putExtra("lat", lat);
                i.putExtra("lng", lng);

                startActivity(i);
            }
        });

        mItemViewAuthorProfile = (Button) getActivity().findViewById(R.id.itemViewAuthorProfile);
        mItemCallToAuthor = (Button) getActivity().findViewById(R.id.itemCallToAuthor);
        mItemPrice = (TextView) mListViewHeader.findViewById(R.id.itemPrice);
        mItemDescription = (TextView) mListViewHeader.findViewById(R.id.itemDescription);
        mItemLike = (ImageView) mListViewHeader.findViewById(R.id.itemLike);
        mItemShare = (ImageView) mListViewHeader.findViewById(R.id.itemShare);
        mItemComment = (ImageView) mListViewHeader.findViewById(R.id.itemComment);
        mItemLocation = (TextView) mListViewHeader.findViewById(R.id.itemLocation);
        imReport = mListViewHeader.findViewById(R.id.report);
        mItemDate = (TextView) mListViewHeader.findViewById(R.id.itemDate);
        mItemTitle = (TextView) mListViewHeader.findViewById(R.id.itemTitle);
        mItemCategory = (TextView) mListViewHeader.findViewById(R.id.itemCategory);
        mItemLikesCount = (TextView) mListViewHeader.findViewById(R.id.itemLikesCount);
        mItemCommentsCount = (TextView) mListViewHeader.findViewById(R.id.itemCommentsCount);
        if (!EMOJI_KEYBOARD) {
            mEmojiBtn.setVisibility(View.GONE);
        }
        btn_chat = getActivity().findViewById(R.id.itemChatToAuthor);
        btn_chat.setOnClickListener(v -> {
            SharePrefHelper sharePrefHelper = new SharePrefHelper(getContext());
            if (sharePrefHelper.isLogin()) {
                Intent i = new Intent(getActivity(), ChatActivity.class);
                i.putExtra("chatId", 0);
                i.putExtra("profileId", item.getFromUserId());
                i.putExtra("withProfile", item.getFromUserFullname());
                i.putExtra("itemId", itemId);
                startActivity(i);
            } else {
                Intent intent = new Intent(getActivity(), ActivityViewItem.class);
                intent.putExtra(ID_LAYOUT, ActivityViewItem.NotLogin);
                startActivity(intent);
            }
        });

        getActivity().findViewById(R.id.itemSmsToAuthor).setOnClickListener(v ->
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", item.getFromUserPhone(), null)))
        );

        mEmojiBtn.setOnClickListener(v -> {
            if (!popup.isShowing()) {
                if (popup.isKeyBoardOpen()) {
                    popup.showAtBottom();
                    setIconSoftKeyboard();
                } else {
                    mCommentText.setFocusableInTouchMode(true);
                    mCommentText.requestFocus();
                    popup.showAtBottomPending();
                    final InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(mCommentText, InputMethodManager.SHOW_IMPLICIT);
                    setIconSoftKeyboard();
                }
            } else {
                popup.dismiss();
            }
        });

        EditTextImeBackListener er = (ctrl, text) -> hideEmojiKeyboard();
        mCommentText.setOnEditTextImeBackListener(er);
        if (!restore) {
            if (App.getInstance().isConnected()) {
                showLoadingScreen();
                getItem();
            } else {
                showErrorScreen();
            }
        } else {
            if (App.getInstance().isConnected()) {
                if (!preload) {
                    loadingComplete();
                    updateItem();
                } else {
                    showLoadingScreen();
                }
            } else {
                showErrorScreen();
            }
        }
        // Inflate the layout for this fragment
        return rootView;
    }

    private void showSlider() {
        if (images.size() <= 0)
            return;

        AdapterVIewSlider adapter = new AdapterVIewSlider(getContext(), images);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {

                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + item.getFromUserPhone()));
                    startActivity(intent);

                } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {

                    if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.CALL_PHONE)) {

                        showNoPhoneCallPermissionSnackbar();
                    }
                }

                return;
            }
        }
    }

    public void showNoPhoneCallPermissionSnackbar() {

        Snackbar.make(getView(), getString(R.string.label_no_phone_call_permission), Snackbar.LENGTH_LONG).setAction(getString(R.string.action_settings), new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                openApplicationSettings();

                Toast.makeText(getActivity(), getString(R.string.label_grant_phone_call_permission), Toast.LENGTH_SHORT).show();
            }

        }).show();
    }

    public void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getActivity().getPackageName()));
        startActivityForResult(appSettingsIntent, 10001);
    }

    public void hideEmojiKeyboard() {

        popup.dismiss();
    }

    public void setIconEmojiKeyboard() {

        mEmojiBtn.setBackgroundResource(R.drawable.ic_emoji);
    }

    public void setIconSoftKeyboard() {

        mEmojiBtn.setBackgroundResource(R.drawable.ic_keyboard);
    }

    public void onDestroyView() {
        super.onDestroyView();
        hidepDialog();
    }

    protected void initpDialog() {

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage(getString(R.string.msg_loading));
        pDialog.setCancelable(false);
    }

    protected void showpDialog() {

        if (!pDialog.isShowing()) pDialog.show();
    }

    protected void hidepDialog() {

        if (pDialog.isShowing()) pDialog.dismiss();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        outState.putBoolean("restore", true);
        outState.putBoolean("loading", loading);
        outState.putBoolean("preload", preload);

        outState.putLong("replyToUserId", replyToUserId);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ITEM_EDIT && resultCode == getActivity().RESULT_OK) {

            images = data.getParcelableArrayListExtra("images");

            item.setAllowComments(data.getIntExtra("itemAllowComments", 1));

            item.setCategoryId(data.getIntExtra("categoryId", 1));
            item.setCategoryTitle(data.getStringExtra("categoryTitle"));
            item.setPrice(data.getIntExtra("itemPrice", 0));
            item.setTitle(data.getStringExtra("itemTitle"));
            item.setContent(data.getStringExtra("itemDescription"));

            item.setCity(data.getStringExtra("itemCity"));
            item.setCountry(data.getStringExtra("itemCountry"));
            item.setArea(data.getStringExtra("itemArea"));

            item.setLat(data.getDoubleExtra("lat", 0.000000));
            item.setLng(data.getDoubleExtra("lng", 0.000000));

            updateItem();

            loadingComplete();
        }
    }

    @Override
    public void onRefresh() {

        if (App.getInstance().isConnected()) {

            mContentContainer.setRefreshing(true);
            getItem();

        } else {

            mContentContainer.setRefreshing(false);
        }
    }

    public void updateItem() {

        if (imageLoader == null) {

            imageLoader = App.getInstance().getImageLoader();
        }

        showSlider();

        getActivity().setTitle(item.getTitle());
//
//        if (item.getCity().length() > 0) {
//
//            mItemLocation.setVisibility(View.VISIBLE);
//            mItemLocation.setText("آدرس : " + item.getCity());
//
//        } else {
//            mItemLocation.setVisibility(View.GONE);
//        }

        mItemCategory.setText("دسته بندی : " + item.getCategoryTitle());
        mItemDate.setText(item.getDate());

        mItemComment.setVisibility(View.GONE);
        mItemCommentsCount.setVisibility(View.GONE);

        mItemCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CategoryActivity.class);
                intent.putExtra("categoryId", item.getCategoryId());
                intent.putExtra("title", item.getCategoryTitle());
                startActivity(intent);
            }
        });

        if (item.getFromUserId() == App.getInstance().getId()) {

            itemAdapter.setMyPost(true);

        } else {

            itemAdapter.setMyPost(false);
        }

        mItemLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (App.getInstance().isConnected()) {
                    if (pre.isLogin()) {
                        CustomRequest jsonReq = new CustomRequest(Request.Method.POST, METHOD_ITEMS_LIKE, null,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        if (!isAdded() || getActivity() == null) {
                                            Log.e("ERROR", "ViewItemFragment Not Added to Activity");
                                            return;
                                        }
                                        try {
                                            if (!response.getBoolean("error")) {
                                                item.setLikesCount(response.getInt("likesCount"));
                                                item.setMyLike(response.getBoolean("myLike"));
                                                if(mItemLike.getTag().equals("0")){
                                                mItemLike.setImageResource(R.drawable.ic_baseline_star_24);
                                                mItemLike.setTag("1");
                                                }else {
                                                 mItemLike.setImageResource(R.drawable.perk);
                                                 mItemLike.setTag("0");
                                                }
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        } finally {
                                            updateItem();
                                        }
                                    }
                                }, error -> {
                                    if (!isAdded() || getActivity() == null) {
                                        Log.e("ERROR", "ViewItemFragment Not Added to Activity");
                                        return;
                                    }

                                    Toast.makeText(getActivity(), getString(R.string.error_data_loading), Toast.LENGTH_LONG).show();
                                }) {

                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("accountId", Long.toString(App.getInstance().getId()));
                                params.put("accessToken", App.getInstance().getAccessToken());
                                params.put("itemId", Long.toString(item.getId()));

                                return params;
                            }
                        };

                        App.getInstance().addToRequestQueue(jsonReq);
                    } else {
                        Intent intent = new Intent(getActivity(), ActivityViewItem.class);
                        intent.putExtra(ID_LAYOUT, ActivityViewItem.NotLogin);
                        startActivity(intent);
                    }
                } else {

                    Toast.makeText(getActivity(), getText(R.string.msg_network_error), Toast.LENGTH_SHORT).show();
                }
            }
        });

        PackageManager pm = getActivity().getPackageManager();

        if (item.getFromUserPhone().length() > 0 && pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {

            mItemCallToAuthor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + item.getFromUserPhone()));
                    startActivity(intent);
                }
            });

            mItemCallToAuthor.setVisibility(View.VISIBLE);

        } else {

            mItemCallToAuthor.setVisibility(View.GONE);
        }

        mItemViewAuthorProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                intent.putExtra("profileId", item.getFromUserId());
                getActivity().startActivity(intent);
            }
        });


        mItemShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Api api = new Api(getActivity());
                api.itemShare(item, createAt);
            }
        });

//        if (item.isMyLike()) {
//
//            mItemLike.setImageResource(R.drawable.perk_active);
//
//        } else {
//
//            mItemLike.setImageResource(R.drawable.perk);
//        }

        if (item.getLikesCount() > 0) {

            mItemLikesCount.setText(Integer.toString(item.getLikesCount()));
            mItemLikesCount.setVisibility(View.VISIBLE);

        } else {

            mItemLikesCount.setText(Integer.toString(item.getLikesCount()));
            mItemLikesCount.setVisibility(View.GONE);
        }

        if (item.getTitle().length() > 0) {

            mItemTitle.setText("عنوان : " + item.getTitle());

            mItemTitle.setVisibility(View.VISIBLE);

        } else {

            mItemTitle.setVisibility(View.GONE);
        }

        if (item.getContent().length() != 0) {

            mItemDescription.setText("توضیحات : " + item.getContent().replaceAll("<br>", "\n"));
            mItemDescription.setVisibility(View.VISIBLE);

        } else {

            mItemDescription.setVisibility(View.GONE);
        }

        Helper helper = new Helper();

        if (item.getPrice() != 0)
            mItemPrice.setText("قیمت : " + helper.getFormatedAmount(item.getPrice()) + " " + getString(R.string.label_currency));
        else
            mItemPrice.setText("قیمت : توافقی");

//        if (item.getImgUrl() != null && !item.getImgUrl().isEmpty())
//            Picasso.with(getContext()).load(item.getImgUrl()).into(im);

        mItemLocation.setText("مکان : "+city);
//        im.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showImage(item.getImgUrl());
//            }
//        });

        imReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                report(0);
            }
        });

    }


    public void getItem() {

        CustomRequest jsonReq = new CustomRequest(Request.Method.POST, METHOD_ITEM_GET, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (!isAdded() || getActivity() == null) {
                            Log.e("ERROR", "ViewItemFragment Not Added to Activity");

                            return;
                        }
                        try {
                            Log.e("aaaassssssss", response.toString());
                            arrayLength = 0;
                            images.clear();
                            if (!response.getBoolean("error")) {

//                                Toast.makeText(ViewItemActivity.this, response.toString(), Toast.LENGTH_SHORT).show();

                                commentsList.clear();

                                itemId = response.getInt("itemId");

                                if (response.has("items")) {

                                    JSONArray itemsArray = response.getJSONArray("items");

                                    arrayLength = itemsArray.length();

                                    if (arrayLength > 0) {

                                        for (int i = 0; i < itemsArray.length(); i++) {

                                            JSONObject itemObj = (JSONObject) itemsArray.get(i);

                                            item = new Item(itemObj);
                                        }
                                    }
                                }

                                if (item.getAllowComments() == COMMENTS_ENABLED) {

                                    if (response.has("comments")) {

                                        JSONObject commentsObj = response.getJSONObject("comments");

                                        if (commentsObj.has("comments")) {

                                            JSONArray commentsArray = commentsObj.getJSONArray("comments");

                                            arrayLength = commentsArray.length();

                                            if (arrayLength > 0) {

                                                for (int i = commentsArray.length() - 1; i > -1; i--) {

                                                    JSONObject itemObj = (JSONObject) commentsArray.get(i);

                                                    Comment comment = new Comment(itemObj);

                                                    commentsList.add(comment);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (response.has("images")) {

                                    JSONArray imagesObj = response.getJSONArray("images");

                                    if (imagesObj.length() > 0) {

                                        JSONObject imgObj = (JSONObject) imagesObj.get(0);

                                        if (imgObj.has("items")) {

                                            JSONArray imagesArray = imgObj.getJSONArray("items");

                                            arrayLength = imagesArray.length();

                                            if (arrayLength > 0) {

                                                for (int i = 0; i < imagesArray.length(); i++) {

                                                    JSONObject iObj = (JSONObject) imagesArray.get(i);

                                                    images.add(new ImageItem(null, iObj.getString("imgUrl")));
                                                }

                                                Collections.reverse(images);
                                            }
                                        }
                                    }

                                    showSlider();
                                }

                                if (images.size() > 0) {

                                    images.add(0, new ImageItem(null, item.getImgUrl()));

                                } else {

                                    images.add(new ImageItem(null, item.getImgUrl()));
                                }

                                updateItem();

                                loadingComplete();

                            } else {
                                showErrorScreen();
                            }

                        } catch (JSONException e) {

                            showErrorScreen();

                            e.printStackTrace();

                        } finally {

                            Log.e("test", response.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (!isAdded() || getActivity() == null) {

                    Log.e("ERROR", "ViewItemFragment Not Added to Activity");

                    return;
                }

                showErrorScreen();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("accountId", Long.toString(App.getInstance().getId()));
                params.put("accessToken", App.getInstance().getAccessToken());
                params.put("itemId", Long.toString(itemId));
                params.put("language", "en");
                return params;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(VOLLEY_REQUEST_SECONDS), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        jsonReq.setRetryPolicy(policy);

        App.getInstance().addToRequestQueue(jsonReq);
    }

    public void send() {
        commentText = mCommentText.getText().toString();
        commentText = commentText.trim();
        if (App.getInstance().isConnected() && App.getInstance().getId() != 0 && commentText.length() > 0) {

            loading = true;

            showpDialog();

            CustomRequest jsonReq = new CustomRequest(Request.Method.POST, METHOD_COMMENTS_NEW, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            if (!isAdded() || getActivity() == null) {

                                Log.e("ERROR", "ViewItemFragment Not Added to Activity");

                                return;
                            }

                            try {

                                if (!response.getBoolean("error")) {

                                    if (response.has("comment")) {

                                        JSONObject commentObj = (JSONObject) response.getJSONObject("comment");

                                        Comment comment = new Comment(commentObj);

                                        commentsList.add(comment);

                                        itemAdapter.notifyDataSetChanged();

                                        listView.setSelection(itemAdapter.getCount() - 1);

                                        mCommentText.setText("");
                                        replyToUserId = 0;
                                    }

                                    Toast.makeText(getActivity(), getString(R.string.msg_comment_has_been_added), Toast.LENGTH_SHORT).show();

                                }

                            } catch (JSONException e) {

                                e.printStackTrace();

                            } finally {

                                loading = false;

                                hidepDialog();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if (!isAdded() || getActivity() == null) {

                        Log.e("ERROR", "ViewItemFragment Not Added to Activity");

                        return;
                    }

                    loading = false;

                    hidepDialog();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("accountId", Long.toString(App.getInstance().getId()));
                    params.put("accessToken", App.getInstance().getAccessToken());

                    params.put("itemId", Long.toString(item.getId()));
                    params.put("commentText", commentText);

                    params.put("replyToUserId", Long.toString(replyToUserId));

                    return params;
                }
            };

            int socketTimeout = 0;//0 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            jsonReq.setRetryPolicy(policy);

            App.getInstance().addToRequestQueue(jsonReq);
        }
    }

    public void onItemDelete(final int position) {

        Api api = new Api(getActivity());

        api.itemDelete(item.getId());

        getActivity().finish();
    }

    public void onItemReport(int position, int reasonId) {

        if (App.getInstance().isConnected()) {

            Api api = new Api(getActivity());

            api.itemReport(item.getId(), reasonId);

        } else {

            Toast.makeText(getActivity(), getText(R.string.msg_network_error), Toast.LENGTH_SHORT).show();
        }
    }

    public void onItemEdit(final int position) {

        Intent i = new Intent(getActivity(), NewItemActivity.class);

        i.putExtra("categoryId", item.getCategoryId());

        i.putExtra("mode", MODE_EDIT);
        i.putParcelableArrayListExtra("images", images);

        i.putExtra("itemId", item.getId());
        i.putExtra("itemPrice", item.getPrice());

        i.putExtra("itemTitle", item.getTitle());
        i.putExtra("itemDescription", item.getContent());

        i.putExtra("itemAllowComments", item.getAllowComments());

        i.putExtra("itemCity", item.getCity());
        i.putExtra("itemCountry", item.getCountry());
        i.putExtra("itemArea", item.getArea());

        i.putExtra("lat", item.getLat());
        i.putExtra("lng", item.getLng());

        startActivityForResult(i, ITEM_EDIT);
    }

    public void onItemShare(final int position) {

        Api api = new Api(getActivity());
        api.itemShare(item, createAt);
    }

    public void onItemRemove(int position) {

        android.app.FragmentManager fm = getActivity().getFragmentManager();

        ItemDeleteDialog alert = new ItemDeleteDialog();

        Bundle b = new Bundle();
        b.putInt("position", 0);

        alert.setArguments(b);
        alert.show(fm, "alert_dialog_post_delete");
    }

    public void report(int position) {

        android.app.FragmentManager fm = getActivity().getFragmentManager();

        ItemReportDialog alert = new ItemReportDialog();

        Bundle b = new Bundle();
        b.putInt("position", position);
        b.putInt("reason", 0);

        alert.setArguments(b);
        alert.show(fm, "alert_dialog_post_report");
    }

    public void action(int position) {

        if (item.getFromUserId() == App.getInstance().getId()) {

            /** Getting the fragment manager */
            android.app.FragmentManager fm = getActivity().getFragmentManager();

            /** Instantiating the DialogFragment class */
            MyItemActionDialog alert = new MyItemActionDialog();

            /** Creating a bundle object to store the selected item's index */
            Bundle b = new Bundle();

            /** Storing the selected item's index in the bundle object */
            b.putInt("position", position);

            /** Setting the bundle object to the dialog fragment object */
            alert.setArguments(b);

            /** Creating the dialog fragment object, which will in turn open the alert dialog window */

            alert.show(fm, "alert_my_post_action");

        } else {

            /** Getting the fragment manager */
            android.app.FragmentManager fm = getActivity().getFragmentManager();

            /** Instantiating the DialogFragment class */
            ItemActionDialog alert = new ItemActionDialog();

            /** Creating a bundle object to store the selected item's index */
            Bundle b = new Bundle();

            /** Storing the selected item's index in the bundle object */
            b.putInt("position", position);

            /** Setting the bundle object to the dialog fragment object */
            alert.setArguments(b);

            /** Creating the dialog fragment object, which will in turn open the alert dialog window */

            alert.show(fm, "alert_post_action");
        }
    }

    public void loadingComplete() {

        itemAdapter.notifyDataSetChanged();

        if (listView.getAdapter().getCount() == 0) {

            showEmptyScreen();

        } else {

            showContentScreen();
        }

        if (mContentContainer.isRefreshing()) {

            mContentContainer.setRefreshing(false);
        }
    }

    public void showLoadingScreen() {

        preload = true;

        mContentScreen.setVisibility(View.GONE);
        mErrorScreen.setVisibility(View.GONE);
        mEmptyScreen.setVisibility(View.GONE);

        mLoadingScreen.setVisibility(View.VISIBLE);

        loadingComplete = false;
    }

    public void showEmptyScreen() {

        mContentScreen.setVisibility(View.GONE);
        mLoadingScreen.setVisibility(View.GONE);
        mErrorScreen.setVisibility(View.GONE);

        mEmptyScreen.setVisibility(View.VISIBLE);

        loadingComplete = false;
    }

    public void showErrorScreen() {

        mContentScreen.setVisibility(View.GONE);
        mLoadingScreen.setVisibility(View.GONE);
        mEmptyScreen.setVisibility(View.GONE);

        mErrorScreen.setVisibility(View.VISIBLE);

        loadingComplete = false;
    }

    public void showContentScreen() {

        preload = false;

        mLoadingScreen.setVisibility(View.GONE);
        mErrorScreen.setVisibility(View.GONE);
        mEmptyScreen.setVisibility(View.GONE);

        mContentScreen.setVisibility(View.VISIBLE);

        if (item.getAllowComments() == COMMENTS_DISABLED) {

            mCommentFormContainer.setVisibility(View.GONE);

        } else {

            mCommentFormContainer.setVisibility(View.VISIBLE);
        }

        if (App.getInstance().getId() == 0) {

            mCommentFormContainer.setVisibility(View.GONE);
        }

        loadingComplete = true;

        getActivity().invalidateOptionsMenu();
    }

    public void commentAction(int position) {

        final Comment comment = commentsList.get(position);

        if (comment.getFromUserId() != App.getInstance().getId()) {

            /** Getting the fragment manager */
            android.app.FragmentManager fm = getActivity().getFragmentManager();

            /** Instantiating the DialogFragment class */
            CommentActionDialog alert = new CommentActionDialog();

            /** Creating a bundle object to store the selected item's index */
            Bundle b = new Bundle();

            /** Storing the selected item's index in the bundle object */
            b.putInt("position", position);

            /** Setting the bundle object to the dialog fragment object */
            alert.setArguments(b);

            /** Creating the dialog fragment object, which will in turn open the alert dialog window */

            alert.show(fm, "alert_dialog_comment_action");

        } else {

            /** Getting the fragment manager */
            android.app.FragmentManager fm = getActivity().getFragmentManager();

            /** Instantiating the DialogFragment class */
            MyCommentActionDialog alert = new MyCommentActionDialog();

            /** Creating a bundle object to store the selected item's index */
            Bundle b = new Bundle();

            /** Storing the selected item's index in the bundle object */
            b.putInt("position", position);

            /** Setting the bundle object to the dialog fragment object */
            alert.setArguments(b);

            /** Creating the dialog fragment object, which will in turn open the alert dialog window */

            alert.show(fm, "alert_dialog_my_comment_action");
        }
    }

    public void onCommentReply(final int position) {

        if (item.getAllowComments() == COMMENTS_ENABLED) {

            final Comment comment = commentsList.get(position);

            replyToUserId = comment.getFromUserId();

            mCommentText.setText("@" + comment.getFromUserUsername() + ", ");
            mCommentText.setSelection(mCommentText.getText().length());

            mCommentText.requestFocus();

        } else {

            Toast.makeText(getActivity(), getString(R.string.msg_comments_disabled), Toast.LENGTH_SHORT).show();
        }
    }

    public void onCommentRemove(int position) {

        /** Getting the fragment manager */
        FragmentManager fm = getActivity().getFragmentManager();

        /** Instantiating the DialogFragment class */
        CommentDeleteDialog alert = new CommentDeleteDialog();

        /** Creating a bundle object to store the selected item's index */
        Bundle b = new Bundle();

        /** Storing the selected item's index in the bundle object */
        b.putInt("position", position);

        /** Setting the bundle object to the dialog fragment object */
        alert.setArguments(b);

        /** Creating the dialog fragment object, which will in turn open the alert dialog window */

        alert.show(fm, "alert_dialog_comment_delete");
    }

    public void onCommentDelete(final int position) {

        final Comment comment = commentsList.get(position);

        commentsList.remove(position);
        itemAdapter.notifyDataSetChanged();

        Api api = new Api(getActivity());

        api.commentDelete(comment.getId());
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//
//        super.onCreateOptionsMenu(menu, inflater);
//
//        menu.clear();
//
//        inflater.inflate(R.menu.menu_view_item, menu);
//
////        MainMenu = menu;
//    }
//
//    @Override
//    public void onPrepareOptionsMenu(Menu menu) {
//
//        super.onPrepareOptionsMenu(menu);
//
//        if (loadingComplete) {
//
//            if (App.getInstance().getId() != item.getFromUserId()) {
//
//                menu.removeItem(R.id.action_edit);
//                menu.removeItem(R.id.action_remove);
//            }
//
//            //show all menu items
//            hideMenuItems(menu, true);
//
//        } else {
//
//            //hide all menu items
//            hideMenuItems(menu, false);
//        }
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_edit: {

                // edit item

                onItemEdit(0);

                return true;
            }

            case R.id.action_remove: {

                // remove item

                onItemRemove(0);

                return true;
            }

            case R.id.action_report: {

                // report item

                report(0);

                return true;
            }

            default: {

                return super.onOptionsItemSelected(item);
            }
        }
    }

    private void hideMenuItems(Menu menu, boolean visible) {

        for (int i = 0; i < menu.size(); i++) {

            menu.getItem(i).setVisible(visible);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}