package ir.asgari.yekmive.adapter;

import android.app.Activity;
import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.asgari.yekmive.R;
import ir.asgari.yekmive.databinding.RecyclerBuyBinding;
import ir.asgari.yekmive.model.ModelBuy;

public class AdapterBuy extends RecyclerView.Adapter<AdapterBuy.Holder> {

    List<ModelBuy> items;
    Activity activity;
    Click click;

    public interface Click {
        public void itemClick(ModelBuy buy);
    }

    public AdapterBuy(List<ModelBuy> items, Activity activity, Click click) {
        this.items = items;
        this.activity = activity;
        this.click = click;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_buy, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.buyBinding.setItem(items.get(position));
        String price = items.get(position).getPrice();

        holder.buyBinding.parent.setOnClickListener(v -> {
                click.itemClick(items.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        private RecyclerBuyBinding buyBinding;

        public Holder(@NonNull RecyclerBuyBinding buyBinding) {
            super(buyBinding.getRoot());
            this.buyBinding = buyBinding;

        }
    }


}
