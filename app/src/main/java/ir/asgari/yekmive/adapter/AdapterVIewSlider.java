package ir.asgari.yekmive.adapter;

import android.app.Dialog;
import android.content.Context;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ir.asgari.yekmive.R;
import ir.asgari.yekmive.model.ImageItem;

public class AdapterVIewSlider extends
        SliderViewAdapter<AdapterVIewSlider.SliderAdapterVH> {

    private Context context;
    private List<ImageItem> mImageItems = new ArrayList<>();

    public AdapterVIewSlider(Context context, List<ImageItem> items) {
        this.context = context;
        mImageItems = items;
    }

    public void renewItems(List<ImageItem> ImageItems) {
        this.mImageItems = ImageItems;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        this.mImageItems.remove(position);
        notifyDataSetChanged();
    }

    public void addItem(ImageItem ImageItem) {
        this.mImageItems.add(ImageItem);
        notifyDataSetChanged();
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_view_image, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {
        ImageItem imageItem = mImageItems.get(position);
        if (!imageItem.getImageUrl().isEmpty())
            Picasso.with(context).load(imageItem.getImageUrl()).into(viewHolder.imageViewBackground);
        viewHolder.imageViewBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImage(imageItem.getImageUrl());
            }
        });

    }

    private void showImage(String imgUrl) {
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.fragment_view_image);
        ImageView im = dialog.findViewById(R.id.im);
        Picasso.with(context).load(imgUrl).into(im);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = 1000;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }

    @Override
    public int getCount() {
        return mImageItems.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.im);
            this.itemView = itemView;
        }
    }


}


