package ir.asgari.yekmive.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.asgari.yekmive.R;
import ir.asgari.yekmive.ViewItemCategory;
import ir.asgari.yekmive.ViewItemChildCategory;
import ir.asgari.yekmive.model.Category;
import ir.asgari.yekmive.model.ModelCategoryNewItem;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private Context mContext;
    private List<ModelCategoryNewItem.Item> itemList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageView thumbnail;
        public RelativeLayout parent;

        public MyViewHolder(View view) {

            super(view);

            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            parent = view.findViewById(R.id.parent);
        }
    }


    public CategoryAdapter(Context mContext, List<ModelCategoryNewItem.Item> itemList) {

        this.mContext = mContext;
        this.itemList = itemList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        ModelCategoryNewItem.Item c = itemList.get(position);
        holder.title.setText(c.getTitle());
        // loading album cover using Glide library
        Glide.with(mContext).load(c.getImgUrl()).into(holder.thumbnail);
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ViewItemChildCategory.class);
                intent.putExtra("category", c.getId());
                mContext.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {

        return itemList.size();
    }
}