package ir.asgari.yekmive.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.asgari.yekmive.R;
import ir.asgari.yekmive.ViewItemCategory;
import ir.asgari.yekmive.model.Category;
import ir.asgari.yekmive.model.ModelChildCategory;

public class ChildCategoryListAdapter extends RecyclerView.Adapter<ChildCategoryListAdapter.MyViewHolder> {

    private Activity activity;
    private List<ModelChildCategory.Item> itemList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageView thumbnail;
        RelativeLayout parent;

        public MyViewHolder(View view) {

            super(view);
            parent = view.findViewById(R.id.parent);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
        }
    }


    public ChildCategoryListAdapter(Activity activity, List<ModelChildCategory.Item> itemList) {
        this.activity = activity;
        this.itemList = itemList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list_row, parent, false);


        return new MyViewHolder(itemView);
    }

    public void setItemList(List<ModelChildCategory.Item> itemList) {
        this.itemList = itemList;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        ModelChildCategory.Item c = itemList.get(position);
        holder.title.setText(c.getChildcategoriesTag());
        holder.parent.setOnClickListener(v -> {
            Intent intent = new Intent(activity, ViewItemCategory.class);
            intent.putExtra("id", Integer.parseInt(c.getId()));
            intent.putExtra("image",c.getImageTwo());
            activity.startActivity(intent);

        });
        // loading album cover using Glide library
        if (c.getImageOne().equals("")||c.getImageOne().equals("system")){
            Glide.with(activity).load(R.drawable.no_image).into(holder.thumbnail);
            holder.thumbnail.setAlpha(0.3f);
        }else {
            Glide.with(activity).load(c.getImageOne()).into(holder.thumbnail);
        }
    }

    @Override
    public int getItemCount() {

        return itemList.size();
    }
}