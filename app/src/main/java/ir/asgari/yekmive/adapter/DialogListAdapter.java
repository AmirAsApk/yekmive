package ir.asgari.yekmive.adapter;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import github.ankushsachdeva.emojicon.EmojiconTextView;

import ir.asgari.yekmive.ProfileActivity;
import ir.asgari.yekmive.R;
import ir.asgari.yekmive.app.App;
import ir.asgari.yekmive.constants.Constants;
import ir.asgari.yekmive.model.Chat;
import ir.asgari.yekmive.model.ModelChatItem;

public class DialogListAdapter extends RecyclerView.Adapter<DialogListAdapter.ViewHolder> implements Constants {

    private AppCompatActivity activity;
    private LayoutInflater inflater;
    private List<ModelChatItem.Chat> chatList;
    Click click;
    ImageLoader imageLoader = App.getInstance().getImageLoader();


    public interface Click {
        void itemClick(int pos);
    }

    public void setClick(Click click) {
        this.click = click;
    }

    public DialogListAdapter(AppCompatActivity activity, List<ModelChatItem.Chat> chatList) {
        this.activity = activity;
        this.chatList = chatList;
    }

    public void setChatList(List<ModelChatItem.Chat> chatList) {
        this.chatList = chatList;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder h, int position) {
        ModelChatItem.Chat chat = chatList.get(position);
//        h.chatLastMessageAgo.setText(chat.getTimeAgo());
        h.chatNewMessagesCount.setText(chat.getNewMessagesCount());
        h.chatLastMessage.setText(chat.getLastMessageAgo());
        if (Integer.parseInt(chat.getNewMessagesCount()) <= 0) {
            h.chatNewMessagesCount.setVisibility(View.INVISIBLE);
        }

        if (chat.getItemTitle().trim().isEmpty()) {
            h.chatOpponentFullname.setText(chat.getWithUserFullname());
            if (!chat.getWithUserPhotoUrl().isEmpty())
                Picasso.with(activity).load(chat.getWithUserPhotoUrl()).into(h.chatOpponent);
        } else {
            h.chatOpponentFullname.setText(chat.getItemTitle());
            Picasso.with(activity).load(chat.getPreviewImgUrl()).into(h.chatOpponent);
        }


        h.parent.setOnClickListener(v -> click.itemClick(position));
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView chatOpponentFullname;
        public EmojiconTextView chatLastMessage;
        public TextView chatLastMessageAgo;
        public TextView chatNewMessagesCount;
        public CircularImageView chatOpponent;
        LinearLayout parent;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            chatOpponent = itemView.findViewById(R.id.chatOpponent);
            chatOpponentFullname = itemView.findViewById(R.id.chatOpponentFullname);
            chatLastMessage = itemView.findViewById(R.id.chatLastMessage);
            chatLastMessageAgo = itemView.findViewById(R.id.chatLastMessageAgo);
            chatNewMessagesCount = itemView.findViewById(R.id.chatNewMessagesCount);
            parent = itemView.findViewById(R.id.parent);
        }
    }


}