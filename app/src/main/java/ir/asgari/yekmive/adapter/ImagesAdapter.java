package ir.asgari.yekmive.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

import java.util.List;

import ir.asgari.yekmive.R;
import ir.asgari.yekmive.model.ImageItem;

public class ImagesAdapter extends PagerAdapter {

    private AppCompatActivity act;
    private List<ImageItem> items;

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {

        void onItemClick(View view, ImageItem obj, int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {

        this.onItemClickListener = onItemClickListener;
    }

    // constructor
    public ImagesAdapter(AppCompatActivity activity, List<ImageItem> items) {

        this.act = activity;
        this.items = items;
    }

    @Override
    public int getCount() {

        return this.items.size();
    }

    public ImageItem getItem(int pos) {

        return items.get(pos);
    }

    public void setItems(List<ImageItem> items) {

        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        final ImageItem item = items.get(position);

        LayoutInflater inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = inflater.inflate(R.layout.addon_item_image, container, false);

        ImageView mImg = (ImageView) v.findViewById(R.id.img);
        MaterialRippleLayout mParent = (MaterialRippleLayout) v.findViewById(R.id.parent);

        try {

            Glide.with(act).load(item.getImageUrl())
                    .transition(withCrossFade())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(mImg);

        } catch (Exception e) {

            Log.e("ImagesAdapter", e.toString());
        }

        mParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (onItemClickListener != null) {

                    onItemClickListener.onItemClick(v, item, position);
                }
            }
        });

        ((ViewPager) container).addView(v);

        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ((ViewPager) container).removeView((RelativeLayout) object);

    }

}
