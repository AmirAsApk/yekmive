package ir.asgari.yekmive.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.IOException;
import java.util.List;

import ir.asgari.yekmive.R;
import ir.asgari.yekmive.SharePrefHelper;
import ir.asgari.yekmive.ViewItemActivity;
import ir.asgari.yekmive.model.ModelSearch;
import ir.asgari.yekmive.model.ResultdeleteItem;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import ir.asgari.yekmive.utilorg.Helper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyItemAdapter extends RecyclerView.Adapter<MyItemAdapter.MyViewHolder> {

    private Activity activity;
    private List<ModelSearch.Item> itemList;
    String imageDefult;
    onBottomReachedListener onBottomReachedListener;
    SharePrefHelper pref;

    public void setOnBottomReachedListener(MyItemAdapter.onBottomReachedListener onBottomReachedListener) {
        this.onBottomReachedListener = onBottomReachedListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, price, date, location, fori;
        public ImageView thumbnail;
        public ProgressBar mProgressBar;
        public LinearLayout parent;
        public ImageView delete;
        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            price = view.findViewById(R.id.price);
            date = view.findViewById(R.id.date);
            parent = view.findViewById(R.id.parent);
            fori = view.findViewById(R.id.fori);
            location = view.findViewById(R.id.location);
            thumbnail = view.findViewById(R.id.thumbnail);
            mProgressBar = view.findViewById(R.id.progressBar);
            delete = view.findViewById(R.id.delete);
        }
    }

    public void setItemList(List<ModelSearch.Item> itemList) {
        this.itemList = itemList;
    }

    public MyItemAdapter(Activity activity, List<ModelSearch.Item> itemList, String imageDefult) {
        this.activity = activity;
        this.itemList = itemList;
        this.imageDefult = imageDefult;
    }

    public MyItemAdapter(Activity activity, List<ModelSearch.Item> itemList) {
        this.activity = activity;
        this.itemList = itemList;
        pref = new SharePrefHelper(activity);
    }


    public MyItemAdapter(Activity activity) {
        this.activity = activity;
    }

    public interface onBottomReachedListener {
        void onBottomReached(int position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mu_item_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if (onBottomReachedListener != null && position == itemList.size() - 1) {
            onBottomReachedListener.onBottomReached(position);
        }
        ModelSearch.Item item = itemList.get(position);
        holder.title.setText(item.getItemTitle());
        Helper helper = new Helper();
        holder.date.setText(item.getTimeAgo());
        if (item.getPrice().equals("0")) {
            holder.price.setText("توافقی");
        } else {
            holder.price.setText(item.getPrice() + activity.getString(R.string.label_currency));
        }
        holder.mProgressBar.setVisibility(View.VISIBLE);
        holder.thumbnail.setVisibility(View.VISIBLE);
        holder.location.setText(item.getCityTitle());

        if (item.getsPayment().equals("1")) {
            holder.parent.setBackgroundColor(activity.getResources().getColor(R.color.vizhe));
        } else {
            holder.parent.setBackgroundColor(activity.getResources().getColor(R.color.white));
        }

        if (item.getiPayment().equals("1")) {
            holder.fori.setVisibility(View.VISIBLE);
        } else {
            holder.fori.setVisibility(View.GONE);
        }

        final ImageView img = holder.thumbnail;
        final ProgressBar progressBar = holder.mProgressBar;

        holder.parent.setOnClickListener(v -> {
            Intent intent = new Intent(activity, ViewItemActivity.class);
            intent.putExtra("itemId", Long.valueOf(item.getId()));
            intent.putExtra("city", item.getCityTitle());
            intent.putExtra("createAt", item.getCreateAt());
            activity.startActivity(intent);
        });


        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("حذف اگهی");
                builder.setMessage("ایا مطمئن هستید که می خواهید اگهی را حذف کنید .");
                builder.setNegativeButton("خیر", null);
                builder.setPositiveButton("بله", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
                        serverRequest.deleteItem("1", pref.getAccountID(), pref.getAccessToken(), item.getId())
                                .enqueue(new Callback<ResultdeleteItem>() {
                                    @Override
                                    public void onResponse(Call<ResultdeleteItem> call, Response<ResultdeleteItem> response) {
                                        if (!response.body().getError())
                                            notifyDataSetChanged();
                                    }
                                    @Override
                                    public void onFailure(Call<ResultdeleteItem> call, Throwable t) {

                                    }
                                });
                    }
                }).setCancelable(false).show();

            }
        });

        if (item.getImgUrl() != null && !item.getImgUrl().isEmpty()) {
            Glide.with(activity)
                    .load(item.getImgUrl())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.mProgressBar.setVisibility(View.INVISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                    })
                    .into(holder.thumbnail);
        } else if (!imageDefult.isEmpty()) {
            Glide.with(activity)
                    .load(imageDefult)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.mProgressBar.setVisibility(View.INVISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                    })
                    .into(holder.thumbnail);
        } else {
            Glide.with(activity)
                    .load(R.drawable.no_image)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.mProgressBar.setVisibility(View.INVISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                    })
                    .into(holder.thumbnail);
        }
        // loading album cover using Glide library
//        Glide.with(mContext).load(item.getImgUrl()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}