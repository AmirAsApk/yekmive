package ir.asgari.yekmive.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ir.asgari.yekmive.ActivityViewItem;
import ir.asgari.yekmive.ActivityWebView;
import ir.asgari.yekmive.BuyActivity;
import ir.asgari.yekmive.MyItemActivity;
import ir.asgari.yekmive.ProfileActivity;
import ir.asgari.yekmive.R;
import ir.asgari.yekmive.SettingsActivity;
import ir.asgari.yekmive.SharePrefHelper;
import ir.asgari.yekmive.SupportActivity;
import ir.asgari.yekmive.app.App;
import ir.asgari.yekmive.view.LoginSms.LoginSmsCodeActivity;

import static ir.asgari.yekmive.ActivityViewItem.ID_LAYOUT;
import static ir.asgari.yekmive.FragmentDrawer.RESULT_LOGIN;

public class NavDrawerAdapter extends RecyclerView.Adapter<NavDrawerAdapter.ViewHolder> {

    String[] titles;
    TypedArray icons;
    Activity activity;
    SharePrefHelper prefHelper;
//    ImageLoader imageLoader = App.getInstance().getImageLoader();

    // The default constructor to receive titles,icons and context from MainActivity.
    public NavDrawerAdapter(String[] titles, TypedArray icons, Activity activity) {
        this.titles = titles;
        this.icons = icons;
        this.activity = activity;
        prefHelper = new SharePrefHelper(activity);
    }

    /**
     * Its a inner class to NavDrawerAdapter Class.
     * This ViewHolder class implements View.OnClickListener to handle click events.
     * If the itemType==1 ; it implies that the view is a single row_item with TextView and ImageView.
     * This ViewHolder describes an item view with respect to its place within the RecyclerView.
     * For every item there is a ViewHolder associated with it .
     */

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView navTitle, navCounter, tv1_header, tv2_header;
        ImageView im_header;
        ImageView navIcon;
        Context context;
        Button btn_header;
        RelativeLayout relativeLayout;

        public ViewHolder(View drawerItem, int itemType, Context context) {
            super(drawerItem);
            this.context = context;
            if (itemType == 1) {
                navCounter = itemView.findViewById(R.id.tv_NavCounter);
                navTitle = itemView.findViewById(R.id.tv_NavTitle);
                navIcon = itemView.findViewById(R.id.iv_NavIcon);
                relativeLayout = itemView.findViewById(R.id.rel);

            } else {
                im_header = itemView.findViewById(R.id.userPhoto);
                tv1_header = (TextView) itemView.findViewById(R.id.userFullname);
                tv2_header = (TextView) itemView.findViewById(R.id.userUsername);
                btn_header = itemView.findViewById(R.id.btn_header);
            }
        }
    }

    /**
     * This is called every time when we need a new ViewHolder and a new ViewHolder is required for every item in RecyclerView.
     * Then this ViewHolder is passed to onBindViewHolder to display items.
     */

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (viewType == 1) {
            View itemLayout = layoutInflater.inflate(R.layout.nav_drawer_row, parent, false);
            return new ViewHolder(itemLayout, viewType, activity);
        } else if (viewType == 0) {
            View itemHeader = layoutInflater.inflate(R.layout.header_navigation_drawer, parent, false);
            return new ViewHolder(itemHeader, viewType, activity);
        }
        return null;
    }

    /**
     * This method is called by RecyclerView.Adapter to display the data at the specified position.
     * This method should update the contents of the itemView to reflect the item at the given position.
     * So here , if position!=0 it implies its a row_item and we set the title and icon of the view.
     */

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//
//        if (imageLoader == null) {
//            imageLoader = App.getInstance().getImageLoader();
//        }
        if (position != 0) {

            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent nologin = new Intent(activity, ActivityViewItem.class);
                    nologin.putExtra(ID_LAYOUT, ActivityViewItem.NotLogin);

                    if (position == 1) {
                        if (prefHelper.isLogin()) {
                            Intent intent = new Intent(activity, ProfileActivity.class);
                            activity.startActivity(intent);
                        } else {
                            activity.startActivity(nologin);
                        }
                    } else if (position == 5) {
                        if (prefHelper.isLogin()) {
                            Intent intent = new Intent(activity, SettingsActivity.class);
                            activity.startActivity(intent);
                        } else {
                            activity.startActivity(nologin);
                        }

                    } else if (position == 6) {
                        if (prefHelper.isLogin()) {
                            Intent intent = new Intent(activity, BuyActivity.class);
                            activity.startActivity(intent);
                        } else {
                            activity.startActivity(nologin);
                        }
                    } else if (position == 2) {
                        if (prefHelper.isLogin()) {
                            Intent intent = new Intent(activity, MyItemActivity.class);
                            activity.startActivity(intent);
                        } else {
                            activity.startActivity(nologin);
                        }
                    } else if (position == 7) {
                        Intent intent = new Intent(activity, ActivityWebView.class);
                        intent.putExtra("url", "https://yekmive.ir/terms.php");
                        activity.startActivity(intent);
                    } else if (position == 8) {
                        if (prefHelper.isLogin()) {
                            Intent intent = new Intent(activity, SupportActivity.class);
                            activity.startActivity(intent);
                        } else {
                            activity.startActivity(nologin);
                        }
                    } else if (position == 9) {
                        showDialogVerSion();
                    } else {
                        Intent intent = new Intent(activity, ActivityViewItem.class);
                        intent.putExtra(ID_LAYOUT, position);
                        activity.startActivity(intent);
                    }
                }
            });

            switch (position) {
                case 6: {
                    holder.navCounter.setVisibility(View.GONE);
                    if (App.getInstance().getNotificationsCount() > 0) {
                        holder.navCounter.setText(Integer.toString(App.getInstance().getNotificationsCount()));
                        holder.navCounter.setVisibility(View.VISIBLE);
                    }

                    holder.navTitle.setText(titles[position - 1]);
                    holder.navIcon.setImageResource(icons.getResourceId(position - 1, -1));

                    break;
                }

                case 7: {

                    holder.navCounter.setVisibility(View.GONE);
                    if (App.getInstance().getMessagesCount() > 0) {
                        holder.navCounter.setText(Integer.toString(App.getInstance().getMessagesCount()));
                        holder.navCounter.setVisibility(View.VISIBLE);
                    }

                    holder.navTitle.setText(titles[position - 1]);
                    holder.navIcon.setImageResource(icons.getResourceId(position - 1, -1));
                    break;
                }
                default: {
                    holder.navCounter.setVisibility(View.GONE);
                    holder.navTitle.setText(titles[position - 1]);
                    holder.navIcon.setImageResource(icons.getResourceId(position - 1, -1));
                    break;
                }
            }


        }
        //header
        else {
            //is login
            if (prefHelper.isLogin()) {

                if (prefHelper.getImg().isEmpty()){
                    holder.im_header.setImageResource(R.drawable.app_logo);
                }else {
                    Picasso.with(activity).load(prefHelper.getImg()).into(holder.im_header);
                }

                if (prefHelper.getFullName().isEmpty()) {
                    holder.tv1_header.setText("کاربر یک میوه");
                } else {
                    holder.tv1_header.setText(prefHelper.getFullName());
                }

                holder.tv2_header.setText(prefHelper.getUserName());

                holder.btn_header.setText("خروج");
                holder.btn_header.setOnClickListener(v -> {
                    prefHelper.deleteParam();
                    notifyDataSetChanged();
                });
            }
            //not login
            else {
                holder.im_header.setImageResource(R.drawable.app_logo);
                holder.tv1_header.setText("yekmive");
                holder.tv2_header.setText("yekmive.ir");
                holder.btn_header.setText("ورود");
                holder.btn_header.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity.startActivityForResult(new Intent(activity, LoginSmsCodeActivity.class), RESULT_LOGIN);
                    }
                });

            }
        }

    }

    /**
     * It returns the total no. of items . We +1 count to include the header view.
     * So , it the total count is 5 , the method returns 6.
     * This 6 implies that there are 5 row_items and 1 header view with header at position zero.
     */

    @Override
    public int getItemCount() {
        return titles.length + 1;
    }

    /**
     * This methods returns 0 if the position of the item is '0'.
     * If the position is zero its a header view and if its anything else
     * its a row_item with a title and icon.
     */

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public void showDialogVerSion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(R.layout.about_dialog);


        builder.show();
    }

}