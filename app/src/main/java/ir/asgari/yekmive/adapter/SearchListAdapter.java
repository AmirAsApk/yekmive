package ir.asgari.yekmive.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import ir.asgari.yekmive.R;
import ir.asgari.yekmive.ViewItemActivity;
import ir.asgari.yekmive.model.ModelSearch;
import ir.asgari.yekmive.utilorg.Helper;

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.MyViewHolder> {

    private Activity activity;
    private List<ModelSearch.Item> itemList;
    String imageDefult = "";
    onBottomReachedListener onBottomReachedListener;

    public void setOnBottomReachedListener(SearchListAdapter.onBottomReachedListener onBottomReachedListener) {
        this.onBottomReachedListener = onBottomReachedListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title, price, date, location, fori;
        public ImageView thumbnail;

        public ProgressBar mProgressBar;
        public LinearLayout parent;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            price = view.findViewById(R.id.price);
            date = view.findViewById(R.id.date);
            parent = view.findViewById(R.id.parent);
            fori = view.findViewById(R.id.fori);
            location = view.findViewById(R.id.location);
            thumbnail = view.findViewById(R.id.thumbnail);
            mProgressBar = view.findViewById(R.id.progressBar);
        }
    }

    public void setItemList(List<ModelSearch.Item> itemList) {
        this.itemList = itemList;
    }

    public SearchListAdapter(Activity activity, List<ModelSearch.Item> itemList, String imageDefult) {
        this.activity = activity;
        this.itemList = itemList;
        this.imageDefult = imageDefult;
    }

    public void setImageDefult(String imageDefult) {
        this.imageDefult = imageDefult;
    }

    public SearchListAdapter(Activity activity, List<ModelSearch.Item> itemList) {
        this.activity = activity;
        this.itemList = itemList;
    }


    public SearchListAdapter(Activity activity) {
        this.activity = activity;
    }

    public interface onBottomReachedListener {
        void onBottomReached(int position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if (onBottomReachedListener != null && position == itemList.size() - 1) {
            onBottomReachedListener.onBottomReached(position);
        }
        ModelSearch.Item item = itemList.get(position);
        holder.title.setText(item.getItemTitle());
        Helper helper = new Helper();
        holder.date.setText(item.getTimeAgo());
        if (item.getPrice().equals("0")) {
            holder.price.setText("توافقی");
        } else {
            holder.price.setText(item.getPrice() + activity.getString(R.string.label_currency));
        }
        holder.mProgressBar.setVisibility(View.VISIBLE);
        holder.thumbnail.setVisibility(View.VISIBLE);
        holder.location.setText(item.getCityTitle());

        if (item.getsPayment().equals("1")) {
            holder.parent.setBackgroundColor(activity.getResources().getColor(R.color.vizhe));
        } else {
            holder.parent.setBackgroundColor(activity.getResources().getColor(R.color.white));
        }

        if (item.getiPayment().equals("1")) {
            holder.fori.setVisibility(View.VISIBLE);
        } else {
            holder.fori.setVisibility(View.INVISIBLE);
        }

        final ImageView img = holder.thumbnail;
        final ProgressBar progressBar = holder.mProgressBar;

        holder.parent.setOnClickListener(v -> {
            Intent intent = new Intent(activity, ViewItemActivity.class);
            intent.putExtra("itemId", Long.valueOf(item.getId()));
            intent.putExtra("city", item.getCityTitle());
            intent.putExtra("createAt", item.getCreateAt());
            activity.startActivity(intent);
        });


        if (item.getImgUrl() != null && !item.getImgUrl().isEmpty()) {
            Glide.with(activity)
                    .load(item.getImgUrl())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.mProgressBar.setVisibility(View.INVISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                    })
                    .into(holder.thumbnail);
        } else if (!imageDefult.isEmpty()) {
            Glide.with(activity)
                    .load(imageDefult)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.mProgressBar.setVisibility(View.INVISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                    })
                    .into(holder.thumbnail);
        } else {
            Glide.with(activity)
                    .load(R.drawable.no_image)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.mProgressBar.setVisibility(View.INVISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                    })
                    .into(holder.thumbnail);
        }
        // loading album cover using Glide library
//        Glide.with(mContext).load(item.getImgUrl()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}