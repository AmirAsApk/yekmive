package ir.asgari.yekmive.adapter;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import ir.asgari.yekmive.R;
import ir.asgari.yekmive.model.Item;
import ir.asgari.yekmive.utilorg.Helper;

public class StreamListAdapter extends RecyclerView.Adapter<StreamListAdapter.MyViewHolder> {

    private Context mContext;
    private List<Item> itemList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView price, date;
        TextView title;
        public ImageView thumbnail;

        public ProgressBar mProgressBar;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            price = view.findViewById(R.id.price);
            date = view.findViewById(R.id.date);
            thumbnail = view.findViewById(R.id.thumbnail);
            mProgressBar = view.findViewById(R.id.progressBar);
        }
    }


    public StreamListAdapter(Context mContext, List<Item> itemList) {
        this.mContext = mContext;
        this.itemList = itemList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        Item item = itemList.get(position);

        holder.title.setText(item.getTitle());

        Helper helper = new Helper();
        holder.date.setText(item.getTimeAgo());


        if (item.getPrice() <= 0) {
            holder.price.setText("توافقی");
        } else {
            holder.price.setText(helper.getFormatedAmount(item.getPrice()) + " " + mContext.getString(R.string.label_currency));
        }
        holder.mProgressBar.setVisibility(View.VISIBLE);
        holder.thumbnail.setVisibility(View.VISIBLE);

        final ImageView img = holder.thumbnail;
        final ProgressBar progressBar = holder.mProgressBar;

        Glide.with(mContext)
                .load(item.getImgUrl())
                .listener(new RequestListener<Drawable>() {

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.mProgressBar.setVisibility(View.INVISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                })
                .into(holder.thumbnail);

        // loading album cover using Glide library
//        Glide.with(mContext).load(item.getImgUrl()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}