package ir.asgari.yekmive.constants;

public interface Constants {

     Boolean EMOJI_KEYBOARD = true; // false = Do not display your own Emoji keyboard | true = allow display your own Emoji keyboard

     Boolean FACEBOOK_AUTHORIZATION = false; // false = Do not show buttons Login/Signup with Facebook | true = allow display buttons Login/Signup with Facebook

     String APP_TEMP_FOLDER = "marketplace"; //directory for temporary storage of images from the camera

     String WEB_SITE = "https://yekmive.ir/";  //web site url address

    String CLIENT_ID = "1";  //Client ID | For identify the application | Example: 12567

     String API_DOMAIN = "https://yekmive.ir/";  //url address to which the application sends requests

 String API_FILE_EXTENSION = ".inc.php"; // Do not change the value of this constant!
 String API_VERSION = "v1"; // Do not change the value of this constant!

     String METHOD_ACCOUNT_GET_SETTINGS = API_DOMAIN + "api/" + API_VERSION + "/method/account.getSettings" + API_FILE_EXTENSION;
     String METHOD_DIALOGS_GET = API_DOMAIN + "api/" + API_VERSION + "/method/dialogs.get" + API_FILE_EXTENSION;
     String METHOD_CHAT_UPDATE = API_DOMAIN + "api/" + API_VERSION + "/method/chat.update" + API_FILE_EXTENSION;

    String METHOD_ACCOUNT_LOGIN = API_DOMAIN + "api/" + API_VERSION + "/method/account.signIn" + API_FILE_EXTENSION;
    String METHOD_ACCOUNT_SIGNUP = API_DOMAIN + "api/" + API_VERSION + "/method/account.signUp" + API_FILE_EXTENSION;
    String METHOD_ACCOUNT_AUTHORIZE = API_DOMAIN + "api/" + API_VERSION + "/method/account.authorize" + API_FILE_EXTENSION;
    String METHOD_ACCOUNT_SET_GCM_TOKEN = API_DOMAIN + "api/" + API_VERSION + "/method/account.setGcmToken" + API_FILE_EXTENSION;
    String METHOD_ACCOUNT_LOGINBYFACEBOOK = API_DOMAIN + "api/" + API_VERSION + "/method/account.signInByFacebook" + API_FILE_EXTENSION;
    String METHOD_ACCOUNT_RECOVERY = API_DOMAIN + "api/" + API_VERSION + "/method/account.recovery" + API_FILE_EXTENSION;
    String METHOD_ACCOUNT_SETPASSWORD = API_DOMAIN + "api/" + API_VERSION + "/method/account.setPassword" + API_FILE_EXTENSION;
    String METHOD_ACCOUNT_DEACTIVATE = API_DOMAIN + "api/" + API_VERSION + "/method/account.deactivate" + API_FILE_EXTENSION;
    String METHOD_ACCOUNT_SAVE_SETTINGS = API_DOMAIN + "api/" + API_VERSION + "/method/account.saveSettings" + API_FILE_EXTENSION;
    String METHOD_ACCOUNT_CONNECT_TO_FACEBOOK = API_DOMAIN + "api/" + API_VERSION + "/method/account.connectToFacebook" + API_FILE_EXTENSION;
    String METHOD_ACCOUNT_DISCONNECT_FROM_FACEBOOK = API_DOMAIN + "api/" + API_VERSION + "/method/account.disconnectFromFacebook" + API_FILE_EXTENSION;
    String METHOD_ACCOUNT_LOGOUT = API_DOMAIN + "api/" + API_VERSION + "/method/account.logOut" + API_FILE_EXTENSION;
    String METHOD_ACCOUNT_SET_ALLOW_MESSAGES = API_DOMAIN + "api/" + API_VERSION + "/method/account.setAllowMessages" + API_FILE_EXTENSION;

     String METHOD_ACCOUNT_SET_GEO_LOCATION = API_DOMAIN + "api/" + API_VERSION + "/method/account.setGeoLocation" + API_FILE_EXTENSION;

     String METHOD_CATEGORIES_GET = API_DOMAIN + "api/" + API_VERSION + "/method/categories.get" + API_FILE_EXTENSION;
     String METHOD_CATEGORY_GET = API_DOMAIN + "api/" + API_VERSION + "/method/category.get" + API_FILE_EXTENSION;

     String METHOD_SUPPORT_SEND_TICKET = API_DOMAIN + "api/" + API_VERSION + "/method/support.sendTicket" + API_FILE_EXTENSION;

     String METHOD_SETTINGS_REVIEWS_GCM = API_DOMAIN + "api/" + API_VERSION + "/method/account.setAllowReviewsGCM" + API_FILE_EXTENSION;
     String METHOD_SETTINGS_COMMENTS_GCM = API_DOMAIN + "api/" + API_VERSION + "/method/account.setAllowCommentsGCM" + API_FILE_EXTENSION;
     String METHOD_SETTINGS_MESSAGES_GCM = API_DOMAIN + "api/" + API_VERSION + "/method/account.setAllowMessagesGCM" + API_FILE_EXTENSION;
     String METHOD_SETTINGS_COMMENT_REPLY_GCM = API_DOMAIN + "api/" + API_VERSION + "/method/account.setAllowCommentReplyGCM" + API_FILE_EXTENSION;

     String METHOD_PROFILE_GET = API_DOMAIN + "api/" + API_VERSION + "/method/profile.get" + API_FILE_EXTENSION;
     String METHOD_PROFILE_REPORT = API_DOMAIN + "api/" + API_VERSION + "/method/profile.report" + API_FILE_EXTENSION;
     String METHOD_PROFILE_UPLOADPHOTO = API_DOMAIN + "api/" + API_VERSION + "/method/profile.uploadPhoto" + API_FILE_EXTENSION;
     String METHOD_PROFILE_UPLOADCOVER = API_DOMAIN + "api/" + API_VERSION + "/method/profile.uploadCover" + API_FILE_EXTENSION;
     String METHOD_WALL_GET = API_DOMAIN + "api/" + API_VERSION + "/method/wall.get" + API_FILE_EXTENSION;


     String METHOD_BLACKLIST_GET = API_DOMAIN + "api/" + API_VERSION + "/method/blacklist.get" + API_FILE_EXTENSION;
     String METHOD_BLACKLIST_ADD = API_DOMAIN + "api/" + API_VERSION + "/method/blacklist.add" + API_FILE_EXTENSION;
     String METHOD_BLACKLIST_REMOVE = API_DOMAIN + "api/" + API_VERSION + "/method/blacklist.remove" + API_FILE_EXTENSION;

     String METHOD_NOTIFICATIONS_GET = API_DOMAIN + "api/" + API_VERSION + "/method/notifications.get" + API_FILE_EXTENSION;
     String METHOD_ITEM_GET = API_DOMAIN + "api/" + API_VERSION + "/method/item.get" + API_FILE_EXTENSION;
     String METHOD_STREAM_GET = API_DOMAIN + "api/" + API_VERSION + "/method/stream.get" + API_FILE_EXTENSION;

     String METHOD_APP_CHECKUSERNAME = API_DOMAIN + "api/" + API_VERSION + "/method/app.checkUsername" + API_FILE_EXTENSION;
     String METHOD_APP_TERMS = API_DOMAIN + "api/" + API_VERSION + "/method/app.terms" + API_FILE_EXTENSION;
     String METHOD_APP_THANKS = API_DOMAIN + "api/" + API_VERSION + "/method/app.thanks" + API_FILE_EXTENSION;
     String METHOD_APP_SEARCH = API_DOMAIN + "api/" + API_VERSION + "/method/app.search" + API_FILE_EXTENSION;

     String METHOD_ITEMS_REMOVE = API_DOMAIN + "api/" + API_VERSION + "/method/items.remove" + API_FILE_EXTENSION;
     String METHOD_ITEMS_UPLOAD_IMG = API_DOMAIN + "api/" + API_VERSION + "/method/items.uploadImg" + API_FILE_EXTENSION;
     String METHOD_ITEMS_NEW = API_DOMAIN + "api/" + API_VERSION + "/method/items.new" + API_FILE_EXTENSION;
     String METHOD_ITEMS_EDIT = API_DOMAIN + "api/" + API_VERSION + "/method/items.edit" + API_FILE_EXTENSION;
     String METHOD_ITEMS_REPORT = API_DOMAIN + "api/" + API_VERSION + "/method/items.report" + API_FILE_EXTENSION;
     String METHOD_ITEMS_LIKE = API_DOMAIN + "api/" + API_VERSION + "/method/items.like" + API_FILE_EXTENSION;

     String METHOD_FAVORITES_GET = API_DOMAIN + "api/" + API_VERSION + "/method/favorites.get" + API_FILE_EXTENSION;

     String METHOD_COMMENTS_REMOVE = API_DOMAIN + "api/" + API_VERSION + "/method/comments.remove" + API_FILE_EXTENSION;
     String METHOD_COMMENTS_NEW = API_DOMAIN + "api/" + API_VERSION + "/method/comments.new" + API_FILE_EXTENSION;

     String METHOD_CHAT_GET = API_DOMAIN + "api/" + API_VERSION + "/method/chat.get" + API_FILE_EXTENSION;
     String METHOD_CHAT_REMOVE = API_DOMAIN + "api/" + API_VERSION + "/method/chat.remove" + API_FILE_EXTENSION;
     String METHOD_CHAT_GET_PREVIOUS = API_DOMAIN + "api/" + API_VERSION + "/method/chat.getPrevious" + API_FILE_EXTENSION;

     String METHOD_MSG_NEW = API_DOMAIN + "api/" + API_VERSION + "/method/msg.new" + API_FILE_EXTENSION;
     String METHOD_MSG_UPLOAD_IMG = API_DOMAIN + "api/" + API_VERSION + "/method/msg.uploadImg" + API_FILE_EXTENSION;

     int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE_PHOTO = 1;                  //WRITE_EXTERNAL_STORAGE
     int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE_COVER = 2;                  //WRITE_EXTERNAL_STORAGE
     int MY_PERMISSIONS_REQUEST_ACCESS_LOCATION = 3;                               //ACCESS_COARSE_LOCATION
     int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 4;                        //WRITE_EXTERNAL_STORAGE
     int MY_PERMISSIONS_REQUEST_CALL_PHONE = 5;                                    //CALL_PHONE

     int VOLLEY_REQUEST_SECONDS = 10;                                              //SECONDS TO REQUEST

     int MODE_NEW = 0;
     int MODE_EDIT = 1;

     int LIST_ITEMS = 20;

     int POST_CHARACTERS_LIMIT = 1000;

     int ENABLED = 1;
     int DISABLED = 0;

     int GCM_ENABLED = 1;
     int GCM_DISABLED = 0;

     int ADMOB_ENABLED = 1;
     int ADMOB_DISABLED = 0;

     int COMMENTS_ENABLED = 1;
     int COMMENTS_DISABLED = 0;

     int MESSAGES_ENABLED = 1;
     int MESSAGES_DISABLED = 0;

     int ERROR_SUCCESS = 0;

     int SEX_UNKNOWN = 0;
     int SEX_MALE = 1;
     int SEX_FEMALE = 2;

     int NOTIFY_TYPE_LIKE = 0;
     int NOTIFY_TYPE_FOLLOWER = 1;
     int NOTIFY_TYPE_MESSAGE = 2;
     int NOTIFY_TYPE_COMMENT = 3;
     int NOTIFY_TYPE_COMMENT_REPLY = 4;
     int NOTIFY_TYPE_GIFT = 6;
     int NOTIFY_TYPE_REVIEW = 7;

     int GCM_NOTIFY_CONFIG = 0;
     int GCM_NOTIFY_SYSTEM = 1;
     int GCM_NOTIFY_CUSTOM = 2;
     int GCM_NOTIFY_LIKE = 3;
     int GCM_NOTIFY_ANSWER = 4;
     int GCM_NOTIFY_QUESTION = 5;
     int GCM_NOTIFY_COMMENT = 6;
     int GCM_NOTIFY_FOLLOWER = 7;
     int GCM_NOTIFY_PERSONAL = 8;
     int GCM_NOTIFY_MESSAGE = 9;
     int GCM_NOTIFY_COMMENT_REPLY = 10;
     int GCM_NOTIFY_GIFT = 14;
     int GCM_NOTIFY_REVIEW = 15;


     int ERROR_LOGIN_TAKEN = 300;
     int ERROR_EMAIL_TAKEN = 301;
     int ERROR_FACEBOOK_ID_TAKEN = 302;

     int ACCOUNT_STATE_ENABLED = 0;
     int ACCOUNT_STATE_DISABLED = 1;
     int ACCOUNT_STATE_BLOCKED = 2;
     int ACCOUNT_STATE_DEACTIVATED = 3;

     int ACCOUNT_TYPE_USER = 0;
     int ACCOUNT_TYPE_GROUP = 1;

     int ERROR_UNKNOWN = 100;
     int ERROR_ACCESS_TOKEN = 101;

     int ERROR_ACCOUNT_ID = 400;

     int UPLOAD_TYPE_PHOTO = 0;
     int UPLOAD_TYPE_COVER = 1;

     int ACTION_NEW = 1;
     int ACTION_EDIT = 2;
     int SELECT_POST_IMG = 3;
     int VIEW_CHAT = 4;
     int CREATE_POST_IMG = 5;
     int SELECT_CHAT_IMG = 6;
     int CREATE_CHAT_IMG = 7;
     int FEED_NEW_POST = 8;
     int FRIENDS_SEARCH = 9;
     int ITEM_EDIT = 10;
     int STREAM_NEW_POST = 11;
     int ACTION_LOGIN = 100;
     int ACTION_SIGNUP = 101;

     int ACCOUNT_ACCESS_LEVEL_AVAILABLE_TO_ALL = 0;
     int ACCOUNT_ACCESS_LEVEL_AVAILABLE_TO_FRIENDS = 1;

     String TAG = "TAG";

     String HASHTAGS_COLOR = "#5BCFF2";
}