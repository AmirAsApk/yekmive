package ir.asgari.yekmive.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountNewMassage {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("messagesCount")
    @Expose
    private Integer messagesCount;
    public Boolean getError() {
        return error;
    }
    public void setError(Boolean error) {
        this.error = error;
    }
    public Integer getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }
    public Integer getMessagesCount() {
        return messagesCount;
    }
    public void setMessagesCount(Integer messagesCount) {
        this.messagesCount = messagesCount;
    }
}