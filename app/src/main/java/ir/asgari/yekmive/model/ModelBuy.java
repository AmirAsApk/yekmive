package ir.asgari.yekmive.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelBuy {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("detilas")
    @Expose
    private String detilas;
    @SerializedName("key")
    @Expose
    private String key;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        if (!price.isEmpty() && Integer.parseInt(price) == 0 || price.equals("0")) {
            return "رایگان";
        } else {
            this.price = price;
        }
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDetilas() {
        return detilas;
    }

    public void setDetilas(String detilas) {
        this.detilas = detilas;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "ModelBuy{" +
                "name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", detilas='" + detilas + '\'' +
                ", key='" + key + '\'' +
                '}';
    }
}
