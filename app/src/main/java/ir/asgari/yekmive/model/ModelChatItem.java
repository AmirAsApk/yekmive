package ir.asgari.yekmive.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelChatItem {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("itemId")
    @Expose
    private String itemId;
    @SerializedName("chats")
    @Expose
    private List<Chat> chats = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public List<Chat> getChats() {
        return chats;
    }

    public void setChats(List<Chat> chats) {
        this.chats = chats;
    }

    public class Chat {
        @SerializedName("error")
        @Expose
        private Boolean error;
        @SerializedName("error_code")
        @Expose
        private Integer errorCode;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("fromUserId")
        @Expose
        private String fromUserId;
        @SerializedName("toUserId")
        @Expose
        private String toUserId;
        @SerializedName("toItemId")
        @Expose
        private String toItemId;
        @SerializedName("itemTitle")
        @Expose
        private String itemTitle;
        @SerializedName("previewImgUrl")
        @Expose
        private String previewImgUrl;
        @SerializedName("fromUserId_lastView")
        @Expose
        private String fromUserIdLastView;
        @SerializedName("toUserId_lastView")
        @Expose
        private String toUserIdLastView;
        @SerializedName("withUserId")
        @Expose
        private String withUserId;
        @SerializedName("withUserVerify")
        @Expose
        private String withUserVerify;
        @SerializedName("withUserState")
        @Expose
        private String withUserState;
        @SerializedName("withUserUsername")
        @Expose
        private String withUserUsername;
        @SerializedName("withUserFullname")
        @Expose
        private String withUserFullname;
        @SerializedName("withUserPhotoUrl")
        @Expose
        private String withUserPhotoUrl;
        @SerializedName("lastMessage")
        @Expose
        private String lastMessage;
        @SerializedName("lastMessageAgo")
        @Expose
        private String lastMessageAgo;
        @SerializedName("lastMessageCreateAt")
        @Expose
        private String lastMessageCreateAt;
        @SerializedName("newMessagesCount")
        @Expose
        private String newMessagesCount;
        @SerializedName("createAt")
        @Expose
        private String createAt;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timeAgo")
        @Expose
        private String timeAgo;
        @SerializedName("removeAt")
        @Expose
        private String removeAt;

        public Boolean getError() {
            return error;
        }

        public void setError(Boolean error) {
            this.error = error;
        }

        public Integer getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(Integer errorCode) {
            this.errorCode = errorCode;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFromUserId() {
            return fromUserId;
        }

        public void setFromUserId(String fromUserId) {
            this.fromUserId = fromUserId;
        }

        public String getToUserId() {
            return toUserId;
        }

        public void setToUserId(String toUserId) {
            this.toUserId = toUserId;
        }

        public String getToItemId() {
            return toItemId;
        }

        public void setToItemId(String toItemId) {
            this.toItemId = toItemId;
        }

        public String getItemTitle() {
            return itemTitle;
        }

        public void setItemTitle(String itemTitle) {
            this.itemTitle = itemTitle;
        }

        public String getPreviewImgUrl() {
            return previewImgUrl;
        }

        public void setPreviewImgUrl(String previewImgUrl) {
            this.previewImgUrl = previewImgUrl;
        }

        public String getFromUserIdLastView() {
            return fromUserIdLastView;
        }

        public void setFromUserIdLastView(String fromUserIdLastView) {
            this.fromUserIdLastView = fromUserIdLastView;
        }

        public String getToUserIdLastView() {
            return toUserIdLastView;
        }

        public void setToUserIdLastView(String toUserIdLastView) {
            this.toUserIdLastView = toUserIdLastView;
        }

        public String getWithUserId() {
            return withUserId;
        }

        public void setWithUserId(String withUserId) {
            this.withUserId = withUserId;
        }

        public String getWithUserVerify() {
            return withUserVerify;
        }

        public void setWithUserVerify(String withUserVerify) {
            this.withUserVerify = withUserVerify;
        }

        public String getWithUserState() {
            return withUserState;
        }

        public void setWithUserState(String withUserState) {
            this.withUserState = withUserState;
        }

        public String getWithUserUsername() {
            return withUserUsername;
        }

        public void setWithUserUsername(String withUserUsername) {
            this.withUserUsername = withUserUsername;
        }

        public String getWithUserFullname() {
            return withUserFullname;
        }

        public void setWithUserFullname(String withUserFullname) {
            this.withUserFullname = withUserFullname;
        }

        public String getWithUserPhotoUrl() {
            return withUserPhotoUrl;
        }

        public void setWithUserPhotoUrl(String withUserPhotoUrl) {
            this.withUserPhotoUrl = withUserPhotoUrl;
        }

        public String getLastMessage() {
            return lastMessage;
        }

        public void setLastMessage(String lastMessage) {
            this.lastMessage = lastMessage;
        }

        public String getLastMessageAgo() {
            return lastMessageAgo;
        }

        public void setLastMessageAgo(String lastMessageAgo) {
            this.lastMessageAgo = lastMessageAgo;
        }

        public String getLastMessageCreateAt() {
            return lastMessageCreateAt;
        }

        public void setLastMessageCreateAt(String lastMessageCreateAt) {
            this.lastMessageCreateAt = lastMessageCreateAt;
        }

        public String getNewMessagesCount() {
            return newMessagesCount;
        }

        public void setNewMessagesCount(String newMessagesCount) {
            this.newMessagesCount = newMessagesCount;
        }

        public String getCreateAt() {
            return createAt;
        }

        public void setCreateAt(String createAt) {
            this.createAt = createAt;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTimeAgo() {
            return timeAgo;
        }

        public void setTimeAgo(String timeAgo) {
            this.timeAgo = timeAgo;
        }

        public String getRemoveAt() {
            return removeAt;
        }

        public void setRemoveAt(String removeAt) {
            this.removeAt = removeAt;
        }

    }

}

