package ir.asgari.yekmive.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelChildCategory {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public class Item {
        @SerializedName("error")
        @Expose
        private Boolean error;
        @SerializedName("error_code")
        @Expose
        private Integer errorCode;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("itemsCount")
        @Expose
        private String itemsCount;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("category_id")
        @Expose
        private String categoryId;
        @SerializedName("category_url")
        @Expose
        private String categoryUrl;
        @SerializedName("exday")
        @Expose
        private String exday;
        @SerializedName("image_one")
        @Expose
        private String imageOne;
        @SerializedName("image_two")
        @Expose
        private String imageTwo;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("allow_mobile")
        @Expose
        private String allowMobile;
        @SerializedName("allow_comment")
        @Expose
        private String allowComment;
        @SerializedName("allow_chat")
        @Expose
        private String allowChat;
        @SerializedName("allow_date")
        @Expose
        private String allowDate;
        @SerializedName("allow_author")
        @Expose
        private String allowAuthor;
        @SerializedName("createAt")
        @Expose
        private String createAt;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("removeAt")
        @Expose
        private String removeAt;
        @SerializedName("childcategories_url")
        @Expose
        private String childcategoriesUrl;
        @SerializedName("childcategories_tag")
        @Expose
        private String childcategoriesTag;
        @SerializedName("childcategories_desc")
        @Expose
        private String childcategoriesDesc;

        public Boolean getError() {
            return error;
        }

        public void setError(Boolean error) {
            this.error = error;
        }

        public Integer getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(Integer errorCode) {
            this.errorCode = errorCode;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getItemsCount() {
            return itemsCount;
        }

        public void setItemsCount(String itemsCount) {
            this.itemsCount = itemsCount;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getCategoryUrl() {
            return categoryUrl;
        }

        public void setCategoryUrl(String categoryUrl) {
            this.categoryUrl = categoryUrl;
        }

        public String getExday() {
            return exday;
        }

        public void setExday(String exday) {
            this.exday = exday;
        }

        public String getImageOne() {
            return imageOne;
        }

        public void setImageOne(String imageOne) {
            this.imageOne = imageOne;
        }

        public String getImageTwo() {
            return imageTwo;
        }

        public void setImageTwo(String imageTwo) {
            this.imageTwo = imageTwo;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getAllowMobile() {
            return allowMobile;
        }

        public void setAllowMobile(String allowMobile) {
            this.allowMobile = allowMobile;
        }

        public String getAllowComment() {
            return allowComment;
        }

        public void setAllowComment(String allowComment) {
            this.allowComment = allowComment;
        }

        public String getAllowChat() {
            return allowChat;
        }

        public void setAllowChat(String allowChat) {
            this.allowChat = allowChat;
        }

        public String getAllowDate() {
            return allowDate;
        }

        public void setAllowDate(String allowDate) {
            this.allowDate = allowDate;
        }

        public String getAllowAuthor() {
            return allowAuthor;
        }

        public void setAllowAuthor(String allowAuthor) {
            this.allowAuthor = allowAuthor;
        }

        public String getCreateAt() {
            return createAt;
        }

        public void setCreateAt(String createAt) {
            this.createAt = createAt;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getRemoveAt() {
            return removeAt;
        }

        public void setRemoveAt(String removeAt) {
            this.removeAt = removeAt;
        }

        public String getChildcategoriesUrl() {
            return childcategoriesUrl;
        }

        public void setChildcategoriesUrl(String childcategoriesUrl) {
            this.childcategoriesUrl = childcategoriesUrl;
        }

        public String getChildcategoriesTag() {
            return childcategoriesTag;
        }

        public void setChildcategoriesTag(String childcategoriesTag) {
            this.childcategoriesTag = childcategoriesTag;
        }

        public String getChildcategoriesDesc() {
            return childcategoriesDesc;
        }

        public void setChildcategoriesDesc(String childcategoriesDesc) {
            this.childcategoriesDesc = childcategoriesDesc;
        }
    }
}
