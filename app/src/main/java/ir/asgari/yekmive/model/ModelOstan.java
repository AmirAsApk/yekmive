package ir.asgari.yekmive.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelOstan {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public class Item {

        @SerializedName("error")
        @Expose
        private Boolean error;
        @SerializedName("error_code")
        @Expose
        private Integer errorCode;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("itemsCount")
        @Expose
        private String itemsCount;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("lat")
        @Expose
        private Integer lat;
        @SerializedName("lng")
        @Expose
        private Integer lng;
        @SerializedName("createAt")
        @Expose
        private String createAt;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("removeAt")
        @Expose
        private String removeAt;

        public Boolean getError() {
            return error;
        }

        public void setError(Boolean error) {
            this.error = error;
        }

        public Integer getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(Integer errorCode) {
            this.errorCode = errorCode;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getItemsCount() {
            return itemsCount;
        }

        public void setItemsCount(String itemsCount) {
            this.itemsCount = itemsCount;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getLat() {
            return lat;
        }

        public void setLat(Integer lat) {
            this.lat = lat;
        }

        public Integer getLng() {
            return lng;
        }

        public void setLng(Integer lng) {
            this.lng = lng;
        }

        public String getCreateAt() {
            return createAt;
        }

        public void setCreateAt(String createAt) {
            this.createAt = createAt;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getRemoveAt() {
            return removeAt;
        }

        public void setRemoveAt(String removeAt) {
            this.removeAt = removeAt;
        }

    }
}