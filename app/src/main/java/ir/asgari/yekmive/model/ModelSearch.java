package ir.asgari.yekmive.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelSearch {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("itemsCount")
    @Expose
    private String itemsCount;
    @SerializedName("itemId")
    @Expose
    private String itemId;
    @SerializedName("query")
    @Expose
    private String query;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getItemsCount() {
        return itemsCount;
    }

    public void setItemsCount(String itemsCount) {
        this.itemsCount = itemsCount;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public class Item {
        @SerializedName("error")
        @Expose
        private Boolean error;
        @SerializedName("error_code")
        @Expose
        private Integer errorCode;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("category")
        @Expose
        private String category;
        @SerializedName("categoryTitle")
        @Expose
        private String categoryTitle;
        @SerializedName("childcategory")
        @Expose
        private String childcategory;
        @SerializedName("childcategoryTitle")
        @Expose
        private String childcategoryTitle;
        @SerializedName("price_agreement")
        @Expose
        private String priceAgreement;
        @SerializedName("min_price")
        @Expose
        private String minPrice;
        @SerializedName("max_price")
        @Expose
        private String maxPrice;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("kind")
        @Expose
        private String kind;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("cityTitle")
        @Expose
        private String cityTitle;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("addressTitle")
        @Expose
        private String addressTitle;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lng")
        @Expose
        private String lng;
        @SerializedName("fromUserId")
        @Expose
        private String fromUserId;
        @SerializedName("fromUserUsername")
        @Expose
        private String fromUserUsername;
        @SerializedName("fromUserFullname")
        @Expose
        private String fromUserFullname;
        @SerializedName("fromUserPhone")
        @Expose
        private String fromUserPhone;
        @SerializedName("fromUserPhoto")
        @Expose
        private String fromUserPhoto;
        @SerializedName("fromUserChat")
        @Expose
        private String fromUserChat;
        @SerializedName("fromUserShowMobile")
        @Expose
        private String fromUserShowMobile;
        @SerializedName("createAtMY")
        @Expose
        private String createAtMY;
        @SerializedName("itemTitle")
        @Expose
        private String itemTitle;
        @SerializedName("titleUrl")
        @Expose
        private String titleUrl;
        @SerializedName("itemDesc")
        @Expose
        private String itemDesc;
        @SerializedName("itemContent")
        @Expose
        private String itemContent;
        @SerializedName("previewImgUrl")
        @Expose
        private String previewImgUrl;
        @SerializedName("imgUrl")
        @Expose
        private String imgUrl;
        @SerializedName("allowComments")
        @Expose
        private String allowComments;
        @SerializedName("rating")
        @Expose
        private String rating;
        @SerializedName("itemType")
        @Expose
        private String itemType;
        @SerializedName("commentsCount")
        @Expose
        private String commentsCount;
        @SerializedName("likesCount")
        @Expose
        private String likesCount;
        @SerializedName("imagesCount")
        @Expose
        private String imagesCount;
        @SerializedName("viewsCount")
        @Expose
        private String viewsCount;
        @SerializedName("reviewsCount")
        @Expose
        private String reviewsCount;
        @SerializedName("reportsCount")
        @Expose
        private String reportsCount;
        @SerializedName("publish")
        @Expose
        private String publish;
        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("myLike")
        @Expose
        private Boolean myLike;
        @SerializedName("createAt")
        @Expose
        private String createAt;
        @SerializedName("fee_payment")
        @Expose
        private String feePayment;
        @SerializedName("fee_time")
        @Expose
        private Object feeTime;
        @SerializedName("i_payment")
        @Expose
        private String iPayment;
        @SerializedName("i_time")
        @Expose
        private Object iTime;
        @SerializedName("l_payment")
        @Expose
        private String lPayment;
        @SerializedName("l_time")
        @Expose
        private Object lTime;
        @SerializedName("s_payment")
        @Expose
        private String sPayment;
        @SerializedName("s_time")
        @Expose
        private Object sTime;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timeAgo")
        @Expose
        private String timeAgo;
        @SerializedName("admin_id")
        @Expose
        private String adminId;
        @SerializedName("removeAt")
        @Expose
        private String removeAt;
        @SerializedName("tag_id")
        @Expose
        private String tagId;

        public Boolean getError() {
            return error;
        }

        public void setError(Boolean error) {
            this.error = error;
        }

        public Integer getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(Integer errorCode) {
            this.errorCode = errorCode;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getCategoryTitle() {
            return categoryTitle;
        }

        public void setCategoryTitle(String categoryTitle) {
            this.categoryTitle = categoryTitle;
        }

        public String getChildcategory() {
            return childcategory;
        }

        public void setChildcategory(String childcategory) {
            this.childcategory = childcategory;
        }

        public String getChildcategoryTitle() {
            return childcategoryTitle;
        }

        public void setChildcategoryTitle(String childcategoryTitle) {
            this.childcategoryTitle = childcategoryTitle;
        }

        public String getPriceAgreement() {
            return priceAgreement;
        }

        public void setPriceAgreement(String priceAgreement) {
            this.priceAgreement = priceAgreement;
        }

        public String getMinPrice() {
            return minPrice;
        }

        public void setMinPrice(String minPrice) {
            this.minPrice = minPrice;
        }

        public String getMaxPrice() {
            return maxPrice;
        }

        public void setMaxPrice(String maxPrice) {
            this.maxPrice = maxPrice;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getKind() {
            return kind;
        }

        public void setKind(String kind) {
            this.kind = kind;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCityTitle() {
            return cityTitle;
        }

        public void setCityTitle(String cityTitle) {
            this.cityTitle = cityTitle;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAddressTitle() {
            return addressTitle;
        }

        public void setAddressTitle(String addressTitle) {
            this.addressTitle = addressTitle;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getFromUserId() {
            return fromUserId;
        }

        public void setFromUserId(String fromUserId) {
            this.fromUserId = fromUserId;
        }

        public String getFromUserUsername() {
            return fromUserUsername;
        }

        public void setFromUserUsername(String fromUserUsername) {
            this.fromUserUsername = fromUserUsername;
        }

        public String getFromUserFullname() {
            return fromUserFullname;
        }

        public void setFromUserFullname(String fromUserFullname) {
            this.fromUserFullname = fromUserFullname;
        }

        public String getFromUserPhone() {
            return fromUserPhone;
        }

        public void setFromUserPhone(String fromUserPhone) {
            this.fromUserPhone = fromUserPhone;
        }

        public String getFromUserPhoto() {
            return fromUserPhoto;
        }

        public void setFromUserPhoto(String fromUserPhoto) {
            this.fromUserPhoto = fromUserPhoto;
        }

        public String getFromUserChat() {
            return fromUserChat;
        }

        public void setFromUserChat(String fromUserChat) {
            this.fromUserChat = fromUserChat;
        }

        public String getFromUserShowMobile() {
            return fromUserShowMobile;
        }

        public void setFromUserShowMobile(String fromUserShowMobile) {
            this.fromUserShowMobile = fromUserShowMobile;
        }

        public String getCreateAtMY() {
            return createAtMY;
        }

        public void setCreateAtMY(String createAtMY) {
            this.createAtMY = createAtMY;
        }

        public String getItemTitle() {
            return itemTitle;
        }

        public void setItemTitle(String itemTitle) {
            this.itemTitle = itemTitle;
        }

        public String getTitleUrl() {
            return titleUrl;
        }

        public void setTitleUrl(String titleUrl) {
            this.titleUrl = titleUrl;
        }

        public String getItemDesc() {
            return itemDesc;
        }

        public void setItemDesc(String itemDesc) {
            this.itemDesc = itemDesc;
        }

        public String getItemContent() {
            return itemContent;
        }

        public void setItemContent(String itemContent) {
            this.itemContent = itemContent;
        }

        public String getPreviewImgUrl() {
            return previewImgUrl;
        }

        public void setPreviewImgUrl(String previewImgUrl) {
            this.previewImgUrl = previewImgUrl;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getAllowComments() {
            return allowComments;
        }

        public void setAllowComments(String allowComments) {
            this.allowComments = allowComments;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getItemType() {
            return itemType;
        }

        public void setItemType(String itemType) {
            this.itemType = itemType;
        }

        public String getCommentsCount() {
            return commentsCount;
        }

        public void setCommentsCount(String commentsCount) {
            this.commentsCount = commentsCount;
        }

        public String getLikesCount() {
            return likesCount;
        }

        public void setLikesCount(String likesCount) {
            this.likesCount = likesCount;
        }

        public String getImagesCount() {
            return imagesCount;
        }

        public void setImagesCount(String imagesCount) {
            this.imagesCount = imagesCount;
        }

        public String getViewsCount() {
            return viewsCount;
        }

        public void setViewsCount(String viewsCount) {
            this.viewsCount = viewsCount;
        }

        public String getReviewsCount() {
            return reviewsCount;
        }

        public void setReviewsCount(String reviewsCount) {
            this.reviewsCount = reviewsCount;
        }

        public String getReportsCount() {
            return reportsCount;
        }

        public void setReportsCount(String reportsCount) {
            this.reportsCount = reportsCount;
        }

        public String getPublish() {
            return publish;
        }

        public void setPublish(String publish) {
            this.publish = publish;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Boolean getMyLike() {
            return myLike;
        }

        public void setMyLike(Boolean myLike) {
            this.myLike = myLike;
        }

        public String getCreateAt() {
            return createAt;
        }

        public void setCreateAt(String createAt) {
            this.createAt = createAt;
        }

        public String getFeePayment() {
            return feePayment;
        }

        public void setFeePayment(String feePayment) {
            this.feePayment = feePayment;
        }

        public Object getFeeTime() {
            return feeTime;
        }

        public void setFeeTime(Object feeTime) {
            this.feeTime = feeTime;
        }

        public String getiPayment() {
            return iPayment;
        }

        public void setiPayment(String iPayment) {
            this.iPayment = iPayment;
        }

        public Object getiTime() {
            return iTime;
        }

        public void setiTime(Object iTime) {
            this.iTime = iTime;
        }

        public String getlPayment() {
            return lPayment;
        }

        public void setlPayment(String lPayment) {
            this.lPayment = lPayment;
        }

        public Object getlTime() {
            return lTime;
        }

        public void setlTime(Object lTime) {
            this.lTime = lTime;
        }

        public String getsPayment() {
            return sPayment;
        }

        public void setsPayment(String sPayment) {
            this.sPayment = sPayment;
        }

        public Object getsTime() {
            return sTime;
        }

        public void setsTime(Object sTime) {
            this.sTime = sTime;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTimeAgo() {
            return timeAgo;
        }

        public void setTimeAgo(String timeAgo) {
            this.timeAgo = timeAgo;
        }

        public String getAdminId() {
            return adminId;
        }

        public void setAdminId(String adminId) {
            this.adminId = adminId;
        }

        public String getRemoveAt() {
            return removeAt;
        }

        public void setRemoveAt(String removeAt) {
            this.removeAt = removeAt;
        }

        public String getTagId() {
            return tagId;
        }

        public void setTagId(String tagId) {
            this.tagId = tagId;
        }

        @Override
        public String toString() {
            return "Item{" +
                    "error=" + error +
                    ", errorCode=" + errorCode +
                    ", id='" + id + '\'' +
                    ", category='" + category + '\'' +
                    ", categoryTitle='" + categoryTitle + '\'' +
                    ", childcategory='" + childcategory + '\'' +
                    ", childcategoryTitle='" + childcategoryTitle + '\'' +
                    ", priceAgreement='" + priceAgreement + '\'' +
                    ", minPrice='" + minPrice + '\'' +
                    ", maxPrice='" + maxPrice + '\'' +
                    ", price='" + price + '\'' +
                    ", kind='" + kind + '\'' +
                    ", city='" + city + '\'' +
                    ", cityTitle='" + cityTitle + '\'' +
                    ", address='" + address + '\'' +
                    ", addressTitle='" + addressTitle + '\'' +
                    ", lat='" + lat + '\'' +
                    ", lng='" + lng + '\'' +
                    ", fromUserId='" + fromUserId + '\'' +
                    ", fromUserUsername='" + fromUserUsername + '\'' +
                    ", fromUserFullname='" + fromUserFullname + '\'' +
                    ", fromUserPhone='" + fromUserPhone + '\'' +
                    ", fromUserPhoto='" + fromUserPhoto + '\'' +
                    ", fromUserChat='" + fromUserChat + '\'' +
                    ", fromUserShowMobile='" + fromUserShowMobile + '\'' +
                    ", createAtMY='" + createAtMY + '\'' +
                    ", itemTitle='" + itemTitle + '\'' +
                    ", titleUrl='" + titleUrl + '\'' +
                    ", itemDesc='" + itemDesc + '\'' +
                    ", itemContent='" + itemContent + '\'' +
                    ", previewImgUrl='" + previewImgUrl + '\'' +
                    ", imgUrl='" + imgUrl + '\'' +
                    ", allowComments='" + allowComments + '\'' +
                    ", rating='" + rating + '\'' +
                    ", itemType='" + itemType + '\'' +
                    ", commentsCount='" + commentsCount + '\'' +
                    ", likesCount='" + likesCount + '\'' +
                    ", imagesCount='" + imagesCount + '\'' +
                    ", viewsCount='" + viewsCount + '\'' +
                    ", reviewsCount='" + reviewsCount + '\'' +
                    ", reportsCount='" + reportsCount + '\'' +
                    ", publish='" + publish + '\'' +
                    ", token='" + token + '\'' +
                    ", myLike=" + myLike +
                    ", createAt='" + createAt + '\'' +
                    ", feePayment='" + feePayment + '\'' +
                    ", feeTime=" + feeTime +
                    ", iPayment='" + iPayment + '\'' +
                    ", iTime=" + iTime +
                    ", lPayment='" + lPayment + '\'' +
                    ", lTime=" + lTime +
                    ", sPayment='" + sPayment + '\'' +
                    ", sTime=" + sTime +
                    ", date='" + date + '\'' +
                    ", timeAgo='" + timeAgo + '\'' +
                    ", adminId='" + adminId + '\'' +
                    ", removeAt='" + removeAt + '\'' +
                    ", tagId='" + tagId + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ModelSearch{" +
                "error=" + error +
                ", errorCode=" + errorCode +
                ", itemsCount='" + itemsCount + '\'' +
                ", itemId='" + itemId + '\'' +
                ", query='" + query + '\'' +
                ", items=" + items +
                '}';
    }
}

