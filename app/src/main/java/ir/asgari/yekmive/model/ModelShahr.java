package ir.asgari.yekmive.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelShahr {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "ModelShahr{" +
                "error=" + error +
                ", errorCode=" + errorCode +
                ", items=" + items +
                '}';
    }

    public class Item {
        @SerializedName("error")
        @Expose
        private Boolean error;
        @SerializedName("error_code")
        @Expose
        private Integer errorCode;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("itemsCount")
        @Expose
        private String itemsCount;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("city_id")
        @Expose
        private String cityId;
        @SerializedName("createAt")
        @Expose
        private String createAt;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("removeAt")
        @Expose
        private String removeAt;

        public Boolean getError() {
            return error;
        }

        public void setError(Boolean error) {
            this.error = error;
        }

        public Integer getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(Integer errorCode) {
            this.errorCode = errorCode;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getItemsCount() {
            return itemsCount;
        }

        public void setItemsCount(String itemsCount) {
            this.itemsCount = itemsCount;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public String getCreateAt() {
            return createAt;
        }

        public void setCreateAt(String createAt) {
            this.createAt = createAt;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getRemoveAt() {
            return removeAt;
        }

        public void setRemoveAt(String removeAt) {
            this.removeAt = removeAt;
        }

        @Override
        public String toString() {
            return "Item{" +
                    "error=" + error +
                    ", errorCode=" + errorCode +
                    ", id='" + id + '\'' +
                    ", itemsCount='" + itemsCount + '\'' +
                    ", title='" + title + '\'' +
                    ", cityId='" + cityId + '\'' +
                    ", createAt='" + createAt + '\'' +
                    ", date='" + date + '\'' +
                    ", removeAt='" + removeAt + '\'' +
                    '}';
        }
    }
}