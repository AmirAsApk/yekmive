package ir.asgari.yekmive.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelUploadImage {
    @Expose
    @SerializedName("error")
    String error;
    @Expose
    @SerializedName("imgUrl")
    String imgUrl;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
