package ir.asgari.yekmive.model;

import android.content.Context;
import android.content.SharedPreferences;

public class PreNewItemHelper {

    public static String OSTAN = "ostan";
    public static String SHAHR = "shahr";
    public static String CATEGORY = "category";
    public static String CHILD_CATEGORY = "child_category";

    SharedPreferences spref;
    SharedPreferences.Editor editor;

    public PreNewItemHelper(Context context) {
        spref = context.getSharedPreferences("newitem", Context.MODE_PRIVATE);
        editor = spref.edit();
    }

    public void setostan( int ostan) {
        editor.putInt(OSTAN, ostan);
        editor.commit();
    }

    public int getostan() {
        return spref.getInt(OSTAN,0);
    }

    public void setshahr(String shahr) {
        editor.putString(shahr, shahr);
        editor.commit();
    }

    public int getshahr() {
        return spref.getInt(SHAHR, 0);
    }

    public void setcategory(int category) {
        editor.putInt(CATEGORY, category);
        editor.commit();
    }

    public int getcategory() {
        return spref.getInt(CATEGORY, 0);
    }

    public void setchildcategory(int childcategory) {
        editor.putInt(CHILD_CATEGORY, childcategory);
        editor.commit();
    }

    public int getchildcategory() {
        return spref.getInt(CHILD_CATEGORY, 0);
    }


}
