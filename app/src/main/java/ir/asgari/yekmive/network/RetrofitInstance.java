package ir.asgari.yekmive.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static ir.asgari.yekmive.network.ServerUrl.BASE_URL;

public class RetrofitInstance {
    public static Retrofit retrofit;

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BASE_URL)
                    .build();
        }

        return retrofit;
    }
public static Retrofit getRetrofitInstance2() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://nominatim.openstreetmap.org/")
                    .build();
        }

        return retrofit;
    }

}
