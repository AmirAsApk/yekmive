package ir.asgari.yekmive.network;


import android.text.GetChars;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ir.asgari.yekmive.ModelGetProfile;
import ir.asgari.yekmive.model.CountNewMassage;
import ir.asgari.yekmive.model.ModelBuy;
import ir.asgari.yekmive.model.ModelCategoryNewItem;
import ir.asgari.yekmive.model.ModelChatItem;
import ir.asgari.yekmive.model.ModelChildCategory;
import ir.asgari.yekmive.model.ModelCityLatLon;
import ir.asgari.yekmive.model.ModelEditProfile;
import ir.asgari.yekmive.model.ModelNewItem;
import ir.asgari.yekmive.model.ModelOstan;
import ir.asgari.yekmive.model.ModelResultSendBuy;
import ir.asgari.yekmive.model.ModelSearch;
import ir.asgari.yekmive.model.ModelShahr;
import ir.asgari.yekmive.model.ModelUploadImage;
import ir.asgari.yekmive.model.ResultdeleteItem;
import ir.asgari.yekmive.view.LoginSms.LoginSmsCheckCodeModel;
import ir.asgari.yekmive.view.LoginSms.LoginSmsModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServerRequest {
    @GET("account.loginSms.inc.php")
    Call<LoginSmsModel> sendPhone(@Query("clientId") String clientId, @Query("req") String req, @Query("mobile") String mobile);

    @GET("account.loginSms.inc.php")
    Call<LoginSmsCheckCodeModel> checkCode(@Query("clientId") String clientId, @Query("req") String req, @Query("mobile") String mobile, @Query("code") String code);

    @GET("categories.get.inc.php")
    Call<ModelCategoryNewItem> getCategoryNewItem(@Query("accountId") int accountId, @Query("itemId") int itemId);

    @GET("app.search.inc.php")
    Call<ModelSearch> getSearch(@Query("query") String query,
                                @Query("itemId") String itemId,
                                @Query("ad_search") String ad_search,
                                @Query("childcategory_id") String childcategory_id,
                                @Query("Province") String province,
                                @Query("city") String city,
                                @Query("min_price") String min_price,
                                @Query("max_price") String max_price,
                                @Query("tag_url") String tag_url);


    @GET("address.ostan.get.inc.php?")
    Call<ModelOstan> getOstan(@Query("clientId") String clientId);

    @GET("address.shahr.get.inc.php?")
    Call<ModelShahr> getShahr(@Query("clientId") String clientId);


    @GET("childcategories.get.inc.php")
    Call<ModelChildCategory> getChildCategory(@Query("accountId") String accountId, @Query("itemId") String itemId, @Query("categoryId") String categoryId);

    @GET("account.authorize.inc.php")
    Call<ModelGetProfile> getProfile(@Query("clientId") String clientId, @Query("accountId") String accountId, @Query("accessToken") String accessToken);

    @GET("account.saveSettings.inc.php")
    Call<ModelEditProfile> setProfile(@Query("clientId") String clientId,
                                      @Query("accountId") String accountId,
                                      @Query("accessToken") String accessToken,
                                      @Query("status") String bio,
                                      @Query("fullname") String fullname,
                                      @Query("facebookPage") String facebookPage,
                                      @Query("instagramPage") String instagramPage, @Query("facebookPage") String phone
    );

    @GET("chat.getNewMessagesCount.inc.php")
    Call<CountNewMassage> getCountNewMassage(@Query("clientId") String clientId,
                                             @Query("accountId") String accountId,
                                             @Query("accessToken") String accessToken);


    @GET("items.new.inc.php")
    Call<ModelNewItem> sendNewItem(@Query("clientId") int clientId,
                                   @Query("accountId") String accountId,
                                   @Query("accessToken") String accessToken,
                                   @Query("token") String token,
                                   @Query("categoryId") String categoryId,
                                   @Query("childcategoryId") String childcategoryId,
                                   @Query("title") String title,
                                   @Query("description") String description,
                                   @Query("ostan") int ostan,
                                   @Query("shahr") int shahr,
                                   @Query("kind") int kind,
                                   @Query("min_price") String min_price,
                                   @Query("max_price") String max_price,
                                   @Query("allowComments") int allowComments,
                                   @Query("agreement") int agreement,
                                   @Query("lat") double lat,
                                   @Query("lng") double lng,
                                   @Query("imgUrlOne") String imgUrlOne,
                                   @Query("imgUrTwo") String imgUrTwo,
                                   @Query("imgUrTree") String imgUrTree,
                                   @Query("imgUrFour") String imgUrFour
    );

    @Multipart
    @POST("items.uploadImg.inc.php")
    Call<ModelUploadImage> sendImage(@Part MultipartBody.Part body,
                                     @Part("token") String token,
                                     @Part("clientId") String clientId,
                                     @Part("accountId") String accountId,
                                     @Part("accessToken") String accessToken
    );


    @GET("wall.get.inc.php")
    Call<ModelSearch> getMyItem(
            @Query("clientId") String clientId,
            @Query("itemId") String itemId,
            @Query("accountId") String accountId,
            @Query("accessToken") String accessToken,
            @Query("profileId") String profileId,
            @Query("usermobile") String usermobile
    );

    @GET("upgrade_price.php")
    Call<List<ModelBuy>> getBuyPlan(@Query("clientId") String clientId);

    @FormUrlEncoded
    @POST("stream.get.inc.php")
    Call<ModelSearch> getOrgItem(@Field("clientId") String clientId,
                                 @Field("accountId") String accountId,
                                 @Field("accessToken") String accessToken,
                                 @Field("itemId") int itemId,
                                 @Field("language") String language
    );

    @FormUrlEncoded
    @POST("dialogs.get.inc.php")
    Call<ModelChatItem> getItemChat(@Field("accountId") String accountId,
                                    @Field("accessToken") String accessToken,
                                    @Field("itemId") String itemId,
                                    @Field("language") String language
    );

    @GET("upgrade_item.php")
    Call<ModelResultSendBuy> sendBuy(
            @Query("clientId") String clientId,
            @Query("accountId") String accountId,
            @Query("accessToken") String accessToken,
            @Query("itemId") String itemId,
            @Query("Price") String Price,
            @Query("Response") String Response,
            @Query("Pakage_name") String Pakage_name,
            @Query("nardebaan") String nardebaan,
            @Query("fori") String fori,
            @Query("vizhe") String vizhe
    );

    @GET("address.shahr.get.inc.php")
    Call<ModelShahr> getShahrWithOstan(@Query("OstanId") String ostanId);

    @FormUrlEncoded
    @POST("items.remove.inc.php")
    Call<ResultdeleteItem> deleteItem(@Field("clientId") String clientId,
                                      @Field("accountId") String accountId,
                                      @Field("accessToken") String accessToken,
                                      @Field("itemId") String itemId
    );


    @GET("search")
    Call<ModelCityLatLon> getCityLatLog(@Query("format") String format, @Query("q") String city);

}
