package ir.asgari.yekmive.util.communication;


import ir.asgari.yekmive.util.IabResult;

public interface BillingSupportCommunication {
    void onBillingSupportResult(int response);
    void remoteExceptionHappened(IabResult result);
}
