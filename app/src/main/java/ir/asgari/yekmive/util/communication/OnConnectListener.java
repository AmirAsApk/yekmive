package ir.asgari.yekmive.util.communication;

public interface OnConnectListener {
    void connected();
}
