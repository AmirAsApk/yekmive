package ir.asgari.yekmive.utilorg;

public interface BlacklistItemInterface {

    public void remove(int position);
}