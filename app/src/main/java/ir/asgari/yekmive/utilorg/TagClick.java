/*
 * interface class which passes the hash tag click to the main page
 * 
 *  @auther Ramesh M Nair
 * */
package ir.asgari.yekmive.utilorg;

public interface TagClick {

	public void clickedTag(CharSequence tag);
}
