package ir.asgari.yekmive.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import ir.asgari.yekmive.R;
import ir.asgari.yekmive.adapter.CategoryAdapter;
import ir.asgari.yekmive.databinding.FragmentCategoryBinding;
import ir.asgari.yekmive.model.ModelCategoryNewItem;
import ir.asgari.yekmive.model.ModelSearch;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryFragment extends BaseFragment<FragmentCategoryBinding> {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding.setMassage("");
        binding.containerItems.setOnRefreshListener(() -> getCategory());
        getCategory();
        return binding.getRoot();
    }

    private void getCategory() {
        binding.containerItems.setRefreshing(true);
        ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
        Call<ModelCategoryNewItem> call = serverRequest.getCategoryNewItem(0, 0);
        call.enqueue(new Callback<ModelCategoryNewItem>() {
            @Override
            public void onResponse(Call<ModelCategoryNewItem> call, Response<ModelCategoryNewItem> response) {
                if (response.body().getItems().size()==0){
                    binding.setMassage("موردی یافت نشد!!!");
                }else {
                    binding.recyclerView.setAdapter(new CategoryAdapter(getContext(),response.body().getItems()));

                }
                binding.containerItems.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ModelCategoryNewItem> call, Throwable t) {
                binding.containerItems.setRefreshing(false);
                binding.setMassage("خطا لطفا دوباره تلاش کنید ");
            }
        });

    }

    @Override
    public int getIdLayout() {
        return R.layout.fragment_category;
    }
}
