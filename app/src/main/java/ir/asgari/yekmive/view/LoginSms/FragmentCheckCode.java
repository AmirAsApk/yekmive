package ir.asgari.yekmive.view.LoginSms;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ir.asgari.yekmive.R;
import ir.asgari.yekmive.SharePrefHelper;
import ir.asgari.yekmive.app.App;
import ir.asgari.yekmive.constants.Constants;
import ir.asgari.yekmive.databinding.FragmentCheckCodeBinding;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import ir.asgari.yekmive.view.BaseFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCheckCode extends BaseFragment<FragmentCheckCodeBinding> implements View.OnClickListener, Constants {

    String number;
    SharePrefHelper prefHelper;
    public boolean bResend = false;

    public FragmentCheckCode(String number) {
        this.number = number;
    }


    CountDownTimer timer = new CountDownTimer(60000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            int s = (int) (millisUntilFinished / 1000);
            binding.resend.setText(" ارسال دوباره کد تایید تا " + s + " ثانیه دیگر ");
        }

        @Override
        public void onFinish() {
            bResend = true;
            binding.resend.setText("ارسال مجدد کد تایید");
        }


    };


    public void startCdt() {
        bResend = false;
        timer.start();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding.done.setOnClickListener(this);
        binding.back.setOnClickListener(v -> ((LoginSmsCodeActivity) getActivity()).setFragment(new FragmentGetPhone()));
        binding.progressBar.setVisibility(View.INVISIBLE);
        prefHelper = new SharePrefHelper(getContext());
        startCdt();

        binding.resend.setOnClickListener(this::resendCode);

        return binding.getRoot();
    }

    @Override
    public int getIdLayout() {
        return R.layout.fragment_check_code;
    }

    @Override
    public void onClick(View v) {
        if (checkCode()) {
            binding.progressBar.setVisibility(View.VISIBLE);
            binding.done.setAlpha(0.5f);
            binding.done.setEnabled(false);
            ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
            Call<LoginSmsCheckCodeModel> call = serverRequest.checkCode(CLIENT_ID, "2", number, binding.getCode());
            call.enqueue(new Callback<LoginSmsCheckCodeModel>() {
                @Override
                public void onResponse(Call<LoginSmsCheckCodeModel> call, Response<LoginSmsCheckCodeModel> response) {
                    int satus = response.body().getApiStatos();
                    switch (satus) {
                        case 1:
                            Toast.makeText(getContext(), "شماره موبایل وارد کنید", Toast.LENGTH_SHORT).show();
                            break;
                        case 2:
                            Toast.makeText(getContext(), "فورمت شماره موبایل نادرست است", Toast.LENGTH_SHORT).show();
                            break;
                        case 3:
                            Toast.makeText(getContext(), "کد دریافتی را وارد کنید", Toast.LENGTH_SHORT).show();
                            break;
                        case 4:
                            Toast.makeText(getContext(), "تعداد تلاش بیش از حد بوده بعدا از ۲۴ ساعت امتحان کنید", Toast.LENGTH_SHORT).show();
                            break;
                        case 5:
                            Toast.makeText(getContext(), "این شماره توسط مدریت بلاک شده", Toast.LENGTH_SHORT).show();
                            break;
                        case 6:
                            Toast.makeText(getContext(), "کد فعال سازی برایتان پیامک شد", Toast.LENGTH_SHORT).show();
                            break;
                        case 7:
                            Toast.makeText(getContext(), "این شماره موجود نمی باشد", Toast.LENGTH_SHORT).show();
                            break;
                        case 8:
                            Toast.makeText(getContext(), "با موفقیت وارد شدید", Toast.LENGTH_SHORT).show();
                            break;
                        case 9:
                            Toast.makeText(getContext(), "کد فعال سازی نادرست است", Toast.LENGTH_SHORT).show();
                            break;
                    }
                    if (!response.body().getError()) {
                        saveData(response.body());
                        if (getActivity() != null) {
                            getActivity().setResult(Activity.RESULT_OK);
                            getActivity().finish();
                        }
                    }
                    binding.progressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<LoginSmsCheckCodeModel> call, Throwable t) {
                    binding.progressBar.setVisibility(View.INVISIBLE);
                    binding.done.setAlpha(1);
                    binding.done.setEnabled(true);
                }
            });
        }
    }

    private void saveData(LoginSmsCheckCodeModel loginSmsCheckCodeModel) {
        LoginSmsCheckCodeModel.Account item = loginSmsCheckCodeModel.getAccount().get(0);
        prefHelper.setParams(
                item.getMobile(),
                item.getUsername(),
                item.getFullname(),
                loginSmsCheckCodeModel.getAccessToken(),
                loginSmsCheckCodeModel.getAccountId(),
                item.getId(),
                item.getLowPhotoUrl()
        );
    }

    public void resendCode(View v) {
        Log.e("loooogass",bResend+"");
        if (bResend) {
            ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
            Call<LoginSmsModel> call = serverRequest.sendPhone(CLIENT_ID, "1", number);
            call.enqueue(new Callback<LoginSmsModel>() {
                @Override
                public void onResponse(Call<LoginSmsModel> call, Response<LoginSmsModel> response) {
                    {
                        int satus = response.body().getApiStatos();
                        switch (satus) {
                            case 1:
                                Toast.makeText(getContext(), "شماره موبایل وارد کنید", Toast.LENGTH_SHORT).show();
                                break;
                            case 2:
                                Toast.makeText(getContext(), "فورمت شماره موبایل نادرست است", Toast.LENGTH_SHORT).show();
                                break;
                            case 3:
                                Toast.makeText(getContext(), "کد دریافتی را وارد کنید", Toast.LENGTH_SHORT).show();
                                break;
                            case 4:
                                Toast.makeText(getContext(), "تعداد تلاش بیش از حد بوده بعدا از ۲۴ ساعت امتحان کنید", Toast.LENGTH_SHORT).show();
                                break;
                            case 5:
                                Toast.makeText(getContext(), "این شماره توسط مدریت بلاک شده", Toast.LENGTH_SHORT).show();
                                break;
                            case 6:
                                Toast.makeText(getContext(), "کد فعال سازی برایتان پیامک شد", Toast.LENGTH_SHORT).show();
                                break;
                            case 7:
                                Toast.makeText(getContext(), "این شماره موجود نمی باشد", Toast.LENGTH_SHORT).show();
                                break;
                            case 8:
                                Toast.makeText(getContext(), "با موفقیت وارد شدید", Toast.LENGTH_SHORT).show();
                                break;
                            case 9:
                                Toast.makeText(getContext(), "کد فعال سازی نادرست است", Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                Toast.makeText(getContext(), "لطفا بعد چند ثانیه تلاش کنید", Toast.LENGTH_SHORT).show();

                        }
                        if (!response.body().getError()) {
                            startCdt();
                        }
                        binding.progressBar.setVisibility(View.INVISIBLE);
                        binding.done.setEnabled(true);
                        binding.done.setAlpha(1);
                    }
                }

                @Override
                public void onFailure(Call<LoginSmsModel> call, Throwable t) {
                    binding.progressBar.setVisibility(View.INVISIBLE);
                    binding.done.setEnabled(true);
                    binding.done.setAlpha(1);
                    Log.i("phoneLogin", t.getMessage());
                }
            });
        }
    }


    public boolean checkCode() {
        if (binding.getCode() != null && binding.getCode().length() == 5)
            return true;
        else
            return false;
    }
}
