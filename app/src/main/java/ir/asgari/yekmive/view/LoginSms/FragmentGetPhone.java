package ir.asgari.yekmive.view.LoginSms;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Objects;

import ir.asgari.yekmive.R;
import ir.asgari.yekmive.app.App;
import ir.asgari.yekmive.constants.Constants;
import ir.asgari.yekmive.databinding.FragmentGetPhoneBinding;
import ir.asgari.yekmive.network.RetrofitInstance;
import ir.asgari.yekmive.network.ServerRequest;
import ir.asgari.yekmive.view.BaseFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentGetPhone extends BaseFragment<FragmentGetPhoneBinding> implements View.OnClickListener, Constants {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding.done.setOnClickListener(this);
        binding.setResultText("");
        binding.progressBar.setVisibility(View.INVISIBLE);
        return binding.getRoot();
    }

    @Override
    public int getIdLayout() {
        return R.layout.fragment_get_phone;
    }

    @Override
    public void onClick(View v) {
        if (binding.getNumber() != null && checkNumber(binding.getNumber())) {
            binding.done.setAlpha(0.5f);
            binding.done.setEnabled(false);
            binding.progressBar.setVisibility(View.VISIBLE);
            ServerRequest serverRequest = RetrofitInstance.getRetrofitInstance().create(ServerRequest.class);
            Call<LoginSmsModel> call = serverRequest.sendPhone(CLIENT_ID, "1", binding.getNumber());

            call.enqueue(new Callback<LoginSmsModel>() {
                @Override
                public void onResponse(Call<LoginSmsModel> call, Response<LoginSmsModel> response) {
                    {
                        int satus = response.body().getApiStatos();
                        switch (satus) {
                            case 1:
                                Toast.makeText(getContext(), "شماره موبایل وارد کنید", Toast.LENGTH_SHORT).show();
                                break;
                            case 2:
                                Toast.makeText(getContext(), "فورمت شماره موبایل نادرست است", Toast.LENGTH_SHORT).show();
                                break;
                            case 3:
                                Toast.makeText(getContext(), "کد دریافتی را وارد کنید", Toast.LENGTH_SHORT).show();
                                break;
                            case 4:
                                Toast.makeText(getContext(), "تعداد تلاش بیش از حد بوده بعدا از ۲۴ ساعت امتحان کنید", Toast.LENGTH_SHORT).show();
                                break;
                            case 5:
                                Toast.makeText(getContext(), "این شماره توسط مدریت بلاک شده", Toast.LENGTH_SHORT).show();
                                break;
                            case 6:
                                Toast.makeText(getContext(), "کد فعال سازی برایتان پیامک شد", Toast.LENGTH_SHORT).show();
                                break;
                            case 7:
                                Toast.makeText(getContext(), "این شماره موجود نمی باشد", Toast.LENGTH_SHORT).show();
                                break;
                            case 8:
                                Toast.makeText(getContext(), "با موفقیت وارد شدید", Toast.LENGTH_SHORT).show();
                                break;
                            case 9:
                                Toast.makeText(getContext(), "کد فعال سازی نادرست است", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        if (!response.body().getError()) {
//                            binding.tvResult.setText("کد فعال سازی برایتان پیامک شد");
                            ((LoginSmsCodeActivity) getActivity()).setFragment(new FragmentCheckCode(binding.getNumber()));
                        }
                        binding.progressBar.setVisibility(View.INVISIBLE);
                        binding.done.setEnabled(true);
                        binding.done.setAlpha(1);
                    }
                }

                @Override
                public void onFailure(Call<LoginSmsModel> call, Throwable t) {
                    binding.progressBar.setVisibility(View.INVISIBLE);
                    binding.tvResult.setText( " لطفا اینترنت خود را چک کنید");
                    binding.done.setEnabled(true);
                    binding.done.setAlpha(1);
                    Log.i("phoneLogin", t.getMessage());
                }
            });

        }
    }

    public boolean checkNumber(String n) {
        if (n.length() < 11) {
            return false;
        } else {
            return true;
        }
    }
}
