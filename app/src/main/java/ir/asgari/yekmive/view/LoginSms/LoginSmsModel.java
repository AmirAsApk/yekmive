package ir.asgari.yekmive.view.LoginSms;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginSmsModel {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("apiStatos")
    @Expose
    private int apiStatos;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public int getApiStatos() {
        return apiStatos;
    }

    public void setApiStatos(Integer apiStatos) {
        this.apiStatos = apiStatos;
    }

}
