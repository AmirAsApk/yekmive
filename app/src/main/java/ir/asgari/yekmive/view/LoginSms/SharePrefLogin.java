package ir.asgari.yekmive.view.LoginSms;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharePrefLogin {

    SharedPreferences sp;
    public static String NUMBER = "number";


    public SharePrefLogin(Context context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }


    public boolean isLogin() {
        return sp.contains(NUMBER);
    }

    public void setParams(String number) {
        sp.edit().putString(NUMBER, number).commit();
    }

    public String getParams() {
        return sp.getString(NUMBER, "");
    }

}
